# Changelog

## 

## v0.1 (2015.04.03)

  - Create and manage user account
  - Create and manage repositories
  - Upload mass spectra per repository
  - Data transformers
    - MassBank format
    - NIST format (individual)
