# Pending features (API / backend)

## User management

[ ] Manage preferences
[ ] Register a new account
[ ] Authorization
[ ] Delete account

## Repository mangement

[ ] Manage preferences
[ ] Add repository creation and last updated timestamps
[ ] Endpoint to show resources updated since a specified timestamp

## Resource management

[ ] Separate ID table relying on database to manage uniqueness and autoinc

## Commit management

[ ] Separate many-to-many table for commit to resource ID + version collecions
[ ] How can I resolve conflicts with uploaded data? Check origin IDs within the same repository?

## Search

[ ] Refine lucene search

## Webhooks

[x] Create specification
[ ] Generate necessary models

## General

[ ] Browse documentation
[ ] Comprehensive error handling
[ ] Migrate to REST API specification
[ ] Endpoint to show repositories updated since a specified timestamp

