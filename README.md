**mnn-data-node** is a data hub for metabolomics data with a Hypermedia API access and simple HTML web application for searching and browsing data.

**Technologies:**

- Written in Scala 2.10.3, runs on any JVM
- Primarily produces HAL-JSON responses [specification](http://stateless.co/hal_specification.html) with some application-specific additions
- Asynchronous request handling via [spray.io](http://spray.io)
- Compatible with multiple database backends via [Slick](http://slick.typesafe.com/)
- Responsive HTML5 web application built with [Bootstrap](http://getbootstrap.com/)
