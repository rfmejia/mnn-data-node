import com.typesafe.sbt.SbtStartScript

import sbtassembly.Plugin.AssemblyKeys._

seq(SbtStartScript.startScriptForClassesSettings: _*)

organization := "jp.riken.mnn"

name := "mnn-data-node"

version := "0.5.1"

scalaVersion := "2.11.1"

scalacOptions ++= Seq(
  "-Xlint",
  "-deprecation",
  "-Xfatal-warnings",
  "-feature", 
  "-unchecked",
  "-encoding",
  "utf8")

val commonDependencies = {
  val specs2Version = "2.4.1"
  Seq(
    "com.typesafe" % "config" % "1.2.0",
    "org.slf4j" % "slf4j-nop" % "1.6.4",
    "org.specs2" %% "specs2" % specs2Version % "test",
    "org.specs2" %% "specs2-core" % specs2Version % "test",
    "org.specs2" %% "specs2-junit" % specs2Version % "test"
  )
}

val repositoryDependencies = Seq(
  "com.github.tototoshi" %% "slick-joda-mapper" % "1.2.0",
  "com.h2database" % "h2" % "1.3.172",
  "com.nothome" % "javaxdelta" % "2.0.1",
  "com.typesafe.slick" %% "slick" % "2.1.0",
  "joda-time" % "joda-time" % "2.2",
  "net.sf.ehcache" % "ehcache-core" % "2.6.6",
  "org.apache.lucene" % "lucene-core" % "4.10.1",
  "org.apache.lucene" % "lucene-analyzers-common" % "4.10.1",
  "org.apache.lucene" % "lucene-queries" % "4.10.1",
  "org.apache.lucene" % "lucene-queryparser" % "4.10.1",
  "org.joda" % "joda-convert" % "1.3.1"
)

val apiDependencies = {
  val akkaVersion = "2.3.7"
  val sprayVersion = "1.3.2"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "io.spray" %% "spray-can" % sprayVersion,
    "io.spray" %% "spray-client" % sprayVersion,
    "io.spray" %% "spray-http" % sprayVersion,
    "io.spray" %% "spray-json" % "1.3.2",
    "io.spray" %% "spray-routing" % sprayVersion,
    "com.jason-goodwin" %% "authentikat-jwt" % "0.3.5",
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test",
    "io.spray" %% "spray-testkit" % sprayVersion % "test"
  )
}

libraryDependencies ++= commonDependencies ++ repositoryDependencies ++ apiDependencies

net.virtualvoid.sbt.graph.Plugin.graphSettings

Revolver.settings

mainClass in Revolver.reStart := Some("jp.riken.mnn.system.HttpServer")

mainClass in Compile := Some("jp.riken.mnn.system.HttpServer")

mainClass in run := Some("jp.riken.mnn.system.Console")

mainClass in assembly := Some("jp.riken.mnn.system.Console")

mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
  {
      case "application.conf" => MergeStrategy.concat
      case x => old(x)
  }
}
