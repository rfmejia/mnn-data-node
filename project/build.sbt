// Import the sbt-start-script plugin
resolvers += Classpaths.typesafeResolver

resolvers += "spray repo" at "http://repo.spray.io"

addSbtPlugin("com.typesafe.sbt" %% "sbt-start-script" % "0.10.0")
