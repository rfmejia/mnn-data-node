// Comment to get more information during initialization
logLevel := Level.Warn

// Typesafe repository
resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

// Spray Repository
resolvers += "spray" at "http://repo.spray.io/"

// Local Ivy repository to fetch other local modules
val ivyLocal = Resolver.file("Local Ivy repository", file(Path.userHome.absolutePath + "/.ivy2/local"))(Resolver.ivyStylePatterns)

externalResolvers += ivyLocal

// Plugin for rapid testing of sbt projects
addSbtPlugin("io.spray" % "sbt-revolver" % "0.7.1")

// Plugin for printing dependency graphs of libraries
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.7.4")

// Plugin for assembling JAR archives with dependencies (fat JARs)
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.11.2")

// Plugin for creating Eclipse projects
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "3.0.0")

addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "0.6.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-scalariform" % "1.3.0")
