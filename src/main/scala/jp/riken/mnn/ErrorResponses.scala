package jp.riken.mnn

import spray.http.StatusCode._
import spray.http.StatusCodes._
import spray.http._

trait ErrorResponses {
  def serverError(msg: String = "", status: ServerError = InternalServerError) =
    new RequestProcessingException(status, msg)

  def badRequest(msg: String = "") = new IllegalRequestException(BadRequest, msg)

  def notFound(msg: String = "") = new IllegalRequestException(NotFound, msg)

  def unsupportedMediaType(msg: String = "") = new IllegalRequestException(UnsupportedMediaType, msg)

  def incompletePayloadFields = badRequest("Some values are missing or have incorrect type. Please check your request.")
}

object ErrorResponses extends ErrorResponses
