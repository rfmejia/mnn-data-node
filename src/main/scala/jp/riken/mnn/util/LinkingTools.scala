package jp.riken.mnn.util

import jp.riken.mnn.Settings
import scala.language.implicitConversions
import spray.http._
import spray.http.Uri.{ Path, Query }

trait LinkingTools {
  val host = Settings.api.uri.host
  val prefix = Settings.api.uri.prefix
  val absolute = Settings.api.uri.absolute

  implicit def uriToString(u: Uri): String = u.toString

  def baseUri(implicit u: Uri) = Uri(u.scheme, u.authority)

  def linkto(u: Uri): Uri = linkto(u.path.toString, u.query)

  def linkto(segment: String, query: Query = Query.Empty): Uri = {
    val path = if (segment.isEmpty) prefix else prefix + segment
    if (!absolute) Uri./.withPath(path).withQuery(query)
    else host.withPath(path).withQuery(query)
  }
}
