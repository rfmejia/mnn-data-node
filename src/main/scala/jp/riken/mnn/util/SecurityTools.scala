package jp.riken.mnn.util

import authentikat.jwt._
import java.security.{ MessageDigest, SecureRandom }
import jp.riken.mnn.types.User
import jp.riken.mnn.Settings
import org.joda.time.DateTime

object SecurityTools {
  private val algorithmName = "SHA-512"
  private lazy val generator = new SecureRandom()
  private val header = JwtHeader(Settings.jwt.algorithm)
  private val registry = new InMemoryRegistry[String, (User, DateTime)]
  private val secret = Settings.jwt.secret

  def generateSalt(length: Int = 32): Array[Byte] = {
    val bytes = new Array[Byte](length)
    generator.nextBytes(bytes)
    bytes
  }

  def hashPassword(password: Array[Byte], salt: Array[Byte]): Array[Byte] = {
    val digest = MessageDigest.getInstance(algorithmName)
    digest.update(password)
    digest.update(salt)
    digest.digest()
  }

  def generateToken(user: User): (String, DateTime) = {
    val exp = new DateTime().plusHours(12)
    val claimsSet = JwtClaimsSet(Map(
      "sub" -> user.username,
      "name" -> user.name,
      "email" -> user.email,
      "exp" -> exp.getMillis() / 1000
    ))
    val token = JsonWebToken(header, claimsSet, secret)
    registry.save(token, (user, exp))
    (token, exp)
  }

  def validate(token: String): Option[(User, DateTime)] = {
    // TODO: There is a bug in validation call
    if (JsonWebToken.validate(token, secret)) {
      // Check expiration
      val now = new DateTime()
      registry.find(token).filter(kv => kv._2.isAfter(now))
    } else None
  }

  def invalidate(user: User): Unit = synchronized {
    registry.store = registry.store.filterNot(kv => kv._1 == user)
  }

  def clearExpired(): Unit = synchronized {
    val now = new DateTime()
    registry.store = registry.store.filter(kv => kv._2._2.isAfter(now))
  }
}

class InMemoryRegistry[K, T] {
  var store = new scala.collection.immutable.HashMap[K, T]
  def find(key: K): Option[T] = store.get(key)
  def save(key: K, value: T): Unit = synchronized(store = store + ((key, value)))
  def removeKey(key: K): Unit = synchronized(store = store - key)
  def removeValue(value: T): Unit = synchronized(store = store.filterNot(_._2 == value))
}
