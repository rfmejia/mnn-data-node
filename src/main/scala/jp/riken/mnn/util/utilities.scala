package jp.riken.mnn.util

import com.typesafe.config.Config
import jp.riken.mnn.ErrorResponses
import java.io.File
import scala.util.matching.Regex
import scala.util.{ Try, Success, Failure }

object Validators extends ErrorResponses {

  def nonNull[T](obj: T, msg: String = "Object is null"): Try[T] = Try {
    Option(obj) match {
      case Some(o) => o
      case None    => throw badRequest(msg)
    }
  }

  def nonEmptyString(s: String, msg: String = "Illegal empty string"): Try[String] = Try {
    if (Option(s).isDefined && !s.isEmpty) s
    else throw badRequest(msg)
  }

  def castTo[T](obj: Any): Try[T] = Try(obj.asInstanceOf[T])

  def stringToInt(s: String): Try[Int] = Try(s.toInt)

  def hasSingleMatch(s: String, r: Regex): Try[String] = Try {
    val matches = (r findFirstIn s)
    if (matches.size == 1) matches.get
    else if (matches.isEmpty) throw badRequest(s"Input '${s}' does not match pattern '${r.toString}'")
    else throw badRequest(s"Input '${s}' must contain only one instance of '${r.toString}'; found ${matches.size}")
  }

  def naturalInteger(i: Int): Try[Int] = Try {
    if (i > 0) i
    else throw badRequest(s"Expected positve integer > 0, found ${i}")
  }

  def validResourceFolder(folder: File): Try[File] = for {
    f <- nonNull(folder)
    _ <- {
      if (!f.exists) throw serverError(s"Resource folder '${f}' does not exist")
      else if (!f.isDirectory) throw serverError(s"Resource folder '${f}' is not a directory")
      else if (!f.canRead) throw serverError(s"Cannot read contents of '${f}'")
      else if (!f.canWrite) throw serverError(s"Cannot write to '${f}'")
      else Success(f)
    }
  } yield f
}

object StringOps {
  def asNonEmptyOption(s: String): Option[String] =
    if (s == null || s.isEmpty) None
    else Some(s)
}
