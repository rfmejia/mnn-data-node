package jp.riken.mnn.types

import com.github.tototoshi.slick.H2JodaSupport._
import java.util.UUID
import jp.riken.mnn.database._
import jp.riken.mnn.json._
import jp.riken.mnn.util.LinkingTools
import org.joda.time.DateTime
import scala.language.implicitConversions
import scala.slick.driver.H2Driver.simple._
import spray.json._

case class Revision(revisionId: RevisionID, projectId: ProjectID, isDraft: Boolean, author: String,
  message: Option[String], date: Option[DateTime])

object Revision extends ((RevisionID, ProjectID, Boolean, String, Option[String], Option[DateTime]) => Revision) {
  def apply(projectId: ProjectID, author: String): Revision =
    Revision(RevisionID.random, projectId, true, author, None, None)

  sealed trait ChangeAction {
    val name: String
    override def toString = name
  }
  case object Create extends ChangeAction { val name = "create" }
  case object Update extends ChangeAction { val name = "update" }
  case object Delete extends ChangeAction { val name = "delete" }
  case object Unknown extends ChangeAction { val name = "unknown" }

  object ChangeAction {
    def fromString(s: String): ChangeAction = s.toLowerCase match {
      case Create.name => Create
      case Update.name => Update
      case Delete.name => Delete
      case _           => Unknown
    }
    implicit val slickMapper = MappedColumnType.base[ChangeAction, String](_.toString, ChangeAction.fromString(_))
  }

  class SlickModel(tag: Tag) extends Table[Revision](tag, "REVISIONS") {
    def revisionId = column[RevisionID]("REVISIONS_PK", O.PrimaryKey)

    def projectId = column[ProjectID]("PROJECT_ID", O.NotNull)
    def isDraft = column[Boolean]("IS_DRAFT", O.NotNull)
    def author = column[String]("AUTHOR", O.NotNull)
    def message = column[Option[String]]("MESSAGE")
    def date = column[Option[DateTime]]("DATE")

    def project = foreignKey("REVISIONS_PROJECTS_FK", projectId, ProjectManager.elems)(
      _.projectId,
      onDelete = ForeignKeyAction.Restrict
    )

    def * = (revisionId, projectId, isDraft, author, message, date) <> (Revision.tupled, Revision.unapply)
  }

  implicit object Writer extends RootJsonWriter[Revision] with ExtendedJsonProtocol with LinkingTools {
    def write(r: Revision): JsValue = {
      JsonApi.resource("revision", r.revisionId.urn)
        .addAttributes(
          "message" -> r.message.toJson,
          "date" -> r.date.toJson
        )
        .addRelationships(
          "project" -> JsObject.empty
            .addSelf(linkto(s"/projects/${r.projectId.urlSegment}"))
            .addData(JsonApi.resource("project", r.projectId.urn))
        )
        .addMeta(
          "is_draft" -> r.isDraft.toJson,
          "author" -> r.author.toJson
        )
        .addLinks(
          "resources" -> linkto(s"/revisions/${r.revisionId.urlSegment}/resources").toJson
        )
    }
  }
}
