package jp.riken.mnn.types

import java.security.MessageDigest
import jp.riken.mnn.mediatypes._
import jp.riken.mnn.ObjectRegistry
import scala.language.implicitConversions
import scala.slick.driver.H2Driver.simple._
import spray.http.MediaType
import spray.json._

case class RegisteredMediaType(
    mtString: String,
    fileExtensions: Seq[String] = Seq.empty
) extends Ordered[RegisteredMediaType] {

  val underlying = MediaType.custom(mtString)

  val hash = MessageDigest.getInstance("MD5").digest(underlying.toString.getBytes).map("%02x".format(_)).mkString

  def computeFilename(name: String): String = {
    val ext: Option[String] = fileExtensions.headOption.map("." + _)
    name + ext.getOrElse("")
  }

  override def equals(obj: Any) = obj match {
    case that: RegisteredMediaType => this.mtString == that.mtString
    case that: String              => this.mtString == that
    case _                         => super.equals(obj)
  }

  def compare(that: RegisteredMediaType) = this.mtString.compareTo(that.mtString)

  override def toString = mtString
}

object RegisteredMediaType {
  implicit def slickMapper = MappedColumnType.base[RegisteredMediaType, String](
    _.mtString,
    ObjectRegistry.registeredMediaTypes.find(_).get
  )

  implicit object Writer extends JsonWriter[RegisteredMediaType] {
    def write(mt: RegisteredMediaType) = JsString(mt.mtString)
  }
}
