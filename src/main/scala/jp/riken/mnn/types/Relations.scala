package jp.riken.mnn.types

import java.util.UUID
import jp.riken.mnn.database._
import jp.riken.mnn.types.Revision.ChangeAction
import scala.slick.driver.H2Driver.simple._

case class ProjectResourcesRelation(projectId: ProjectID, versionedId: VersionedID)

object ProjectResourcesRelation extends ((ProjectID, VersionedID) => ProjectResourcesRelation) {
  class SlickModel(tag: Tag) extends Table[ProjectResourcesRelation](tag, "PROJECT_RESOURCES") {
    def projectId = column[ProjectID]("PROJECT_ID", O.NotNull)
    def versionedId = column[VersionedID]("VERSIONED_ID", O.NotNull)
    def pk = primaryKey("PROJECT_RESOURCES_PK", (projectId, versionedId))

    def project = foreignKey("PROJECT_RESOURCES_PROJECTS_FK", projectId,
      ProjectManager.elems)(_.projectId, onDelete = ForeignKeyAction.Cascade)
    def resource = foreignKey("PROJECT_RESOURCES_REGISTERED_RESOURCES_FK", versionedId,
      RegisteredResourceManager.elems)(_.versionedId, onDelete = ForeignKeyAction.Restrict)

    def * = (projectId, versionedId) <> (ProjectResourcesRelation.tupled, ProjectResourcesRelation.unapply)
  }
}

case class RevisionResourcesRelation(revisionId: RevisionID, instanceId: InstanceID, action: ChangeAction)

object RevisionResourcesRelation extends ((RevisionID, InstanceID, ChangeAction) => RevisionResourcesRelation) {
  class SlickModel(tag: Tag) extends Table[RevisionResourcesRelation](tag, "REVISION_RESOURCES") {
    def revisionId = column[RevisionID]("REVISION_ID", O.NotNull)
    def instanceId = column[InstanceID]("INSTANCE_ID", O.NotNull)
    def pk = primaryKey("REVISION_RESOURCES_PK", (revisionId, instanceId))

    def action = column[ChangeAction]("CHANGE_ACTION", O.NotNull)

    def revision = foreignKey("REVISION_RESOURCES_REVISIONS_FK", revisionId, RevisionManager.elems)(
      _.revisionId,
      onDelete = ForeignKeyAction.Restrict
    )

    def * = (revisionId, instanceId, action) <> (RevisionResourcesRelation.tupled, RevisionResourcesRelation.unapply)
  }
}
