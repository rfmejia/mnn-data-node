package jp.riken.mnn.types

import com.github.tototoshi.slick.H2JodaSupport._
import jp.riken.mnn.database._
import jp.riken.mnn.json._
import jp.riken.mnn.util.LinkingTools
import scala.slick.driver.H2Driver.simple._
import spray.json._

case class User(userId: UserID, username: String, password: Array[Byte], salt: Array[Byte], name: String, email: String)

object User extends ((UserID, String, Array[Byte], Array[Byte], String, String) => User) {
  class SlickModel(tag: Tag) extends Table[User](tag, "USERS") {
    def userId = column[UserID]("USERS_PK", O.PrimaryKey)

    def username = column[String]("USERNAME", O.NotNull)
    def password = column[Array[Byte]]("PASSWORD", O.NotNull)
    def salt = column[Array[Byte]]("SALT", O.NotNull)
    def name = column[String]("NAME", O.NotNull)
    def email = column[String]("EMAIL", O.NotNull)

    def usernameIdx = index("USERNAME_UK", username, unique = true)

    def * = (userId, username, password, salt, name, email) <> (User.tupled, User.unapply)
  }

  implicit object Writer extends RootJsonWriter[User] with ExtendedJsonProtocol with LinkingTools {
    def write(u: User): JsValue = {
      JsonApi.resource("user", u.userId.urn)
        .addSelf(linkto(s"/users/${u.userId.urlSegment}"))
        .addAttributes(
          "name" -> u.name.toJson,
          "email" -> u.email.toJson,
          "password" -> "[encrypted]".toJson
        )
        .addMeta(
          "username" -> u.username.toJson
        )
    }
  }
}
