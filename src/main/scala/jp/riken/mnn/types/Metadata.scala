package jp.riken.mnn.types

import jp.riken.mnn.database.SlickTypeMappers
import jp.riken.mnn.database._
import scala.slick.driver.H2Driver.simple._

/** A pair of metadata information for a resource */
case class Metadata(instanceId: InstanceID, key: String, value: String)

object Metadata extends ((InstanceID, String, String) => Metadata) {
  class SlickModel(tag: Tag) extends Table[Metadata](tag, "METADATA_PAIRS") {
    def instanceId = column[InstanceID]("METADATA_PAIRS_PK", O.NotNull)
    def key = column[String]("KEY", O.NotNull)
    def pk = primaryKey("METADATA_PAIRS_PK", (instanceId, key))

    def value = column[String]("VALUE", O.NotNull)

    def * = (instanceId, key, value) <> (Metadata.tupled, Metadata.unapply)
  }
}
