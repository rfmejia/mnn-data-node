package jp.riken.mnn.types

import com.github.tototoshi.slick.H2JodaSupport._
import jp.riken.mnn.database.SlickTypeMappers
import jp.riken.mnn.json._
import org.joda.time.DateTime
import scala.slick.driver.H2Driver.simple._
import spray.http.Uri
import spray.json._

/** Lists information about a node in the network. */
case class NodeInfo(id: String, modified: DateTime, href: Uri, nodeType: String)

object NodeInfo extends ((String, DateTime, Uri, String) => NodeInfo) with SlickTypeMappers {
  class SlickModel(tag: Tag) extends Table[NodeInfo](tag, "NODE_INFOS") {
    def id = column[String]("NODE_INFOS_PK", O.PrimaryKey)

    def modified = column[DateTime]("MODIFIED", O.NotNull)
    def href = column[Uri]("HREF", O.NotNull)
    def nodeType = column[String]("NODE_TYPE", O.NotNull)

    def * = (id, modified, href, nodeType) <> (NodeInfo.tupled, NodeInfo.unapply)
  }

  implicit object Writer extends RootJsonWriter[NodeInfo] with ExtendedJsonProtocol {
    def write(n: NodeInfo): JsValue = {
      JsonApi.resource("node", n.id)
        .addAttributes(
          "modified" -> n.modified.toJson,
          "href" -> n.href.toJson,
          "node_type" -> n.nodeType.toJson
        )
    }
  }
}
