package jp.riken.mnn.types

import com.github.tototoshi.slick.H2JodaSupport._
import java.io.File
import jp.riken.mnn.database._
import jp.riken.mnn.json._
import jp.riken.mnn.util.LinkingTools
import org.joda.time.DateTime
import scala.collection.mutable
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }
import spray.json._

sealed trait Resource {
  val id: InstanceID
  val title: String
  val originId: Option[String]
  val data: Option[RawData]

  def saveToDisk(): Try[Option[RawData]] = data match {
    case Some(d) =>
      val filename = id match {
        case v: VersionedID => s"${v.asPlainID}/v${v.version}/raw"
        case t: TemporaryID => s"temp/${t.urn}"
      }
      for {
        relpath <- Try(new File(filename))
        saved <- d.saveToDisk(relpath.toString)
      } yield Some(saved)

    case None => Success(None)
  }
}

case class RegisteredResource(versionedId: VersionedID, date: DateTime, title: String, originId: Option[String],
    data: Option[RawData]) extends Resource {
  val id = versionedId
}

object RegisteredResource extends ((VersionedID, DateTime, String, Option[String], Option[RawData]) => RegisteredResource) {
  class SlickModel(tag: Tag) extends Table[RegisteredResource](tag, "REGISTERED_RESOURCES") {
    def versionedId = column[VersionedID]("REGISTERED_RESOURCES_PK", O.PrimaryKey)

    def date = column[DateTime]("DATE", O.NotNull)
    def title = column[String]("TITLE", O.NotNull)
    def originId = column[Option[String]]("ORIGIN_ID")
    def data = column[Option[RawData]]("RAW_DATA_META")

    def * = (versionedId, date, title, originId, data) <> (RegisteredResource.tupled, RegisteredResource.unapply)
  }

  implicit object Writer extends RootJsonWriter[RegisteredResource] with ExtendedJsonProtocol with LinkingTools {
    def write(r: RegisteredResource) = {
      JsonApi.resource("resource", r.versionedId.urn)
        .addSelf(linkto(s"/resources/${r.versionedId.urlSegment}"))
        .addAttributes(
          "title" -> r.title.toJson
        )
        .addMeta(
          "date" -> r.date.toJson,
          "origin_id" -> r.originId.toJson,
          "data" -> r.data.map(_.toJson).toJson
        )
    }
  }
}

case class UnregisteredResource(temporaryId: TemporaryID, title: String, versionedId: Option[VersionedID],
    originId: Option[String], data: Option[RawData]) extends Resource {
  val id = temporaryId
}

object UnregisteredResource extends ((TemporaryID, String, Option[VersionedID], Option[String], Option[RawData]) => UnregisteredResource) {
  class SlickModel(tag: Tag) extends Table[UnregisteredResource](tag, "UNREGISTERED_RESOURCES") {
    def temporaryId = column[TemporaryID]("UNREGISTERED_RESOURCES_PK", O.PrimaryKey)

    def title = column[String]("TITLE", O.NotNull)
    def versionedId = column[Option[VersionedID]]("VERSIONED_ID")
    def originId = column[Option[String]]("ORIGIN_ID")
    def data = column[Option[RawData]]("RAW_DATA_META")

    def * = (temporaryId, title, versionedId, originId, data) <> (UnregisteredResource.tupled, UnregisteredResource.unapply)
  }

  implicit object Writer extends RootJsonWriter[UnregisteredResource] with ExtendedJsonProtocol with LinkingTools {
    def write(r: UnregisteredResource) = {
      JsonApi.resource("resource", r.temporaryId.urn)
        .addSelf(linkto(s"/resources/${r.temporaryId.urlSegment}"))
        .addAttributes(
          "title" -> r.title.toJson,
          "versioned_id" -> r.versionedId.toJson,
          "origin_id" -> r.originId.toJson
        )
        .addMeta(
          "is_draft" -> false.toJson,
          "data" -> r.data.map(_.toJson).toJson
        )
    }
  }
}
