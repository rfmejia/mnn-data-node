package jp.riken.mnn.types

import jp.riken.mnn.database._
import jp.riken.mnn.json._
import jp.riken.mnn.util.LinkingTools
import scala.slick.driver.H2Driver.simple._
import spray.json._

/** A container for user-uploaded resources */
case class Project(projectId: ProjectID, owner: UserID, projectName: String, description: Option[String],
  readme: Option[String], policy: String = Project.ManagementPolicy.Public)

object Project extends ((ProjectID, UserID, String, Option[String], Option[String], String) => Project)
    with ExtendedJsonProtocol {

  object ManagementPolicy {
    val Private = "private"
    val Protected = "protected"
    val Public = "public"
  }

  class SlickModel(tag: Tag) extends Table[Project](tag, "PROJECTS") {
    def projectId = column[ProjectID]("PROJECTS_PK", O.PrimaryKey)

    def owner = column[UserID]("OWNER", O.NotNull)
    def projectName = column[String]("PROJECT_NAME", O.NotNull)
    def description = column[Option[String]]("DESCRIPTION")
    def readme = column[Option[String]]("README")
    def policy = column[String]("POLICY", O.NotNull)

    def user = foreignKey("PROJECTS_USERS_FK", owner, UserManager.elems)(_.userId, onDelete = ForeignKeyAction.Restrict)

    def * = (projectId, owner, projectName, description, readme, policy) <> (Project.tupled, Project.unapply)
  }

  implicit object Writer extends RootJsonWriter[Project] with ExtendedJsonProtocol with LinkingTools {
    def write(p: Project): JsValue = {
      JsonApi.resource("project", p.projectId.urn)
        .addSelf(linkto(s"/projects/${p.projectId.urlSegment}"))
        .addAttributes(
          "project_name" -> p.projectName.toJson,
          "description" -> p.description.toJson,
          "readme" -> p.readme.toJson,
          "policy" -> p.policy.toJson
        )
        .addRelationships(
          "owner" -> JsObject.empty
            .addSelf(linkto(s"/users/${p.owner.urlSegment}"))
            .addData(JsonApi.resource("user", p.owner.urn))
        )
    }
  }
}
