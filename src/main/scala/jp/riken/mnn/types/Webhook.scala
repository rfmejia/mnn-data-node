package jp.riken.mnn.types

import com.github.tototoshi.slick.H2JodaSupport._
import jp.riken.mnn.ErrorResponses
import java.util.UUID
import jp.riken.mnn.database._
import jp.riken.mnn.json._
import jp.riken.mnn.util.LinkingTools
import scala.language.implicitConversions
import scala.slick.driver.H2Driver.simple._
import spray.http.Uri
import spray.json._

case class Webhook(uuid: UUID, subscriber: UserID, publisher: ProjectID, uri: Uri, eventType: String)

object Webhook extends ((UUID, UserID, ProjectID, Uri, String) => Webhook) with SlickTypeMappers {

  def apply(subscriber: UserID, publisher: ProjectID, url: Uri, eventType: String): Webhook =
    Webhook(UUID.randomUUID, subscriber, publisher, url, eventType)

  class SlickModel(tag: Tag) extends Table[Webhook](tag, "WEBHOOKS") {
    def uuid = column[UUID]("WEBHOOKS_PK", O.PrimaryKey)

    def subscriber = column[UserID]("SUBSCRIBER", O.NotNull)
    def publisher = column[ProjectID]("PUBLISHER", O.NotNull)
    def uri = column[Uri]("URI", O.NotNull)
    def eventType = column[String]("EVENT_TYPE", O.NotNull)

    def user = foreignKey("WEBHOOKS_USERS_FK", subscriber,
      UserManager.elems)(_.userId, onDelete = ForeignKeyAction.Cascade)
    def project = foreignKey("WEBHOOKS_PROJECTS_FK", publisher,
      ProjectManager.elems)(_.projectId, onDelete = ForeignKeyAction.Cascade)

    def * = (uuid, subscriber, publisher, uri, eventType) <> (Webhook.tupled, Webhook.unapply)
  }

  implicit object Writer extends JsonWriter[Webhook] with ExtendedJsonProtocol with LinkingTools {
    def write(w: Webhook): JsValue = {
      JsonApi.resource("project", w.uuid.toString)
        .addSelf(linkto(s"/webhooks/${w.uuid.toString}"))
        .addAttributes(
          "uri" -> w.uri.toJson,
          "event_type" -> w.eventType.toJson
        )
        .addRelationships(
          "subscriber" -> JsObject.empty
            .addSelf(linkto(s"/users/${w.subscriber.urlSegment}"))
            .addData(JsonApi.resource("user", w.subscriber.urn)),
          "publisher" -> JsObject.empty
            .addSelf(linkto(s"/projects/${w.publisher.urlSegment}"))
            .addData(JsonApi.resource("project", w.publisher.urn))
        )
    }
  }
}
