package jp.riken.mnn.types

import java.io._
import jp.riken.mnn.database.DB
import jp.riken.mnn.json._
import jp.riken.mnn.util.StringOps._
import jp.riken.mnn.util.Validators._
import jp.riken.mnn.{ ObjectRegistry, Settings }
import scala.io.{ Codec, Source }
import scala.language.implicitConversions
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }
import spray.json._

trait RawData {
  val deltaMethod: Option[String]
  val mediaType: RegisteredMediaType
  val size: Int
  def getData(): Try[Array[Byte]]
  def serialize: String

  def saveToDisk(relpath: String): Try[LocalFile] = for {
    d <- getData
    parent <- Try(new File(Settings.project.resourcePath))
    child <- nonEmptyString(relpath, "Relative path for raw data is empty")
    abspath <- Try {
      val path = new File(parent, child)
      if (path.isDirectory) throw new IllegalArgumentException(s"Supplied relative path ${path} is a directory")
      else {
        val dir = path.getParentFile
        if (!dir.exists) dir.mkdirs
        path
      }
    }
    lfile <- Try {
      val out = new BufferedOutputStream(new FileOutputStream(abspath))
      out.write(d)
      out.close()
      LocalFile(mediaType, deltaMethod, relpath, d.size)
    }
  } yield lfile

  def compareBytes(that: RawData): Boolean = {
    val check = for {
      d1 <- this.getData
      d2 <- that.getData
    } yield {
      d1.deep == d2.deep
    }
    check getOrElse false
  }
}

object RawData {
  def deserialize(s: String): Try[RawData] = for {
    str <- nonEmptyString(s, "Supplied raw data string to deserialize is empty")
    tokens <- Try(str.split('|'))
    rawDataType <- Try(RawData.Type.withName(tokens(0)))
    mt <- Try(ObjectRegistry.registeredMediaTypes.find(tokens(1)).get)
    delta <- Try(asNonEmptyOption(tokens(2)))
    data <- rawDataType match {
      case RawData.Type.LocalFile =>
        Try {
          val relpath = tokens(3)
          val size = tokens(4).toInt
          LocalFile(mt, delta, relpath, size)
        }
      case RawData.Type.InMemoryData =>
        Try {
          val data = tokens.drop(2).reduce(_ + _).getBytes("UTF-8")
          InMemoryData(mt, delta, data)
        }
    }
  } yield data

  /** By default, create an InMemoryData representation of data */
  def apply(mediaType: RegisteredMediaType, data: Array[Byte]): Try[RawData] = for {
    mt <- Success(mediaType)
    d <- nonNull(data)
  } yield InMemoryData(mt, None, d)

  def apply(mediaType: RegisteredMediaType, data: String): Try[RawData] =
    apply(mediaType, data.getBytes("UTF-8"))

  object Type extends Enumeration {
    val LocalFile, InMemoryData, Cache = Value
  }

  implicit def rawDataMapper = MappedColumnType.base[RawData, String](_.serialize, RawData.deserialize(_).get)

  implicit object Writer extends RootJsonWriter[RawData] with ExtendedJsonProtocol {
    def write(r: RawData) =
      JsObject(
        "mediaType" -> r.mediaType.toJson,
        "size" -> r.size.toJson
      )
  }
}

// TODO: Convert string to path object
case class LocalFile(mediaType: RegisteredMediaType, deltaMethod: Option[String], relpath: String, size: Int) extends RawData {

  def serialize = s"${RawData.Type.LocalFile}|${mediaType}|${deltaMethod getOrElse ""}|${relpath}|${size}"

  def getData = {
    def getByEncoding(file: File, codec: Codec): Try[Array[Byte]] = Try {
      val source = Source.fromFile(file)(codec)
      val arr = source.map(_.toByte).toArray
      source.close
      arr
    }

    for {
      parent <- Try(new File(Settings.project.resourcePath))
      child <- nonEmptyString(relpath, "Relative path for local file is empty")
      abspath <- Try(new File(parent, child))
      // Read UTF-8 for raw files, or ISO-8859-1 for deltas
      // Because of type erasure, we have to check the encoding here twice. This
      // can be still improved through better understanding of the Try construct.
      bytes <- getByEncoding(abspath, Codec.UTF8) orElse getByEncoding(abspath, Codec.ISO8859)
    } yield bytes
  }
}

case class InMemoryData(mediaType: RegisteredMediaType, deltaMethod: Option[String], data: Array[Byte]) extends RawData {
  val size = data.size
  def serialize = s"${RawData.Type.InMemoryData}|${mediaType}|${deltaMethod}|${new String(data)}"
  def getData = Success(data)
}
