package jp.riken.mnn.types

import com.github.tototoshi.slick.H2JodaSupport._
import jp.riken.mnn.database._
import jp.riken.mnn.types._
import scala.slick.driver.H2Driver.simple._

class QueuedIndicesModel(tag: Tag) extends Table[VersionedID](tag, "QUEUED_INDICES") {
  def versionedId = column[VersionedID]("QUEUED_INDICES_PK", O.PrimaryKey)

  def resource = foreignKey("QUEUED_INDICES_REGISTERED_RESOURCES_FK", versionedId, TableQuery[RegisteredResource.SlickModel])(_.versionedId)

  def * = versionedId
}
