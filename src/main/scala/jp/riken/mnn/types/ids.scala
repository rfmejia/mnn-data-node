package jp.riken.mnn.types

import java.util.UUID
import jp.riken.mnn.{ ErrorResponses, Settings }
import jp.riken.mnn.json.ExtendedJsonProtocol
import scala.language.implicitConversions
import scala.slick.driver.H2Driver.simple._
import scala.util.Try
import scala.util.parsing.combinator._
import spray.json._

/**
  * Defines the ID hierarchy for all resources in the system.
  *
  * ID:                            mnn://<ns>
  * - UserID:                            <ns>/<username>
  * - ProjectID:                      <ns>/<username>/<projectName>
  * - ResourceID:
  *   - PlainID                          <ns>.<serial>
  *     - VersionedID                    <ns>.<serial>.<version>
  *   - TemporaryID                      <uuid>
  * - RevisionID                         <uuid>
  */
sealed trait ID extends Ordered[ID] {
  val namespace: String
  val urn: String
  val urlSegment: String

  override def toString = urn

  override def equals(obj: Any) = obj match {
    case that: ID     => this.urn == that.urn
    case that: String => this.urn == that
    case _            => super.equals(obj)
  }

  override def hashCode = this.urn.hashCode

  def compare(that: ID) = this.urn.compareTo(that.urn)
}

trait IDParser[T <: ID] extends JavaTokenParsers with ErrorResponses {
  def structure: Parser[T]

  def parse(s: String)(implicit t: reflect.Manifest[T]): Try[T] =
    if (s.startsWith("mnn::")) parseUrlSegment(s.drop(5))
    else parseUrlSegment(s)

  def parseUrlSegment(s: String)(implicit t: reflect.Manifest[T]): Try[T] = parseAll(structure, s) match {
    case Success(id, _) => util.Success(id)
    case NoSuccess(err, _) =>
      val className = t.toString
      util.Failure(badRequest(s"'${s}' is an invalid ${className}"))
  }

  def validate(id: T)(implicit t: reflect.Manifest[T]): T = validate(id.urn)
  def validate(s: String)(implicit t: reflect.Manifest[T]): T = parse(s) match {
    case util.Success(result) => result
    case util.Failure(err)    => throw err
  }

  def javaUUID: Parser[UUID] = ".+".r ^^ { case uuid => UUID.fromString(uuid) }

  /** Automatic mapper into Slick column */
  implicit def slickMapper(implicit t: reflect.Manifest[T]) = MappedColumnType.base[T, String](_.urn, parse(_).get)

  implicit object Writes extends JsonWriter[T] {
    def write(id: T) = JsString(id.toString)
  }
}

case class UserID(namespace: String, username: String) extends ID {
  val urn = s"mnn::${namespace}/${username}"
  val urlSegment = s"${username}"
}
object UserID extends IDParser[UserID] {
  def local(username: String) = UserID(Settings.node.namespace, username)

  def structure = ident ~ "/" ~ ident ^^ {
    case namespace ~ _ ~ username => UserID(namespace, username)
  }

  implicit object Format extends JsonFormat[UserID] {
    def write(id: UserID) = JsString(id.urn)
    def read(v: JsValue) = v match {
      case JsString(s) => UserID.parse(s) match {
        case util.Success(id) => id
        case util.Failure(_)  => deserializationError(s"'${s}' is an invalid user ID")
      }
      case _ => deserializationError(s"'${v.toString} is an invalid user ID")
    }
  }
}

case class ProjectID(namespace: String, owner: String, projectName: String) extends ID with ExtendedJsonProtocol {
  val urn = s"mnn::${namespace}/${owner}/${projectName}"
  val urlSegment = s"${owner}/${projectName}"
  val asUserID = UserID(namespace, owner)
}
object ProjectID extends IDParser[ProjectID] {
  def local(owner: String, projectName: String) = ProjectID(Settings.node.namespace, owner, projectName)

  def structure = ident ~ "/" ~ ident ~ "/" ~ ident ^^ {
    case namespace ~ _ ~ username ~ _ ~ projectName => ProjectID(namespace, username, projectName)
  }

  implicit object Format extends JsonFormat[ProjectID] {
    def write(id: ProjectID) = JsString(id.urn)
    def read(v: JsValue) = v match {
      case JsString(s) => ProjectID.parse(s) match {
        case util.Success(rid) => rid
        case util.Failure(_)   => deserializationError(s"'${s}' is an illegal project ID")
      }
      case _ => deserializationError(s"'${v.toString}' is an illegal project ID")
    }
  }
}

case class RevisionID(uuid: UUID) extends ID {
  val namespace = Settings.node.namespace
  val urn = uuid.toString
  val urlSegment = urn
}
object RevisionID extends IDParser[RevisionID] with ExtendedJsonProtocol {
  def random = RevisionID(UUID.randomUUID)

  def structure = javaUUID ^^ { case uuid => RevisionID(uuid) }

  implicit object Format extends JsonFormat[RevisionID] {
    def write(id: RevisionID) = JsString(id.urn)
    def read(v: JsValue) = v match {
      case JsString(s) => RevisionID.parse(s) match {
        case util.Success(rid) => rid
        case util.Failure(_)   => deserializationError(s"'${s}' is an illegal revision ID")
      }
      case _ => deserializationError(s"'${v.toString}' is an illegal revision ID")
    }
  }
}

sealed trait ResourceID extends ID
object ResourceID extends IDParser[ResourceID] {
  def versionedId = ident ~ "." ~ wholeNumber ~ "." ~ wholeNumber
  def plainId = ident ~ "." ~ wholeNumber
  def temporaryId = javaUUID

  def structure =
    (versionedId ^^ { case namespace ~ _ ~ serial ~ _ ~ version => VersionedID(namespace, serial.toInt, version.toInt) }
      | plainId ^^ { case namespace ~ _ ~ serial => PlainID(namespace, serial.toInt) }
      | temporaryId ^^ { case uuid => TemporaryID(uuid) })
}

/** ID assigned to **committed** resources, hence assigned a serial (abstract or concrete). */
sealed trait SerialID extends ResourceID {
  val serial: Int
  val hasVersion: Boolean
  val asPlainID: PlainID
}
object SerialID extends IDParser[SerialID] {
  def versionedId = ident ~ "." ~ wholeNumber ~ "." ~ wholeNumber
  def plainId = ident ~ "." ~ wholeNumber
  def structure =
    (versionedId ^^ { case namespace ~ _ ~ serial ~ _ ~ version => VersionedID(namespace, serial.toInt, version.toInt) }
      | plainId ^^ { case namespace ~ _ ~ serial => PlainID(namespace, serial.toInt) })
}

/** ID assigned to **concrete** resource instances (draft or committed). */
sealed trait InstanceID extends ResourceID
object InstanceID extends IDParser[InstanceID] {
  def versionedId = ident ~ "." ~ wholeNumber ~ "." ~ wholeNumber
  def temporaryId = javaUUID
  def structure =
    (versionedId ^^ { case namespace ~ _ ~ serial ~ _ ~ version => VersionedID(namespace, serial.toInt, version.toInt) }
      | temporaryId ^^ { case uuid => TemporaryID(uuid) })
}

case class PlainID(namespace: String, serial: Int) extends SerialID {
  val urn = f"mnn::${namespace}.${serial}%06d"
  val urlSegment = f"${namespace}.${serial}%06d"
  val hasVersion = false
  val asPlainID = this
}
object PlainID extends ((String, Int) => PlainID) with IDParser[PlainID] {
  def structure = ident ~ "." ~ wholeNumber ^^ {
    case namespace ~ _ ~ serial => PlainID(namespace, serial.toInt)
  }
}

case class VersionedID(namespace: String, serial: Int, version: Int) extends SerialID with InstanceID {
  val urn = f"mnn::${namespace}.${serial}%06d.${version}"
  val urlSegment = f"${namespace}.${serial}%06d.${version}"
  val hasVersion = true
  val asPlainID = PlainID(namespace, serial)

  lazy val nextVersion: VersionedID = VersionedID(namespace, serial, version + 1)
}
object VersionedID extends ((String, Int, Int) => VersionedID) with IDParser[VersionedID] {
  def structure = ident ~ "." ~ wholeNumber ~ "." ~ wholeNumber ^^ {
    case namespace ~ _ ~ serial ~ _ ~ version => VersionedID(namespace, serial.toInt, version.toInt)
  }

  implicit object Format extends JsonFormat[VersionedID] {
    def write(id: VersionedID) = JsString(id.urn)
    def read(v: JsValue) = v match {
      case JsString(s) => VersionedID.parse(s) match {
        case util.Success(vid) => vid
        case util.Failure(_)   => deserializationError(s"'${s}' is an illegal versioned resource ID")
      }
      case _ => deserializationError(s"'${v.toString}' is an illegal versioned resource ID")
    }
  }
}

case class TemporaryID(uuid: UUID) extends InstanceID {
  val namespace = Settings.node.namespace
  val urn = uuid.toString
  val urlSegment = urn
}
object TemporaryID extends IDParser[TemporaryID] {
  def random = TemporaryID(UUID.randomUUID)

  def structure = javaUUID ^^ { case uuid => TemporaryID(uuid) }
}
