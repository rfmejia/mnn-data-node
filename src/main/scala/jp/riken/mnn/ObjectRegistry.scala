package jp.riken.mnn

import jp.riken.mnn.types.RegisteredMediaType
import jp.riken.mnn.mediatypes._
import jp.riken.mnn.mediatypes.mappers._
import jp.riken.mnn.mediatypes.readers._
import jp.riken.mnn.mediatypes.writers._

object ObjectRegistry {
  object registeredMediaTypes {
    val `application/hal+json` = RegisteredMediaType("application/hal+json", Seq("json"))
    val `application/vnd.api+json` = RegisteredMediaType("application/vnd.api+json", Seq("json"))
    val `application/vnd.massbank` = RegisteredMediaType("application/vnd.massbank", Seq("txt"))
    val `application/vnd.mona` = RegisteredMediaType("application/vnd.mona", Seq("json"))
    val `application/vnd.nist.list` = RegisteredMediaType("application/vnd.nist.list", Seq("msl", "msp", "txt"))
    val `application/vnd.nist` = RegisteredMediaType("application/vnd.nist", Seq("msp", "txt"))

    val elems: Vector[RegisteredMediaType] = Vector(
      `application/hal+json`,
      `application/vnd.api+json`,
      `application/vnd.massbank`,
      `application/vnd.mona`,
      `application/vnd.nist.list`,
      `application/vnd.nist`
    )

    def find(mt: RegisteredMediaType): Option[RegisteredMediaType] = elems.find(_ == mt)

    def find(s: String): Option[RegisteredMediaType] = find(RegisteredMediaType(s))

    def findHash(target: String): Option[RegisteredMediaType] = elems.find(_.hash == target)
  }

  object readers {
    def of(mt: RegisteredMediaType): Option[MassSpectraReader] = elems.get(mt)

    val elems: Map[RegisteredMediaType, MassSpectraReader] = {
      import registeredMediaTypes._
      Map(
        `application/vnd.massbank` -> MassBankReader,
        `application/vnd.mona` -> MonaRecordReader,
        `application/vnd.nist.list` -> NistListReader,
        `application/vnd.nist` -> NistReader
      )
    }
  }

  object writers {
    def of(mt: RegisteredMediaType): Option[MassSpectraWriter] = elems.get(mt)

    val elems: Map[RegisteredMediaType, MassSpectraWriter] = {
      import registeredMediaTypes._
      Map(
        `application/vnd.massbank` -> MassBankWriter,
        `application/vnd.nist.list` -> NistListWriter,
        `application/vnd.nist` -> NistWriter
      )
    }
  }

  object mappers {
    /** Returns a list of mappers that can work with the given source media type */
    // Note: Basic implementation for now; once mappers are dynamically registered,
    // this should be upgraded as well
    def ofSource(source: RegisteredMediaType): Vector[MassSpectraMapper] = elems.filter(_.source == source)

    def ofTarget(target: RegisteredMediaType): Vector[MassSpectraMapper] = elems.filter(_.target == target)

    def of(source: RegisteredMediaType, target: RegisteredMediaType): Vector[MassSpectraMapper] =
      elems.filter(mapper => mapper.source == source && mapper.target == target)

    def elems = Vector(
      MassBankToNistMapper,
      NistToMassBankMapper,
      NistToNistListMapper
    )
  }
}
