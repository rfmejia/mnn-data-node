package jp.riken.mnn.api

import akka.actor.ActorRef
import scala.concurrent.ExecutionContext
import jp.riken.mnn.ObjectRegistry
import jp.riken.mnn.ObjectRegistry.registeredMediaTypes.`application/vnd.nist.list`
import jp.riken.mnn.database._
import jp.riken.mnn.json._
import jp.riken.mnn.mediatypes.MassSpectraListWriter
import jp.riken.mnn.mediatypes.writers.NistListWriter
import jp.riken.mnn.types._
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }
import spray.http._
import spray.json._
import spray.routing._

class ProjectResourceService(eventHandler: ActorRef)(implicit ec: ExecutionContext) extends ApiService {

  val route = {
    path("projects" / Segment / Segment / "resources") { (username, projectName) =>
      getWithUri { implicit self: Uri =>
        extractOffsetLimit { (offset, limit) =>
          extractViewFromParams { view =>
            val projectId = ProjectID.local(username, projectName)
            ProjectID.validate(projectId)
            listProjectResources(projectId, offset, limit,
              View.defaultCollection ++ view)
          }
        }
      }
    } ~ path("projects" / Segment / Segment / "resources" / "mt" / Segment) { (username, projectName, mtHash) =>
      getWithUri { implicit self: Uri =>
        extractOffsetLimit { (offset, limit) =>
          val target = ObjectRegistry.registeredMediaTypes.findHash(mtHash)
          // For now, only support the NIST list format
          if (target.exists(_ == `application/vnd.nist.list`)) {
            val projectId = ProjectID.local(username, projectName)
            ProjectID.validate(projectId)
            listProjectResourcesRepresentation(projectId, target.head, offset, limit, NistListWriter)
          } else respondWithError(StatusCodes.UnsupportedMediaType)
        }
      }
    }
  }

  private def listProjectResourcesRepresentation(projectId: ProjectID, target: RegisteredMediaType, offset: Int,
    limit: Int, writer: MassSpectraListWriter)(implicit self: Uri): Route =
    DB.connect withSession { implicit session =>
      ProjectManager.find(projectId).firstOption match {
        case Some(project) =>
          val rawData: Seq[RawData] = RegisteredResourceManager.resourcesOf(projectId).drop(offset).take(limit)
            .map(_.data).list.flatten
          convertListToRepresentation(rawData, writer) match {
            case Success(data) =>
              val filename = target.computeFilename(s"${projectId.owner}-${projectId.projectName}")
              respondWithRawData(data, Some(filename))
            case Failure(err) => respondWithError(err)
          }
        case None => complete(StatusCodes.NotFound)
      }
    }

  private def listProjectResources(projectId: ProjectID, offset: Int, limit: Int, view: View): Route =
    DB.connect withSession { implicit session =>
      ProjectManager.find(projectId).firstOption match {
        case Some(project) =>
          val query = RegisteredResourceManager.resourcesOf(projectId).sortBy(_.date.desc)

          val (resources, total) = {
            val items = query.drop(offset).take(limit).buildColl[Vector].map(convertSingle(_))
            (items, query.size.run)
          }

          val supported = Vector(`application/vnd.nist.list`)
          val representations: Vector[JsObject] = supported.map { mt =>
            var url = s"/projects/${projectId.urlSegment}/resources/mt/${mt.hash}"
            JsObject("href" -> linkto(url).toJson, "media_type" -> mt.toJson)
          }

          completeJsonApi {
            JsonApi.collection(resources)
              .addCollectionNavLinks(linkto(s"/projects/${projectId.urlSegment}/resources"), total, offset, limit)
              .addMeta("total" -> total.toJson)
              .addLinks("representations" -> JsArray(representations))
          }

        case None => respondWithError(StatusCodes.NotFound, "Supplied user or project does not exist.")
      }
    }
}
