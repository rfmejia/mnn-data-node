package jp.riken.mnn.api

import akka.actor.ActorRef
import scala.concurrent.ExecutionContext
import java.util.UUID
import jp.riken.mnn.ObjectRegistry
import jp.riken.mnn.ObjectRegistry.registeredMediaTypes.`application/vnd.nist.list`
import jp.riken.mnn.Settings
import jp.riken.mnn.database._
import jp.riken.mnn.json._
import jp.riken.mnn.mediatypes.MassSpectraListWriter
import jp.riken.mnn.mediatypes.writers.NistListWriter
import jp.riken.mnn.types._
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }
import spray.http._
import spray.http.HttpHeaders.{ Accept, Location }
import spray.json._
import spray.routing.Route

class RevisionService(eventHandler: ActorRef)(implicit ec: ExecutionContext) extends ApiService {

  val route = {
    path("projects" / Segment / Segment / "revisions") { (owner, projectName) =>
      get {
        extractOffsetLimit { (offset, limit) =>
          val projectId = ProjectID.local(owner, projectName)
          ProjectID.validate(projectId)
          listRevisions(projectId, offset, limit)
        }
      } ~ post {
        authenticateToken { user =>
          val projectId = ProjectID.local(owner, projectName)
          ProjectID.validate(projectId)
          createRevision(projectId, user.username)
        }
      }
    } ~ path("revisions" / JavaUUID) { (uuid) =>
      get {
        showRevision(RevisionID(uuid))
      } ~ delete {
        authenticateToken { user =>
          deleteRevision(RevisionID(uuid))
        }
      }
    } ~ path("revisions" / JavaUUID / "resources") { (uuid) =>
      getWithUri { implicit self: Uri =>
        extractOffsetLimit { (offset, limit) =>
          extractViewFromParams { view =>
            listRevisionResources(RevisionID(uuid), offset, limit, View.defaultCollection ++ view)
          }
        }
      } ~ post {
        authenticateToken { user =>
          extractRawData { raw: Option[RawData] =>
            raw match {
              case Some(data) => createResource(RevisionID(uuid), user, data)
              case None       => respondWithError(badRequest("Cannot create a resource with empty data"))
            }
          }
        }
      }
    } ~ path("revisions" / JavaUUID / "resources" / Segment) { (uuid, instanceId) =>
      delete {
        authenticateToken { user =>
          TemporaryID.parse(instanceId) match {
            case Success(tid) => deleteResource(RevisionID(uuid), user, tid)
            case Failure(err) => respondWithError(err)
          }
        }
      }
    } ~ path("revisions" / JavaUUID / "resources" / "mt" / Segment) { (uuid, mtHash) =>
      getWithUri { implicit self: Uri =>
        extractOffsetLimit { (offset, limit) =>
          extractViewFromParams { view =>
            val target = ObjectRegistry.registeredMediaTypes.findHash(mtHash)
            // For now, support only the NIST list writer
            val writer = ObjectRegistry.writers.of(`application/vnd.nist.list`)
              .map(_.asInstanceOf[MassSpectraListWriter]).get
            if (target.exists(_ == `application/vnd.nist.list`)) {
              listRevisionResourcesRepresentation(RevisionID(uuid), offset, limit, writer,
                View.defaultCollection ++ view)
            } else respondWithError(StatusCodes.UnsupportedMediaType)
          }
        }
      }
    } ~ path("revisions" / JavaUUID / "commit") { (uuid) =>
      post {
        authenticateToken { user =>
          entity(as[JsObject]) { json =>
            val msg = json.fields.get("message") match {
              case Some(JsString(s)) => s
              case Some(x: JsValue)  => x.toString
              case None              => ""
            }
            commitRevision(RevisionID(uuid), user, msg)
          }
        }
      }
    } ~ path("revisions" / JavaUUID / "webhooks" / "test-publisher") { (uuid) =>
      post {
        testRevisionCommittedWebhook(RevisionID(uuid))
      }
    }
  }

  private def listRevisions(projectId: ProjectID, offset: Int, limit: Int): Route = {
    val (revisions, total) = DB.connect withSession { implicit session =>
      val query = RevisionManager.revisionsOf(projectId)
      val items = query.drop(offset).take(limit).buildColl[Vector].map { r =>
        r.toJson.asJsObject
          .addSelf(linkto(s"/revisions/${r.revisionId.urlSegment}"))
      }
      (items, query.size.run)
    }

    completeJsonApi {
      JsonApi.collection(revisions)
        .addMeta("total" -> total.toJson)
    }
  }

  private def showRevision(revId: RevisionID): Route =
    DB.connect withSession { implicit session =>
      RevisionManager.find(revId).firstOption match {
        case None => complete(StatusCodes.NotFound)
        case Some(rev) =>
          completeJsonApi {
            JsObject.empty.addData(rev.toJson.asJsObject)
          }
      }
    }

  private def createRevision(projectId: ProjectID, source: String): Route =
    DB.connect withSession { implicit session =>
      RevisionManager.create(projectId, source) match {
        case Success(rev) =>
          val location = linkto(s"/revisions/${rev.revisionId.urlSegment}")
          respondWithHeader(Location(location)) {
            completeJsonApi {
              val obj = rev.toJson.asJsObject.addSelf(location)
              JsObject.empty.addData(obj)
            }
          }
        case Failure(err) => respondWithError(err)
      }
    }

  private def deleteRevision(revId: RevisionID): Route =
    DB.connect withSession { implicit session =>
      RevisionManager.delete(revId) match {
        case Success(_)   => complete(StatusCodes.NoContent)
        case Failure(err) => respondWithError(err)
      }
    }

  private def listRevisionResources(revId: RevisionID, offset: Int, limit: Int, collectionView: View): Route =
    DB.connect withSession { implicit session =>
      RevisionManager.find(revId).firstOption map { rev =>
        val query = {
          if (!rev.isDraft) RegisteredResourceManager.resourcesOf(rev.revisionId).sortBy(_.date.desc)
          else UnregisteredResourceManager.resourcesOf(rev.revisionId)
        }
        val (resources, total) = {
          val items = query.drop(offset).take(limit).buildColl[Vector].map(convertSingle(_))
          (items, query.size.run)
        }

        val supported = Vector(`application/vnd.nist.list`)
        val representations: Vector[JsObject] = supported.map { mt =>
          var url = s"/revisions/${revId.urlSegment}/resources/mt/${mt.hash}"
          JsObject("href" -> linkto(url).toJson, "media_type" -> mt.toJson)
        }

        completeJsonApi {
          JsonApi.collection(resources)
            .addCollectionNavLinks(linkto(s"/revisions/${revId.urlSegment}/resources"), total, offset, limit)
            .addLinks(
              "create" -> {
                if (rev.isDraft) linkto(s"/revisions/${revId.urlSegment}/resources").toJson
                else JsNull
              },
              "representations" -> JsArray(representations)
            )
        }
      } getOrElse complete(StatusCodes.NotFound)
    }

  private def listRevisionResourcesRepresentation(revId: RevisionID, offset: Int, limit: Int,
    writer: MassSpectraListWriter, collectionView: View)(implicit self: Uri): Route =
    DB.connect withSession { implicit session =>
      RevisionManager.find(revId).firstOption map { rev =>
        val query = {
          if (!rev.isDraft) RegisteredResourceManager.resourcesOf(rev.revisionId)
          else UnregisteredResourceManager.resourcesOf(rev.revisionId)
        }
        val rawData: Vector[RawData] = query.drop(offset).take(limit).buildColl[Vector].map(_.data).flatten
        convertListToRepresentation(rawData, writer) match {
          case Success(data) =>
            val filename = s"${rev.projectId.owner}-${rev.projectId.projectName}-${revId}"
            respondWithRawData(data, Some(writer.registeredMediaType.computeFilename(filename)))
          case Failure(err) => respondWithError(err)
        }
      } getOrElse complete(StatusCodes.NotFound)
    }

  /** Commits all resources stored in this revision. */
  private def commitRevision(revId: RevisionID, user: User, message: String): Route =
    DB.connect withSession { implicit session =>
      RevisionManager.commit(revId, message) match {
        case Success(rev) =>
          import jp.riken.mnn.system.Events.RevisionCommitted
          eventHandler ! RevisionCommitted(rev)
          val location = linkto(s"/revisions/${rev.revisionId.urlSegment}")
          respondWithHeader(Location(location)) {
            completeJsonApi {
              val obj = rev.toJson.asJsObject.addSelf(location)
              JsObject.empty.addData(obj)
            }
          }
        case Failure(err) => respondWithError(err)
      }
    }

  /** Creates new resource(s) and adds it to this revision. */
  private def createResource(revId: RevisionID, user: User, data: RawData): Route =
    DB.connect withSession { implicit session =>
      val query = for {
        saved <- UnregisteredResourceManager.create(data)
        revs <- Success(saved.map(r => (r.temporaryId, Revision.Create)))
        _ <- RevisionManager.registerRevisions(revId, revs)
      } yield saved

      query match {
        case Success(resources) => complete(StatusCodes.Created)
        case Failure(err)       => respondWithError(err)
      }
    }

  /** Removes the resource from this revision, and deletes it. */
  private def deleteResource(revId: RevisionID, user: User, temporaryId: TemporaryID): Route =
    DB.connect withSession { implicit session =>
      val relations = TableQuery[RevisionResourcesRelation.SlickModel]
      val relation = relations.filter(rel => rel.instanceId === temporaryId.asInstanceOf[InstanceID])
      if (relation.exists.run) {
        val query = for {
          _ <- Try(relation.delete)
          _ <- UnregisteredResourceManager.delete(temporaryId)
        } yield ()

        query match {
          case Success(_)   => complete(StatusCodes.NoContent)
          case Failure(err) => respondWithError(err)
        }
      } else respondWithError(notFound("The resource is not part of the revision"))
    }

  private def testRevisionCommittedWebhook(revId: RevisionID): Route =
    DB.connect withSession { implicit session =>
      RevisionManager.find(revId).firstOption match {
        case Some(rev) =>
          complete {
            import jp.riken.mnn.system.Events.RevisionCommitted
            eventHandler ! RevisionCommitted(rev)
            StatusCodes.NoContent
          }
        case None => complete(StatusCodes.NotFound)
      }
    }
}
