package jp.riken.mnn.api

import jp.riken.mnn.database._
import jp.riken.mnn.types.User
import org.joda.time.DateTime
import scala.concurrent.{ ExecutionContext, Future }
import scala.slick.driver.H2Driver.simple._
import spray.http.StatusCodes.Unauthorized
import spray.routing._
import spray.routing.authentication.{ BasicAuth, UserPass }
import jp.riken.mnn.util.SecurityTools

trait SecurityDirectives extends Directives with DBTools {
  import scala.concurrent.ExecutionContext.Implicits.global

  def hashPasswordAuthenticator(userpass: Option[UserPass]): Future[Option[(User, (String, DateTime))]] = {
    Future {
      userpass flatMap { up =>
        DB.connect withSession { implicit session =>
          UserManager.find(up.user).firstOption filter { user =>
            val hashed = SecurityTools.hashPassword(up.pass.getBytes, user.salt)
            user.password.sameElements(hashed)
          }
        }
      } map (user => (user, SecurityTools.generateToken(user)))
    }
  }

  def authenticateUser(f: (User, (String, DateTime)) => Route): Route = {
    authenticate(BasicAuth(
      hashPasswordAuthenticator _,
      realm = "mnn 0.1 - secured resource"
    )) { result =>
      f(result._1, result._2)
    }
  }

  def jwtTokenHeader(f: String => Route): Route = {
    optionalHeaderValueByName("Authorization") {
      _ map { header =>
        header.split(" ").toList match {
          case "Bearer" :: token :: Nil => f(token)
          case _                        => complete(Unauthorized)
        }
      } getOrElse complete(Unauthorized)
    }
  }

  def authenticateToken(f: User => Route): Route = jwtTokenHeader { token =>
    SecurityTools.validate(token) match {
      case Some((user, _)) => f(user)
      case None            => complete(Unauthorized)
    }
  }
}
