package jp.riken.mnn.api

import jp.riken.mnn.system.{ CoreActors, Core }
import jp.riken.mnn.Settings
import jp.riken.mnn.database.DB
import jp.riken.mnn.json._
import jp.riken.mnn.util.SecurityTools
import org.joda.time.DateTimeZone
import org.joda.time.format.ISODateTimeFormat
import scala.concurrent.ExecutionContext
import scala.language.implicitConversions
import spray.http.CacheDirectives._
import spray.http.HttpHeaders.`Cache-Control`
import spray.http.MediaTypes.`application/json`
import spray.http._
import spray.json._
import spray.routing._

trait RootService extends HttpService with ApiService with CoreActors with Core with HalJsObjectTools {

  implicit val ec: ExecutionContext = system.dispatcher

  val routes = respondWithHeaders(corsHeaders.toList) {
    options {
      complete(StatusCodes.NoContent)
    } ~
      new ProjectService(eventHandler).route ~
      new ResourceService(eventHandler).route ~
      new RevisionService(eventHandler).route ~
      new UserService(eventHandler).route ~
      new WebhookService(eventHandler).route ~
      rootRoute ~ authRoute ~ docsRoute
  }

  private def rootRoute = path("") { // root path
    getWithUri { implicit self: Uri =>
      DB.connect withSession { implicit session =>
        completeJsonApi {
          val base = JsonApi.resource("mnn:node", Settings.node.namespace)
            .addLinks(
              "mnn:auth" -> linkto("/auth/token").toJson,
              "mnn:docs" -> linkto("/docs").toJson,
              "projects" -> linkto("/projects").toJson,
              "resources" -> linkto("/resources").toJson,
              "users" -> linkto("/users").toJson
            )
            .addAttributes(
              "href" -> (Settings.api.uri.host + Settings.api.uri.prefix).toJson,
              "node_classification" -> Settings.node.classification.toJson,
              "node_namespace" -> Settings.node.classification.toJson
            )

          JsObject.empty.addData(base)
        }
      }
    }
  }

  private def authRoute = pathPrefix("auth" / "token") {
    post {
      authenticateUser { (user, token) =>
        respondWithMediaType(`application/json`) {
          complete {
            JsObject(
              "token" -> JsString(token._1),
              "exp" -> JsString(token._2.toString(ISODateTimeFormat.dateTime.withZone(DateTimeZone.UTC)))
            )
          }
        }
      }
    } ~ delete {
      authenticateToken { token =>
        SecurityTools.invalidate(token)
        complete(StatusCodes.NoContent)
      }
    }
  }

  val `text/markdown` = MediaTypes.register(MediaType.custom(
    mainType = "text",
    subType = "markdown",
    compressible = true,
    binary = false,
    fileExtensions = Seq("md", "mdown", "markdown")
  ))

  private def docsRoute = (pathPrefix("docs") & get) {
    pathEnd {
      redirect("/docs/about.md", StatusCodes.PermanentRedirect)
    } ~
      getFromResourceDirectory("docs")
  }
}
