package jp.riken.mnn.api

import akka.actor.ActorRef
import scala.concurrent.ExecutionContext
import jp.riken.mnn.database._
import jp.riken.mnn.json._
import jp.riken.mnn.json.SprayJsonUtils.valueToString
import jp.riken.mnn.types._
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }
import spray.http._
import spray.http.HttpHeaders.Location
import spray.json._
import spray.routing._

class UserService(eventHandler: ActorRef)(implicit ec: ExecutionContext) extends ApiService {

  val route = {
    path("users") {
      get {
        extractOffsetLimit { (offset, limit) =>
          listUsers(offset, limit)
        }
      } ~ post {
        requestUri { implicit self: Uri =>
          //authenticateToken { user =>
          entity(as[JsObject]) { json =>
            json.getFields("username", "password", "name", "email") match {
              case Seq(JsString(username), JsString(password), JsString(name), JsString(email)) =>
                val userId = UserID.local(username)
                UserID.validate(userId)
                createUser(userId, password, username, email)
              case _ => respondWithError(incompletePayloadFields)
            }
          }
        }
      }
    } ~ path("users" / Segment) { username: String =>
      get {
        showUser(UserID.local(username))
      } ~ put {
        authenticateToken { user =>
          entity(as[JsObject]) { json =>
            json.getFields("name", "email", "password") match {
              case Seq(JsString(name), JsString(email), JsString(password)) =>
                editUser(UserID.local(username), name, email, password)
            }
          }
        }
      } ~ delete {
        authenticateToken { user =>
          deleteUser(UserID.local(username))
        }
      }
    }
  }

  private def listUsers(offset: Int, limit: Int): Route = {
    val (users, total) = DB.connect withSession { implicit session =>
      val query = UserManager.elems.sortBy(_.username)
      val items = query.drop(offset).take(limit).buildColl[Vector].map {
        u => u.toJson.asJsObject
      }
      (items, query.size.run)
    }

    completeJsonApi {
      JsonApi.collection(users)
        .addCollectionNavLinks(linkto("/users"), total, offset, limit)
        .addLinks("create" -> linkto("/users").toJson)
    }
  }

  private def createUser(userId: UserID, password: String, name: String, email: String): Route =
    DB.connect withSession { implicit session =>
      UserManager.create(userId, password.getBytes, name, email) match {
        case Success(user) =>
          respondWithHeader(Location(linkto(s"/users/${user.username}"))) {
            complete(StatusCodes.Created)
          }
        case Failure(err) => respondWithError(err)
      }
    }

  private def showUser(userId: UserID): Route =
    DB.connect withSession { implicit session =>
      UserManager.find(userId).firstOption.map { user =>
        val projects: Vector[JsObject] = ProjectManager.projectsOf(userId)
          .sortBy(_.projectName).buildColl[Vector].map(_.toJson.asJsObject)
        completeJsonApi {
          JsObject.empty.addData(
            user.toJson.asJsObject.addRelationships(
              "projects" -> projects.toJson
            )
          )
        }
      } getOrElse respondWithError(notFound("Supplied user does not exist."))
    }

  private def editUser(userId: UserID, password: String, name: String, email: String) =
    DB.connect withSession { implicit session =>
      UserManager.edit(userId, password.getBytes, name, email) match {
        case Success(_)   => complete(StatusCodes.NoContent)
        case Failure(err) => respondWithError(err)
      }
    }

  private def deleteUser(userId: UserID): Route =
    DB.connect withSession { implicit session =>
      UserManager.delete(userId) match {
        case Success(_)   => complete(StatusCodes.NoContent)
        case Failure(err) => respondWithError(err)
      }
    }
}
