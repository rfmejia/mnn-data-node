package jp.riken.mnn.api

import akka.actor.ActorRef
import scala.concurrent.ExecutionContext
import jp.riken.mnn.Settings
import jp.riken.mnn.database._
import jp.riken.mnn.json._
import jp.riken.mnn.types._
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }
import spray.http.HttpHeaders.Location
import spray.http._
import spray.json._
import spray.routing._

class ProjectService(eventHandler: ActorRef)(implicit ec: ExecutionContext) extends ApiService {

  val route = {
    path("projects") {
      getWithUri { implicit self: Uri =>
        parameters('offset.as[Int] ? 0, 'limit.as[Int] ? 50) { (offset, limit) =>
          listProjects(offset, limit)
        }
      } ~ post {
        authenticateToken { user =>
          entity(as[JsObject]) { json =>
            createProject(user, json)
          }
        }
      }
    } ~ path("projects" / Segment / Segment) { (owner, projectName) =>
      getWithUri { implicit self: Uri =>
        parameters('offset.as[Int] ? 0, 'limit.as[Int] ? 50) { (offset, limit) =>
          val projectId = ProjectID.local(owner, projectName)
          ProjectID.validate(projectId)
          showProject(projectId, offset, limit)
        }
      } ~ put {
        requestUri { implicit self: Uri =>
          authenticateToken { user =>
            entity(as[JsObject]) { json =>
              val projectId = ProjectID.local(owner, projectName)
              ProjectID.validate(projectId)
              editProject(projectId, user, json)
            }
          }
        }
      } ~ delete {
        authenticateToken { user =>
          requestUri { implicit self: Uri =>
            val projectId = ProjectID.local(owner, projectName)
            ProjectID.validate(projectId)
            deleteProject(projectId)
          }
        }
      }
    } ~ new ProjectResourceService(eventHandler).route ~ new RevisionService(eventHandler).route
  }

  private def listProjects(offset: Int, limit: Int)(implicit self: Uri): Route = {
    val (projects, total) = DB.connect withSession { implicit session =>
      val query = ProjectManager.elems.sortBy(_.projectName)
      val items = query.drop(offset).take(limit).buildColl[Vector].map(_.toJson.asJsObject)
      (items, query.size.run)
    }

    completeJsonApi {
      JsonApi.collection(projects)
        .addCollectionNavLinks(linkto("/projects"), total, offset, limit)
        .addLinks("create" -> linkto("/projects").toJson)
    }
  }

  private def createProject(user: User, json: JsObject): Route = {
    (
      json.fields.get("description"),
      json.fields.get("project_name"),
      json.fields.get("readme"),
      json.fields.get("policy")
    ) match {
        case (Some(JsString(description)), Some(JsString(projectName)), Some(JsString(readme)), Some(JsString(policy))) =>
          DB.connect withSession { implicit session =>
            ProjectManager.create(user.userId, projectName, Option(description), Option(readme), policy) match {
              case Success(project) =>
                respondWithHeader(Location(linkto(s"/projects/${project.projectId.urlSegment}"))) {
                  complete(StatusCodes.Created)
                }
              case Failure(err) => respondWithError(err)
            }
          }
        case _ => respondWithError(incompletePayloadFields)
      }
  }

  private def showProject(projectId: ProjectID, offset: Int, limit: Int)(implicit self: Uri) =
    DB.connect withSession { implicit session =>
      ProjectManager.find(projectId).firstOption match {
        case Some(project) =>
          completeJsonApi {
            JsObject.empty
              .addData(project.toJson.asJsObject)
              .addLinks(
                "resources" -> linkto(s"/projects/${projectId.urlSegment}/resources").toJson,
                "revisions" -> linkto(s"/projects/${projectId.urlSegment}/revisions").toJson,
                "webhooks" -> linkto(s"/projects/${projectId.urlSegment}/webhooks").toJson
              )
          }

        case None => respondWithError(notFound("Supplied user or project does not exist."))
      }
    }

  private def editProject(projectId: ProjectID, user: User, json: JsObject)(implicit self: Uri): Route =
    (
      json.fields.get("description"),
      json.fields.get("readme"),
      json.fields.get("policy")
    ) match {
        case (Some(JsString(description)), Some(JsString(readme)), Some(JsString(policy))) =>
          DB.connect withSession { implicit session =>
            val existing = ProjectManager.find(projectId).firstOption
            val updated = Project(projectId, projectId.asUserID, projectId.projectName, Option(description), Option(readme), policy)
            ProjectManager.edit(updated) match {
              case Success(user) => complete(StatusCodes.NoContent)
              case Failure(err)  => respondWithError(err)
            }
          }
        case _ => respondWithError(incompletePayloadFields)
      }

  private def deleteProject(projectId: ProjectID)(implicit self: Uri): Route =
    DB.connect withSession { implicit session =>
      ProjectManager.delete(projectId) match {
        case Success(num) => complete(StatusCodes.NoContent)
        case Failure(err) => respondWithError(err)
      }
    }
}
