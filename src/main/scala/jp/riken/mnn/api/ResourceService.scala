package jp.riken.mnn.api

import akka.actor.ActorRef
import scala.concurrent.ExecutionContext
import jp.riken.mnn.ObjectRegistry
import jp.riken.mnn.ObjectRegistry.registeredMediaTypes._
import jp.riken.mnn.database._
import jp.riken.mnn.json._
import jp.riken.mnn.types._
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }
import spray.http._
import spray.json._
import spray.routing._

class ResourceService(eventHandler: ActorRef)(implicit ec: ExecutionContext) extends ApiService {

  val route = {
    path("resources") {
      getWithUri { implicit self: Uri =>
        extractOffsetLimit { (offset, limit) =>
          extractViewFromParams { v =>
            listResources(offset, limit, View.defaultCollection ++ v)
          }
        }
      }
    } ~ path("resources" / Segment) { id: String =>
      getWithUri { implicit self: Uri =>
        extractViewFromParams { v =>
          ResourceID.parse(id) match {
            case Success(i: PlainID) => extractOffsetLimit((o, l) => serveVersions(i, o, l,
              View.defaultCollection ++ v))
            case Success(i: InstanceID) => serveResource(i, v)
            case Failure(err)           => respondWithError(err)
          }
        }
      } ~ put {
        TODO
      } ~ delete {
        authenticateToken { user =>
          TemporaryID.parse(id) match {
            case Success(i) => deleteResource(i, user)
            case Failure(_) => respondWithError(badRequest("Invalid request: can only delete temporary resources"))
          }
        }
      }
    } ~ path("resources" / Segment / "mt" / Segment) { (id, mtHash) =>
      getWithUri { implicit self: Uri =>
        extractViewFromParams { v =>
          (InstanceID.parse(id), ObjectRegistry.registeredMediaTypes.findHash(mtHash)) match {
            case (Success(i), Some(mt)) => serveRepresentation(i, mt, v)
            case (Failure(err), _)      => respondWithError(err)
            case (_, None)              => respondWithError(StatusCodes.UnsupportedMediaType)
          }
        }
      }
    } ~ path("search") {
      getWithUri { implicit self: Uri =>
        parameter('q) { query =>
          extractOffsetLimit { (offset, limit) =>
            extractViewFromParams { view =>
              searchResources(query, offset, limit, View.defaultCollection ++ view)
            }
          }
        }
      }
    }
  }

  private def listResources(offset: Int, limit: Int, view: View)(implicit self: Uri): Route =
    DB.connect withSession { implicit session =>
      val query = RegisteredResourceManager.elems.sortBy(_.date.desc)

      val (resources, total) = {
        val items = query.drop(offset).take(limit).buildColl[Vector].map(convertSingle(_))
        (items, query.size.run)
      }

      completeJsonApi {
        JsonApi.collection(resources)
          .addCollectionNavLinks(linkto(s"/resources"), total, offset, limit)
      }
    }

  private def serveVersions(id: PlainID, offset: Int, limit: Int, view: View)(implicit self: Uri): Route = {
    val (versions, total, latest) = DB.connect withSession { implicit session =>
      val query = RegisteredResourceManager.versionsOf(id).sortBy(_.versionedId.desc)
      val items = query.drop(offset).take(limit).buildColl[Vector].map(convertSingle(_))
      val latest = RegisteredResourceManager.latestResource(id)
      (items, query.size.run, latest)
    }

    completeJsonApi {
      JsonApi.collection(versions)
        .addCollectionNavLinks(linkto(s"/resources/${id.urlSegment}"), total, offset, limit)
        .addMeta("latest" -> { if (latest.isDefined) latest.get.versionedId.toJson else JsNull })
    }
  }

  private def serveResource(id: InstanceID, view: View)(implicit self: Uri): Route =
    DB.connect withSession { implicit session =>
      val resOption = id match {
        case vid: VersionedID => RegisteredResourceManager.find(vid).firstOption
        case tid: TemporaryID => UnregisteredResourceManager.find(tid).firstOption
      }
      resOption match {
        case Some(resource) =>
          completeJsonApi {
            JsObject.empty
              .addData(convertSingle(resource))
          }
        case None => respondWithError(StatusCodes.NotFound)
      }
    }

  private def serveRepresentation(id: InstanceID, target: RegisteredMediaType, view: View)(implicit self: Uri): Route =
    DB.connect withSession { implicit session =>
      val resOption = id match {
        case vid: VersionedID => RegisteredResourceManager.find(vid).firstOption
        case tid: TemporaryID => UnregisteredResourceManager.find(tid).firstOption
      }
      resOption match {
        case Some(resource) =>
          resource.data match {
            case Some(source) =>
              if (source.mediaType == target) {
                val filename = target.computeFilename(s"${resource.id}")
                respondWithRawData(source, Some(filename))
              } else {
                val mappers = ObjectRegistry.mappers.of(source.mediaType, target)
                if (mappers.isEmpty) respondWithError(StatusCodes.UnsupportedMediaType, "No matching mappers available")
                else {
                  val filename = target.computeFilename(s"${resource.id}")
                  convertSingleToRepresentation(source, mappers.head) match {
                    case Success(result) => respondWithRawData(result, Some(filename))
                    case Failure(err)    => respondWithError(err)
                  }
                }
              }
            case None =>
              respondWithError(StatusCodes.PreconditionFailed, "The resource contains no data to transform")
          }
        case None => respondWithError(StatusCodes.NotFound)
      }
    }

  private def searchResources(query: String, offset: Int, limit: Int, view: View)(implicit self: Uri) =
    DB.connect withSession { implicit session =>
      RegisteredResourceManager.search(query, offset, limit) match {
        case Success(result) =>
          respondWithHalJson {
            convertTextSearchResult(result, view, offset, limit)
          }
        case Failure(err) => respondWithError(err)
      }
    }

  private def editResource(id: TemporaryID, user: User): Route = ???

  private def deleteResource(id: TemporaryID, user: User): Route = ???
  // user must own the project to delete the resource from a revision
}
