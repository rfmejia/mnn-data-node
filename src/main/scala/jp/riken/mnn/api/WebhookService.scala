package jp.riken.mnn.api

import akka.actor.ActorRef
import scala.concurrent.ExecutionContext
import java.util.UUID
import jp.riken.mnn.database.{ DB, WebhookManager }
import jp.riken.mnn.json._
import jp.riken.mnn.types._
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }
import spray.http.MediaTypes._
import spray.http._
import spray.http.HttpHeaders.Location
import spray.json._
import spray.routing._

class WebhookService(eventHandler: ActorRef)(implicit ec: ExecutionContext) extends ApiService {

  val route = {
    path("projects" / Segment / Segment / "webhooks") { (username, projectName) =>
      getWithUri { implicit self: Uri =>
        extractOffsetLimit { (offset, limit) =>
          val projectId = ProjectID.local(username, projectName)
          ProjectID.validate(projectId)
          listHooks(projectId, offset, limit)
        }
      } ~ post {
        requestUri { implicit self: Uri =>
          authenticateToken { user =>
            entity(as[JsObject]) { json =>
              json.getFields("url", "event_type") match {
                case Seq(JsString(url), JsString(event_type)) =>
                  val projectId = ProjectID.local(username, projectName)
                  val params = for {
                    p <- Try(ProjectID.validate(projectId))
                    u <- Try(Uri(url))
                  } yield (p, u)

                  params match {
                    case Success((_projectId, _url)) => createHook(user.userId, _projectId, _url, event_type)
                    case Failure(err)                => respondWithError(err)
                  }
                case _ => respondWithError(incompletePayloadFields)
              }
            }
          }
        }
      }
    } ~ path("webhooks" / JavaUUID) { uuid =>
      getWithUri { implicit self: Uri =>
        showHook(uuid)
      } ~ put {
        requestUri { implicit self: Uri =>
          authenticateToken { user =>
            entity(as[JsObject]) { json =>
              json.getFields("url", "event_type") match {
                case Seq(JsString(url), JsString(eventType)) =>
                  val params = for { u <- Try(Uri(url)) } yield u
                  params match {
                    case Success(_url) => editHook(uuid, _url, eventType)
                    case Failure(err)  => respondWithError(err)
                  }
                case _ => respondWithError(badRequest("Webhook requires the follwing fields: `url`, `event_type`"))
              }
            }
          }
        }
      } ~ delete {
        requestUri { implicit self: Uri =>
          authenticateToken { user =>
            deleteHook(uuid)
          }
        }
      }
    } ~ path("webhooks" / "test-subscriber") {
      post {
        extract(_.request.headers) { headers =>
          entity(as[String]) { body =>
            println("Received POST request from webhook")
            println(headers.mkString("\n") + "\n")
            println(body)
            complete(StatusCodes.NoContent)
          }
        }
      }
    }
  }

  private def listHooks(projectId: ProjectID, offset: Int, limit: Int)(implicit self: Uri): Route = {
    val (hooks, total): (Vector[JsObject], Int) =
      DB.connect withSession { implicit session =>
        val query = WebhookManager.webhooksOf(projectId)
        val items = query.drop(offset).take(limit).buildColl[Vector].map {
          w => w.toJson.asJsObject
        }
        (items, query.length.run)
      }

    completeJsonApi {
      JsonApi.collection(hooks)
        .addCollectionNavLinks(linkto("/webhooks"), total, offset, limit)
        .addLinks("create" -> linkto(s"/users/${projectId.urlSegment}/webhooks").toJson)
    }
  }

  private def createHook(userId: UserID, projectId: ProjectID, url: Uri,
    event_type: String)(implicit self: Uri): Route =
    DB.connect withSession { implicit session =>
      WebhookManager.create(userId, projectId, url, event_type) match {
        case Success(hook) =>
          respondWithHeader(Location(linkto(s"/webhooks/${hook.uuid}"))) {
            complete(StatusCodes.Created)
          }
        case Failure(err) => respondWithError(err)
      }
    }

  private def showHook(id: UUID)(implicit self: Uri): Route = {
    DB.connect withSession { implicit session =>
      WebhookManager.find(id).firstOption match {
        case Some(hook) => completeJsonApi(hook.toJson)
        case None       => respondWithError(StatusCodes.NotFound)
      }
    }
  }

  private def editHook(id: UUID, url: Uri, eventType: String)(implicit self: Uri): Route =
    DB.connect withSession { implicit session =>
      WebhookManager.edit(id, url, eventType) match {
        case Success(updatedHook) => completeJsonApi(updatedHook.toJson)
        case Failure(err)         => respondWithError(err)
      }
    }

  private def deleteHook(id: UUID)(implicit self: Uri): Route = {
    DB.connect withSession { implicit session =>
      WebhookManager.delete(id) match {
        case Success(_)   => complete(StatusCodes.NoContent)
        case Failure(err) => respondWithError(err)
      }
    }
  }
}
