package jp.riken.mnn.api

import jp.riken.mnn.ObjectRegistry
import jp.riken.mnn.ObjectRegistry.registeredMediaTypes._
import jp.riken.mnn.database.DBTools
import jp.riken.mnn.json._
import jp.riken.mnn.mediatypes._
import jp.riken.mnn.types.{ ID, RawData }
import jp.riken.mnn.util.LinkingTools
import jp.riken.mnn.{ Credentials, ErrorResponses }
import scala.util.{ Try, Success, Failure }
import spray.http.HttpHeaders._
import spray.http.HttpMethods._
import spray.http.MediaTypes.`application/json`
import spray.http.Uri._
import spray.http._
import spray.httpx.SprayJsonSupport
import spray.httpx.unmarshalling.BasicUnmarshallers
import spray.json._
import spray.routing._
import spray.routing.authentication.{ BasicAuth, UserPass }

trait ApiService extends Directives
    with BasicUnmarshallers // Need to import so that entity(as[Array[Byte]]) is not extracted via JSON
    with DBTools
    with ErrorResponses
    with HalJsObjectTools
    with LinkingTools
    with RepresentationTools
    with SecurityDirectives
    with SprayJsonSupport {

  val TODO: Route = complete(StatusCodes.NotImplemented)

  val corsHeaders = Vector(
    `Access-Control-Allow-Origin`(AllOrigins),
    `Access-Control-Allow-Methods`(GET, POST, PUT, OPTIONS, DELETE),
    `Access-Control-Allow-Headers`("Origin, X-Requested-With, Content-Type, Accept, Accept-Encoding, " +
      "Accept-Language, Host, Referer, User-Agent, Authorization")
  )

  def getWithUri(f: Uri => Route): Route = get(requestUri(uri => f(uri)))

  val extractContentType = headerValuePF {
    case `Content-Type`(ct) => ct
  }

  def extractContentTags(f: Option[Set[String]] => Route) =
    optionalHeaderValueByName("Content-Tags") { tags =>
      val t: Option[Set[String]] = tags.map { tag =>
        tag.split(",").map(_.trim.toLowerCase).toSet
      }
      f.apply(t)
    }

  def extractRawData(f: Option[RawData] => Route): Route = {
    entity(as[Array[Byte]]) { bytes =>
      if (bytes.isEmpty) f.apply(None)
      else extractContentType { ct =>
        ObjectRegistry.registeredMediaTypes.find(ct.toString) map { mt =>
          RawData(mt, bytes) match {
            case Success(raw) => f.apply(Some(raw))
            case Failure(err) => respondWithError(StatusCodes.BadRequest, err.getMessage)
          }
        } getOrElse respondWithError(unsupportedMediaType(s"Entity media type '${ct}' is not registered"))
      }
    }
  }

  def extractOffsetLimit(f: (Int, Int) => Route): Route =
    parameters('offset.as[Int] ? 0, 'limit.as[Int] ? 50) { (offset, limit) =>
      val off = if (offset >= 0) offset else 0
      val lim = if (limit > 0) limit else 50
      f(off, lim)
    }

  def extractViewFromParams(f: View => Route): Route = {
    parameterMap { params =>
      f(View.fromParams(params))
    }
  }

  def completeJsonApi(value: JsValue): Route = value match {
    case obj: JsObject =>
      respondWithMediaType(`application/vnd.api+json`.underlying) {
        requestUri { self: Uri =>
          complete(obj.addSelf(self).compactPrint)
        }
      }
    case arr: JsArray =>
      respondWithMediaType(`application/json`) {
        complete(arr)
      }
    case _ => respondWithError(
      serverError(s"Attempting to respond with illegal JSON. Response body: ${value.prettyPrint}")
    )
  }

  def respondWithHalJson(obj: HalJsObject): Route =
    respondWithHalJson(obj.asJsValue)

  def respondWithHalJson(obj: JsObject): Route =
    respondWithMediaType(`application/hal+json`.underlying) {
      complete(obj.compactPrint)
    }

  def respondWithError(status: StatusCode, msgs: Vector[String] = Vector.empty): Route =
    requestUri { self =>
      respondWithStatus(status) {
        if (msgs.isEmpty) complete { "" }
        else {
          respondWithHalJson {
            val base = HalJsObject.create(linkto(self.path.toString, self.query))
              .withField("profile", JsString("mnn:error"))
            val errors = msgs.foldLeft(Vector.empty[JsString]) { (acc, msg) =>
              acc :+ JsString(msg)
            }
            base.withField("_errors", JsArray(errors))
          }
        }
      }
    }

  def respondWithError(status: StatusCode, msg: String): Route =
    respondWithError(status, Vector(msg))

  def respondWithError(t: Throwable): Route = {
    t.printStackTrace()
    t match {
      case ire: IllegalRequestException    => respondWithError(ire.status, ire.getMessage())
      case rpe: RequestProcessingException => respondWithError(rpe.status, rpe.getMessage())
      case _                               => respondWithError(StatusCodes.InternalServerError, t.getMessage())
    }
  }

  def respondWithRawData(data: RawData, filename: Option[String] = None): Route =
    data.getData match {
      case Success(d) =>
        val params: Map[String, String] = Map.empty ++ filename.map(f => ("filename", f))
        respondWithHeader(`Content-Disposition`("attachment", params)) {
          respondWithMediaType(data.mediaType.underlying) {
            complete(new String(d, "UTF-8"))
          }
        }
      case Failure(err) => respondWithError(err)
    }
}
