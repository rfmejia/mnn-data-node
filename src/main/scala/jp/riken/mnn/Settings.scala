package jp.riken.mnn

import akka.util.Timeout
import com.typesafe.config.{ Config, ConfigFactory }
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Properties
import spray.http.Uri

/**
  * Application-wide settings. Load settings at startup time to catch missing or mistyped
  * values as early as possible.
  */
object Settings {
  val c = ConfigFactory.load()

  object api {
    object uri {
      val host: Uri = Uri(c.getString("api.uri.host"))
      val prefix: Uri.Path = Uri.Path(c.getString("api.uri.prefix"))
      val absolute: Boolean = c.getBoolean("api.uri.absolute")
    }
  }

  object eventHandler {
    val maxRetries: Int = c.getInt("event_handler.max_retries")
  }

  object node {
    val namespace: String = c.getString("node.namespace")
    val classification: String = c.getString("node.classification")
  }

  object project {
    val resourcePath = c.getString("project.resourcePath")

    // Check for the existence but do not save the following values
    c.getString("project.driver")
    c.getString("project.url")
  }

  object indexing {
    val path = c.getString("indexing.path")
    val duration: FiniteDuration = Duration.create(c.getString("indexing.interval")) match {
      case fd: FiniteDuration => fd
      case _                  => throw new IllegalArgumentException(s"Supplied indexing interval is not a finite duration")
    }
  }

  object akka {
    val interface: String = c.getString("akka.interface")
    val port: Int = Properties.envOrElse("PORT", c.getString("akka.port")).toInt
    val timeout: Timeout = Duration.create(c.getString("akka.timeout")) match {
      case fd: FiniteDuration => Timeout(fd)
      case _                  => throw new IllegalArgumentException(s"Supplied indexing interval is not a finite duration")
    }
  }

  object jwt {
    val algorithm: String = c.getString("jwt.algorithm")
    val secret: String = c.getString("jwt.secret")
  }

  def elems[_ <: Any]: Map[String, _] = Map(
    "akka.interface" -> akka.interface,
    "akka.port" -> akka.port,
    "akka.timeout" -> akka.timeout,
    "api.uri.absolute" -> api.uri.absolute,
    "api.uri.host" -> api.uri.host,
    "api.uri.prefix" -> api.uri.prefix,
    "event_handler.max_retries" -> eventHandler.maxRetries,
    "indexing.duration" -> indexing.duration,
    "indexing.path" -> indexing.path,
    "jwt.algorithm" -> jwt.algorithm,
    "node.classification" -> node.classification,
    "node.namespace" -> node.namespace,
    "project.resource_path" -> project.resourcePath
  )
}
