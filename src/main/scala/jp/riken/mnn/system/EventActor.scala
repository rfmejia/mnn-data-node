package jp.riken.mnn.system

import akka.actor.{ Actor, ActorSystem, Props }
import jp.riken.mnn.database._
import jp.riken.mnn.json._
import jp.riken.mnn.types._
import jp.riken.mnn.util.LinkingTools
import org.joda.time.DateTime
import scala.concurrent.ExecutionContext
import scala.slick.driver.H2Driver.simple._
import spray.client.pipelining._
import spray.http.Uri
import spray.httpx.SprayJsonSupport
import spray.json.AdditionalFormats
import spray.json._

class EventActor(maxRetries: Int) extends Actor
    with DBTools
    with HttpClient
    with ExtendedJsonProtocol
    with SprayJsonSupport
    with AdditionalFormats
    with LinkingTools {

  implicit val system: ActorSystem = context.system
  implicit val ec: ExecutionContext = system.dispatcher

  def receive: Receive = {
    case Events.RevisionCommitted(revision, payload) =>
      val eventType = "revision_committed"

      val hooks: Vector[Webhook] = DB.connect withSession { implicit session =>
        WebhookManager.elems.filter(h => h.publisher === revision.projectId && h.eventType === eventType).list.toVector
      }

      val total = DB.connect withSession { implicit session =>
        val relations = TableQuery[RevisionResourcesRelation.SlickModel]
        relations.filter(_.revisionId === revision.revisionId).size.run
      }
      val limit = 50
      val pages = (0 to total by limit).map((_, limit))

      val downloadLinks: Vector[Uri] =
        pages.toVector.map {
          case (offset, limit) =>
            linkto(s"/revisions/${revision.revisionId.urn}/resources")
              .withQuery(Uri.Query("offset" -> offset.toString, "limit" -> limit.toString))
        }

      val hash = jp.riken.mnn.ObjectRegistry.registeredMediaTypes.`application/vnd.nist.list`.hash
      val nistListLinks: Vector[Uri] =
        pages.toVector.map {
          case (offset, limit) =>
            linkto(s"/revisions/${revision.revisionId.urn}/resources/mt/${hash}")
              .withQuery(Uri.Query("offset" -> offset.toString, "limit" -> limit.toString))
        }

      val data = payload.getOrElse(JsObject.empty)
        .addAttributes("event_type" -> eventType.toJson)
        .addRelationships("revision" -> revision.toJson)
        .addLinks(
          "resources" -> JsObject(
            "href" -> JsArray(downloadLinks.map(l => JsString(l.toString)))
          ),
          "representations" -> JsArray(
            JsObject(
              "media_type" -> "application/vnd.nist.list".toJson,
              "href" -> JsArray(nistListLinks.map(l => JsString(l.toString)))
            )
          )
        )

      val _payload = JsObject("type" -> "mnn:event".toJson)
        .addData(data)
        .addMeta(
          "version" -> 1.toJson,
          "generated_on" -> DateTime.now.toJson
        )

      val requests = hooks.map(hook => Post(hook.uri.toString, _payload))
      sendRequests(requests)

      println(s"[${eventType}] Notified ${hooks.size} webhooks of ${revision.projectId}")
  }
}

object EventActor {
  def props(maxRetries: Int): Props = Props(new EventActor(maxRetries))
}

object Events {
  case class RevisionCommitted(revision: Revision, payload: Option[JsObject] = None)
}
