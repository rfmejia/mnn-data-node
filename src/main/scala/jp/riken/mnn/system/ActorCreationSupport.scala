package jp.riken.mnn.system

import akka.actor.{ ActorContext, ActorRef, Props }

trait ActorCreationSupport {
  def createChild(props: Props, name: String): ActorRef
}

trait ActorContextCreationSupport extends ActorCreationSupport {
  def context: ActorContext
  def createChild(props: Props, name: String): ActorRef = context.actorOf(props, name)
}
