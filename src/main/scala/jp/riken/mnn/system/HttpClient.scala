package jp.riken.mnn.system

import akka.actor.ActorSystem
import akka.pattern.ask
import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Try, Success, Failure }
import spray.can.Http
import spray.client.pipelining._
import spray.http._
import spray.httpx.encoding.{ Gzip, Deflate }

trait HttpClient {
  def sendRequest(request: HttpRequest)(implicit system: ActorSystem): Future[Try[HttpResponse]] = {
    import system.dispatcher

    val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
    // TODO: encode(Gzip) ~> sendReceive ~> decode(Deflate)

    val response = pipeline(request)
    response.map(s => Success(s)).recover({ case f => Failure(f) })
  }

  def sendRequests(requests: Seq[HttpRequest])(implicit
    ec: ExecutionContext,
    system: ActorSystem): Future[Vector[Try[HttpResponse]]] = {
    val responses = requests.toVector.map(req => sendRequest(req))
    Future.sequence(responses)
  }
}
