package jp.riken.mnn.system

import akka.actor.{ Actor, ActorLogging, ActorRefFactory }
import jp.riken.mnn.api._

/** Main actor listening to HTTP requests */
class ApiActor extends Actor
    with ActorContextCreationSupport
    with ActorLogging {

  def actorRefFactory: ActorRefFactory = context
  val executionContext = context.dispatcher

  def receive: Receive = ???
}

