package jp.riken.mnn.system

import akka.actor._
import akka.io.IO
import akka.util.Timeout
import jp.riken.mnn.Settings
import jp.riken.mnn.api.{ RootService, RoutedHttpService }
import spray.can.Http

trait Core {
  protected implicit def system: ActorSystem
}

trait StandaloneCore extends Core with RootService {
  def system: ActorSystem = ActorSystem("mnn-actor-system")
  def actorRefFactory: ActorRefFactory = system

  def boot() = {
    implicit val timeout = Settings.akka.timeout

    val apiHandler = system.actorOf(Props(new RoutedHttpService(routes)), "api-service")

    IO(Http)(system) ! Http.Bind(apiHandler, interface = Settings.akka.interface, port = Settings.akka.port)

    sys.addShutdownHook(system.shutdown())
  }
}

trait CoreActors extends Core {
  this: Core =>

  val eventHandler = system.actorOf(EventActor.props(Settings.eventHandler.maxRetries), "event-service")
  val indexHandler = system.actorOf(IndexingActor.props(Settings.indexing.duration), "indexing-service")
}
