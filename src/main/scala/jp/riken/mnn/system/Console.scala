package jp.riken.mnn.system

import com.typesafe.config.ConfigFactory
import jp.riken.mnn.Settings
import jp.riken.mnn.database.DB
import scala.collection.JavaConversions._
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }

object Console extends App with StandaloneCore {
  args.toList match {
    case "create" :: Nil =>
      val conf = ConfigFactory.load()
      println("Loaded configuration:")
      println(Settings.elems.map { case (k, v) => s"\t${k} => ${v}" }.toSeq.sorted.mkString("\n"))

      DB.create(conf) match {
        case Success(msgs) =>
          println(msgs.mkString("\n"))
          println("Done")
        case Failure(msg) => println(msg)
      }

    case "export" :: "schema" :: Nil =>
      DB.tables foreach { table =>
        table.ddl.createStatements.foreach { ddl =>
          println(ddl + ";")
        }
        println()
      }

    case "serve" :: Nil =>
      println("Starting built-in server from console...")
      boot()
      println(s"""
Data Node Information
  Namespace:         ${Settings.node.namespace} 
  Classification:    ${Settings.node.classification}
  Indexing interval: ${Settings.indexing.duration}

Starting server at ${Settings.akka.interface} port ${Settings.akka.port}; Hit <Ctrl-C> to shutdown...
      """)

    case "help" :: Nil =>
      println("""
mnn Data Node
-------------

Available commands:
  create => Create a new project using the supplied configuration file
  serve => Run the API server using the supplied configuration file
  export schema => Print the database schema in SQL to stdout

To supply a configuration file, us 'java -jar -Dconfig.file=[my config file] mnn [command]'
""")
    case _ :: params => println("Unknown command. Run 'help' for more information.")
    case Nil         => println("No commands given. Run 'help' for more information.")
  }
}
