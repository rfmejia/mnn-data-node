package jp.riken.mnn.system

import akka.actor.{ Actor, Props }
import jp.riken.mnn.database._
import org.joda.time.DateTime
import scala.concurrent.duration.FiniteDuration

class IndexingActor(interval: FiniteDuration) extends Actor with DBTools {
  import context.dispatcher

  override def preStart() =
    context.system.scheduler.scheduleOnce(interval, self, "processQueue")

  def receive = {
    case "processQueue" =>
      println(s"Index manager started ${new DateTime}")

      DB.connect withSession { implicit session =>
        IndexingManager.RegisteredResourceIndex.processQueue
      }

      println(s"Done. Index manager will restart in ${interval}.")
      context.system.scheduler.scheduleOnce(interval, self, "processQueue")
  }
}

object IndexingActor {
  def props(interval: FiniteDuration): Props = Props(new IndexingActor(interval))
}
