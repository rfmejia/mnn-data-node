package jp.riken.mnn.database

import com.typesafe.config.Config
import jp.riken.mnn.Credentials
import jp.riken.mnn.types._
import jp.riken.mnn.util.Validators._
import scala.slick.driver.H2Driver.simple._
import scala.slick.jdbc.meta.MTable
import scala.util.{ Try, Success, Failure }

/**
  * The collection of tables which maybe queried using the Slick API, and other
  *  functions related to project access and management.
  */
object DB {
  // Database tables
  lazy val tables = Vector(
    TableQuery[NodeInfo.SlickModel],
    TableQuery[RegisteredResource.SlickModel],
    TableQuery[UnregisteredResource.SlickModel],
    TableQuery[Metadata.SlickModel],
    TableQuery[User.SlickModel],
    TableQuery[Project.SlickModel],
    TableQuery[Webhook.SlickModel],
    TableQuery[Revision.SlickModel],
    TableQuery[IDManager.SlickModel],
    TableQuery[QueuedIndicesModel],
    TableQuery[ProjectResourcesRelation.SlickModel],
    TableQuery[RevisionResourcesRelation.SlickModel]
  )

  private var initialized = false

  def connect(implicit c: Credentials) = {
    val conn = scala.slick.driver.H2Driver.simple.Database.forURL(c.url, driver = c.driver,
      user = c.username getOrElse null,
      password = c.password getOrElse null, prop = c.prop)

    if (!initialized) {
      conn withSession { implicit s =>
        println("Initializing database...")
        buildTables
        initialized = true
      }
    }
    conn
  }

  /**
    *  Builds tables one by one if they do not exist.
    *  !Note that this does not upgrade a table schema if they are changed.
    */
  def buildTables(implicit s: Session): Try[Seq[String]] = Try {
    val messages = tables.map(t => {
      val tableName = t.baseTableRow.tableName
      if (MTable.getTables(tableName).list.isEmpty) {
        t.ddl.create
      }
      s"Table '${tableName}' created successfully."
    })
    messages
  }

  def create(conf: Config): Try[Seq[String]] =
    Credentials.fromConfig(conf).flatMap { cred =>
      DB.connect(cred) withSession { implicit s =>
        buildTables
      }
    }
}
