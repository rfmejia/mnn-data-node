package jp.riken.mnn.database

import com.nothome.delta.{ Delta, GDiffPatcher }
import jp.riken.mnn.ErrorResponses
import jp.riken.mnn.types._
import jp.riken.mnn.util.Validators._
import org.joda.time.DateTime
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }

/**
  * Manages the versioning of created, updated, deleted resources prior to permanent saving into the project.
  * Hence, it only generates "committed" data. Resources should not be treated as temporary in this scenario.
  */
object VersionManager extends ErrorResponses {
  /**
    * Creates a new resource temporary resource
    */
  def generateNew(newId: VersionedID, title: String, originId: Option[String], data: Option[RawData]): Try[RegisteredResource] =
    Success(RegisteredResource(newId, DateTime.now, title, originId, data))

  /**
    * Generates a pair of resources where the first is a versioned (delta) of
    *  the second (latest) resource.
    */
  def generateUpdated(
    idToUpdate: VersionedID,
    updated: UnregisteredResource
  )(implicit s: Session): Try[(RegisteredResource, RegisteredResource)] = Try {
    val existing: Option[RegisteredResource] = for {
      res <- RegisteredResourceManager.find(idToUpdate).firstOption
    } yield res

    existing match {
      case Some(orig) =>
        val nextId = idToUpdate.nextVersion
        val newRes = RegisteredResource(nextId, DateTime.now, updated.title, updated.originId, None)
        (orig.data, updated.data) match {
          case (Some(oldData), Some(newData)) =>
            diff(newData, oldData) match {
              case Success(diff) =>
                val oldRes = RegisteredResource(orig.id, orig.date, orig.title, orig.originId, Option(diff))
                (oldRes, newRes.copy(data = Option(newData)))
              case Failure(t) =>
                (orig, newRes.copy(data = Option(newData)))
            }
          case (None, Some(newData)) => (orig, newRes.copy(data = Option(newData)))
          case (_, None)             => throw serverError("Updated resource has no data")
        }
      case None => throw badRequest("Previous version of resource being changed does not exist")
    }
  }

  def generateDeleted(idToDelete: VersionedID, orig: Resource): Try[RegisteredResource] =
    Success(RegisteredResource(idToDelete.nextVersion, DateTime.now, orig.title, orig.originId, None))

  /**
    * Compute the binary diff of source and target. Returns a patch that
    *  computes the target given the source.
    */
  def diff(newData: RawData, oldData: RawData): Try[RawData] = for {
    source <- newData.getData
    target <- oldData.getData
  } yield {
    // TODO: Find out what happens if this fails
    val diff = ((new Delta()).compute(source, target))
    InMemoryData(oldData.mediaType, Some("com.nothome.javaxdelta-2.0.0"), diff)
  }

  /** Compute some target data given its source and a patch. */
  def patch(newData: RawData, patch: RawData): Try[RawData] = for {
    tdata <- newData.getData
    pdata <- patch.getData
  } yield {
    // TODO: Find out what happens if this fails
    val sdata = new GDiffPatcher().patch(tdata, pdata)
    InMemoryData(patch.mediaType, None, sdata)
  }

  def patchMulti(latest: RawData, patches: Seq[RawData]): Try[RawData] = patches.toList match {
    case p :: ps =>
      for {
        d <- patch(latest, p)
      } yield patchMulti(d, ps) getOrElse null
    case Nil => Success(latest)
  }
}
