package jp.riken.mnn.database

import jp.riken.mnn.ObjectRegistry
import jp.riken.mnn.ObjectRegistry._
import jp.riken.mnn.json.SprayJsonUtils.valueToString
import jp.riken.mnn.types._
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }

object MetadataManager {
  val elems = TableQuery[Metadata.SlickModel]

  def find(instanceId: InstanceID): Query[Metadata.SlickModel, Metadata, Seq] =
    elems.filter(m => m.instanceId === instanceId)

  def find(instanceId: InstanceID, key: String): Query[Metadata.SlickModel, Metadata, Seq] =
    elems.filter(m => m.instanceId === instanceId && m.key === key)

  def get(id: InstanceID)(implicit s: Session): Map[String, String] =
    find(id).list.map(meta => (meta.key, meta.value)).toMap

  def getTags(id: InstanceID)(implicit s: Session): Option[Set[String]] =
    find(id, "_tags").firstOption.map(_.value.split(",").toSet)

  def save(id: InstanceID, kvMap: Map[String, String])(implicit s: Session): Option[Int] =
    elems ++= kvMap.map(e => Metadata(id, e._1, e._2))

  def save(id: InstanceID, key: String, value: String)(implicit s: Session): Option[Int] =
    save(id, Map(key -> value))

  def saveTags(id: InstanceID, tags: Set[String])(implicit s: Session): Option[Int] = {
    val value = if (tags.isEmpty) "" else tags.reduce(_ + "," + _)
    save(id, "_tags", value)
  }

  def remove(id: InstanceID)(implicit s: Session): Try[Int] = Try(find(id).delete)

  def remove(id: InstanceID, key: String)(implicit s: Session): Try[Int] = Try(find(id, key).delete)

  def extractBasic(data: RawData): Map[String, String] = {
    // Search reader for a media type
    val readers = ObjectRegistry.readers.of(data.mediaType)
    if (readers.isEmpty) Map.empty
    else { // For now, just use the first matching reader
      // See note on using the first reader for the time being
      val reader = readers.head
      val result = for {
        meta <- reader.readOne(data)
      } yield meta.fields

      result match {
        case Failure(_) => Map.empty
        case Success(meta) =>
          meta.filterKeys(_.startsWith("_")).map { e =>
            (e._1, valueToString(e._2))
          }
      }
    }
  }

  def searchOriginID(originId: String)(implicit s: Session): Option[InstanceID] =
    elems.filter(r => r.key === "_origin_id" && r.value === originId).firstOption.map(_.instanceId)
}
