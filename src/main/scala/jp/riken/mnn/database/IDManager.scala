package jp.riken.mnn.database

import com.github.tototoshi.slick.H2JodaSupport._
import java.util.UUID
import jp.riken.mnn.{ ErrorResponses, Settings }
import jp.riken.mnn.types._
import jp.riken.mnn.util.Validators._
import org.joda.time.DateTime
import scala.slick.driver.H2Driver.simple._
import scala.util.Try

object IDManager extends ErrorResponses {
  val elems = TableQuery[SlickModel]

  /** If given a PlainID, returns the latest version of that ID; else returns supplied VersionedID */
  def asVersionedID(sid: SerialID)(implicit s: Session): Option[VersionedID] = sid match {
    case v: VersionedID => Some(v)
    case p: PlainID     => latestVersionedID(p).firstOption
  }

  def latestVersionedID(sid: SerialID): Query[_, VersionedID, Seq] = sid match {
    case v: VersionedID => latestVersionedID(v.asPlainID)
    case p: PlainID     => find(p).map(_.latestVersionedID)
  }

  def find(p: PlainID): Query[SlickModel, RegisteredID, Seq] =
    elems.filter(rid => rid.namespace === p.namespace && rid.serial === p.serial)

  def find(serial: Int): Query[SlickModel, RegisteredID, Seq] = elems.filter(rid => rid.serial === serial)

  /** Create a new versioned ID with a system unique serial. */
  def generateVersionedID()(implicit s: Session): Try[VersionedID] = Try {
    // Serial is set to 0, will be skipped by Slick because it is auto-incrementing
    val newId = RegisteredID(Settings.node.namespace, 0, 1, DateTime.now)
    val serial = (elems returning elems.map(_.serial)) += newId
    find(serial).map(_.latestVersionedID).firstOption match {
      case Some(vid) => vid
      case None      => throw serverError("Could not generate a versioned ID")
    }
  }

  def modifiedWithin(from: DateTime, to: DateTime): Query[SlickModel, RegisteredID, Seq] =
    elems.filter(r => r.lastModified >= from && r.lastModified <= to)

  case class RegisteredID(namespace: String, serial: Int, latestVersion: Int, lastModified: DateTime) {
    val asPlainID = PlainID(namespace, serial)
    val latestVersionedID = VersionedID(namespace, serial, latestVersion)
  }

  class SlickModel(tag: Tag) extends Table[RegisteredID](tag, "REGISTERED_IDS") {
    def serial = column[Int]("SERIAL", O.PrimaryKey, O.AutoInc)

    def namespace = column[String]("NAMESPACE", O.NotNull)
    def latestVersion = column[Int]("LATEST_VERSION", O.NotNull)
    def lastModified = column[DateTime]("LAST_MODIFIED", O.NotNull)

    def plainIdIdx = index("NAMESPACE_SERIAL_UK", (namespace, serial), unique = true)

    def * = (namespace, serial, latestVersion, lastModified) <> (RegisteredID.tupled, RegisteredID.unapply)

    def asPlainID = (namespace, serial) <> (PlainID.tupled, PlainID.unapply)
    def latestVersionedID = (namespace, serial, latestVersion) <> (VersionedID.tupled, VersionedID.unapply)
  }
}
