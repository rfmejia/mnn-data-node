package jp.riken.mnn.database

import akka.event.Logging
import jp.riken.mnn.database.indexing.LuceneTextIndexEngine
import jp.riken.mnn.types._
import org.joda.time.DateTime
import scala.slick.driver.H2Driver.simple._
import scala.util.Try

object IndexingManager {
  val queue = TableQuery[QueuedIndicesModel]
  val textIndexEngine = LuceneTextIndexEngine

  object RegisteredResourceIndex {
    def textSearch = textIndexEngine.search _

    def queue(id: VersionedID)(implicit s: Session): Try[VersionedID] =
      Try {
        val now = new DateTime
        IndexingManager.queue += id
        println(s"Added resource ${id} into queue")
        id
      }

    def processQueue()(implicit s: Session): Try[Vector[VersionedID]] = {
      LuceneTextIndexEngine.withIndexWriter { implicit writer =>
        val items = IndexingManager.queue.buildColl[Vector]
        println(s"Processing ${items.length} resources")
        val l = for {
          versionedId <- items
        } yield {
          RegisteredResourceManager.find(versionedId).firstOption map { resource =>
            LuceneTextIndexEngine.index(resource)

            // Remove ID from queue
            val r = for {
              r <- IndexingManager.queue if r.versionedId === versionedId
            } yield r
            r.delete
            resource.id
          }
        }
        l.flatten
      }
    }
  }
}
