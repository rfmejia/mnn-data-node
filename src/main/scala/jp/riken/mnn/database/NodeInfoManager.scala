package jp.riken.mnn.database

import jp.riken.mnn.types.NodeInfo
import scala.slick.driver.H2Driver.simple._

object NodeInfoManager {
  private val elems = TableQuery[NodeInfo.SlickModel]

  /** Get all nodeInfos in project. */
  def apply() = elems

  /** Get a specific node info given its ID */
  def apply(id: String)(implicit s: Session) = elems.filter(_.id === id).firstOption
}

