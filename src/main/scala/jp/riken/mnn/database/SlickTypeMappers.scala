package jp.riken.mnn.database

import java.util.UUID
import jp.riken.mnn.types._
import scala.language.implicitConversions
import scala.slick.driver.H2Driver.simple._
import spray.http.Uri

/** Defines implicit converters for custom datatypes from/to database columns */
trait SlickTypeMappers {
  implicit def uriMapper = MappedColumnType.base[Uri, String](_.toString, Uri(_))
}
