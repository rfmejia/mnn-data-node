package jp.riken.mnn.database

import com.github.tototoshi.slick.H2JodaSupport._
import java.util.UUID
import jp.riken.mnn.ErrorResponses
import jp.riken.mnn.types._
import org.joda.time.DateTime
import scala.annotation.tailrec
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }

/** Encapsulates internal logic of resource storage, versioning, diff, etc. */
object RegisteredResourceManager extends ErrorResponses {
  val elems = TableQuery[RegisteredResource.SlickModel]

  /** Get all resources in project */
  def find(versionedId: VersionedID): Query[RegisteredResource.SlickModel, RegisteredResource, Seq] =
    elems.filter(r => r.versionedId === versionedId)

  def latestResource(id: SerialID)(implicit s: Session): Option[RegisteredResource] =
    IDManager.latestVersionedID(id).firstOption.flatMap(vid => find(vid).firstOption)

  /** Convenience method Get all version infos related to resource */
  def versionsOf(plainId: PlainID): Query[RegisteredResource.SlickModel, RegisteredResource, Seq] =
    elems.filter(r => r.versionedId.asColumnOf[String].startsWith(plainId.urn))

  def resourcesOf(projectId: ProjectID): Query[RegisteredResource.SlickModel, RegisteredResource, Seq] = {
    val relations = TableQuery[ProjectResourcesRelation.SlickModel]
    for {
      rel <- relations if rel.projectId === projectId
      res <- rel.resource
    } yield res
  }

  def resourcesOf(revisionId: RevisionID): Query[RegisteredResource.SlickModel, RegisteredResource, Seq] = {
    val relations = TableQuery[RevisionResourcesRelation.SlickModel]
    for {
      rel <- relations if rel.revisionId === revisionId
      res <- elems if res.versionedId.asColumnOf[InstanceID] === rel.instanceId
    } yield res
  }

  def resourceOfOriginId(originId: String): Query[RegisteredResource.SlickModel, RegisteredResource, Seq] =
    elems.filter(r => r.originId === Option(originId))

  def committedWithin(from: DateTime, to: DateTime): Query[RegisteredResource.SlickModel, RegisteredResource, Seq] =
    elems.filter(r => r.date >= from && r.date <= to)

  /**
    * Get a specific resource, given its version number.
    * The target data is recreated (patched) when necessary.
    */
  def get(id: VersionedID)(implicit s: Session): Option[RegisteredResource] =
    find(id).firstOption flatMap { r =>
      if (r.data.isDefined && r.data.get.deltaMethod.isDefined) {
        val latest = IDManager.find(id.asPlainID).firstOption.map(_.latestVersion).getOrElse(id.version)
        val versionNums = (id.version to latest).reverse
        val ids = versionNums.map(v => id.copy(version = v))

        // Database query for resources
        val l1: Vector[Resource] = ids.map(v => find(v).firstOption).flatten.filter(_.data.isDefined).toVector

        // In-memory list
        val l2: Vector[Resource] = {
          // Find the closest version with no deltaMethod
          @tailrec def targetSegment(l: List[Resource], acc: List[Resource]): List[Resource] =
            l match {
              case Nil => throw serverError("Empty resource list while patching data")
              case head :: tail =>
                if (!head.data.get.deltaMethod.isDefined) acc :+ head
                else targetSegment(tail, acc :+ head)
            }

          targetSegment(l1.toList, List.empty).toVector
        }

        val l3 = l2.map(_.data.get).reverse

        val l4 = for {
          patchedData <- VersionManager.patchMulti(l3.head, l3.tail)
          patchedResource <- Try(r.copy(data = Some(patchedData)))
        } yield patchedResource

        l4 match {
          case Success(patched) => Some(patched)
          case Failure(_)       => None
        }
      } else Some(r)
    }

  /**
    * If there is a versioned ID associated, generate an updated version save.
    */
  def commit(projectId: ProjectID, temporaryId: TemporaryID,
    action: Revision.ChangeAction)(implicit s: Session): Try[VersionedID] =
    for {
      instance <- Try {
        UnregisteredResourceManager.find(temporaryId).firstOption match {
          case Some(res) => res
          case None      => throw notFound("Unregistered resource not found")
        }
      }

      versionedId <- action match {
        case Revision.Create =>
          for {
            vid <- IDManager.generateVersionedID()
            newRes <- VersionManager.generateNew(vid, instance.title, instance.originId, instance.data)
            data <- newRes.saveToDisk()
            _ <- Try(elems += newRes)
            _ <- ProjectManager.registerResources(projectId, Seq(newRes.versionedId))
            _ <- Try {
              if (data.isDefined) {
                val metadata = MetadataManager.extractBasic(data.get)
                MetadataManager.save(newRes.versionedId, metadata)
              }
            }
          } yield vid

        case Revision.Update =>
          for {
            idToUpdate <- instance.versionedId match {
              case Some(vid) => Success(vid)
              case None      => Failure(serverError(s"Attempting to update a non-existent resource in ${temporaryId}"))
            }
            (oldRes, updatedRes) <- VersionManager.generateUpdated(idToUpdate, instance)
            _ <- oldRes.saveToDisk()
            _ <- Try(elems += oldRes)
            data <- updatedRes.saveToDisk()
            _ <- Try(elems += updatedRes)
            _ <- ProjectManager.registerResources(projectId, Seq(updatedRes.versionedId))
            _ <- ProjectManager.deregisterResources(projectId, Seq(oldRes.versionedId))
            _ <- Try {
              if (data.isDefined) {
                val metadata = MetadataManager.extractBasic(data.get)
                MetadataManager.save(updatedRes.versionedId, metadata)
              }
            }
          } yield updatedRes.versionedId

        case Revision.Delete =>
          for {
            idToDelete <- instance.versionedId match {
              case Some(vid) => Success(vid)
              case None      => Failure(serverError(s"Attempting to delete a non-existent resource in ${temporaryId}"))
            }
            deletedRes <- VersionManager.generateDeleted(idToDelete, instance)
            _ <- Try(elems += deletedRes)
            _ <- deletedRes.saveToDisk()
            _ <- ProjectManager.registerResources(projectId, Seq(deletedRes.versionedId))
            _ <- ProjectManager.deregisterResources(projectId, Seq(idToDelete))
          } yield deletedRes.versionedId

        case Revision.Unknown => Failure(serverError("Illegal change action supplied"))
      }

      _ <- IndexingManager.RegisteredResourceIndex.queue(versionedId)
      _ <- UnregisteredResourceManager.delete(temporaryId)
    } yield versionedId

  def search(term: String, offset: Int = 0, limit: Int = 50) =
    IndexingManager.RegisteredResourceIndex.textSearch(term, offset, limit)
}
