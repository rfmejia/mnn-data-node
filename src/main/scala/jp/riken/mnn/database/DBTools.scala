package jp.riken.mnn.database

import com.typesafe.config.ConfigFactory
import jp.riken.mnn.Credentials
import scala.language.implicitConversions
import scala.slick.driver.H2Driver.simple._
import scala.slick.lifted.CanBeQueryCondition

trait DBTools {
  /**
    * Exposes credentials stored in configuration file implicitly.
    */
  implicit lazy val cred = Credentials.fromConfig(ConfigFactory.load()).getOrElse {
    throw new IllegalStateException("Credentials could not be loaded, please check your configuration file")
  }

  /**
    * Optionally filter on a column with a supplied predicate.
    * Credit: https://github.com/neowinx/hello-slick-2.1-dynamic-filter/blob/master/src/main/scala/HelloSlick.scala#L3-L7
    */
  case class MaybeFilter[X, Y](val query: scala.slick.lifted.Query[X, Y, Seq]) {
    def filteredBy(op: Option[_])(f: (X) => Column[Option[Boolean]]) = {
      op map { o => MaybeFilter(query.filter(f)) } getOrElse { this }
    }
  }
}
