package jp.riken.mnn.database

import jp.riken.mnn.ErrorResponses
import jp.riken.mnn.Settings
import jp.riken.mnn.types._
import scala.slick.driver.H2Driver.simple._
import scala.util.Try

/** Encapsulates internal logic of resource storage, versioning, diff, etc. */
object ProjectManager extends ErrorResponses {
  val elems = TableQuery[Project.SlickModel]
  val relations = TableQuery[ProjectResourcesRelation.SlickModel]

  def find(projectId: ProjectID): Query[Project.SlickModel, Project, Seq] =
    elems.filter(r => r.projectId === projectId)

  def projectsOf(owner: UserID): Query[Project.SlickModel, Project, Seq] =
    elems.filter(r => r.owner === owner)

  def create(userId: UserID, projectName: String, description: Option[String], readme: Option[String],
    policy: String)(implicit s: Session): Try[Project] = Try {
    val projectId = ProjectID(userId.namespace, userId.username, projectName)
    ProjectID.validate(projectId)
    val project = Project(projectId, userId, projectName, description, readme, policy)
    elems += project
    find(projectId).first
  }

  def edit(project: Project)(implicit s: Session): Try[Project] = Try {
    val existing = find(project.projectId)
    if (existing.exists.run) {
      existing.update(project)
      find(project.projectId).first
    } else throw notFound("Project does not exist")
  }

  def delete(projectId: ProjectID)(implicit s: Session): Try[Int] = Try {
    val existing = find(projectId)
    if (existing.exists.run) existing.delete
    else throw notFound("Project does not exist")
  }

  /** Registers a resource to the default listing of a project. */
  def registerResources(projectId: ProjectID, versionedIDs: Seq[VersionedID])(implicit s: Session): Try[Int] =
    Try {
      val newRelations = versionedIDs.map(id => ProjectResourcesRelation(projectId, id))
      (relations ++= newRelations).get
    }

  /** Deregisters a resource to the default listing of a project. */
  def deregisterResources(projectId: ProjectID, versionedIDs: Seq[VersionedID])(implicit s: Session): Try[Int] =
    Try {
      val target = versionedIDs.toSet
      val existingRelations = relations.filter(r => r.projectId === projectId && r.versionedId.inSetBind(target))
      existingRelations.delete
    }
}
