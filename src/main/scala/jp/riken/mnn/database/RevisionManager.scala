package jp.riken.mnn.database

import java.util.UUID
import jp.riken.mnn.ErrorResponses
import jp.riken.mnn.types._
import org.joda.time.DateTime
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }

object RevisionManager extends ErrorResponses {
  val elems = TableQuery[Revision.SlickModel]
  val relations = TableQuery[RevisionResourcesRelation.SlickModel]

  def forRevision[T](revisionId: RevisionID)(f: Revision => T)(implicit s: Session): Try[T] = Try {
    val revision = elems.filter(r => r.revisionId === revisionId)
    if (revision.exists.run) f(revision.first)
    else throw notFound(s"Revision ${revisionId} does not exist")
  }

  def forDraftRevision[T](revisionId: RevisionID)(f: Revision => T)(implicit s: Session): Try[T] =
    forRevision(revisionId) { revision =>
      if (revision.isDraft) f(revision)
      else throw badRequest("Cannot modify a draft revision")
    }

  def find(revisionId: RevisionID): Query[Revision.SlickModel, Revision, Seq] =
    elems.filter(rev => rev.revisionId === Option(revisionId))

  def revisionsOf(projectId: ProjectID): Query[Revision.SlickModel, Revision, Seq] =
    elems.filter(r => r.projectId === projectId)

  // Create only if there is no draft
  def create(projectId: ProjectID, source: String)(implicit s: Session): Try[Revision] =
    Try {
      if (!RevisionManager.hasDraft) {
        val revision = Revision(projectId, source)
        val id = elems += revision
        find(revision.revisionId).first
      } else throw badRequest("A draft revision already exists")
    }

  def hasDraft(implicit s: Session): Boolean = elems.filter(_.isDraft === true).exists.run

  def commit(revisionId: RevisionID, message: String)(implicit s: Session): Try[Revision] =
    s.withTransaction {
      forDraftRevision(revisionId) { revision =>
        try {
          // Committing is only allowed if it is a draft
          val oldRels: Seq[RevisionResourcesRelation] = relations.filter(_.revisionId === revisionId).list

          // Commit resources
          val updates: Seq[(VersionedID, Revision.ChangeAction)] =
            oldRels.map { rel =>
              RegisteredResourceManager.commit(revision.projectId, rel.instanceId.asInstanceOf[TemporaryID],
                rel.action) match {
                case Success(vid) => (vid, rel.action)
                case Failure(err) => throw err
              }
            }

          val newRels = updates.map {
            case (versionedId, action) => (versionedId, action)
          }
          val newIDs = updates.map(_._1)

          // Deregister/register resources in revision and project
          deregisterRevisions(revisionId, oldRels.map(_.instanceId))
          registerRevisions(revisionId, newRels)

          ProjectManager.registerResources(revision.projectId, newIDs)

          val newRevision = revision.copy(isDraft = false, message = Some(message), date = Some(DateTime.now))
          find(revisionId).update(newRevision)

          newRevision
        } catch {
          case err: Throwable =>
            s.rollback
            throw err
        }
      }
    }

  // Permanently deletes revisions before deleting the revision
  def delete(revisionId: RevisionID)(implicit s: Session): Try[Int] =
    forDraftRevision(revisionId) { revision =>
      val relations = TableQuery[RevisionResourcesRelation.SlickModel]
      val unregistered = UnregisteredResourceManager.resourcesOf(revisionId)
      MetadataManager.elems.filter(m => m.instanceId in unregistered.map(_.temporaryId.asColumnOf[InstanceID])).delete
      UnregisteredResourceManager.elems.filter(r => r.temporaryId in unregistered.map(_.temporaryId)).delete
      relations.filter(r => r.revisionId === revisionId).delete
      find(revisionId).delete
    }

  def changeAction(revisionId: RevisionID, instanceId: InstanceID,
    action: Revision.ChangeAction)(implicit s: Session): Try[Int] =
    forDraftRevision(revisionId) { revision =>
      val relation = relations.filter(r => r.revisionId === revisionId && r.instanceId === instanceId)
      relation.update(RevisionResourcesRelation(revisionId, instanceId, action))
    }

  def registerRevisions(
    revisionId: RevisionID,
    revisions: Seq[(InstanceID, Revision.ChangeAction)]
  )(implicit s: Session): Try[Option[Int]] =
    forDraftRevision(revisionId) { revision =>
      val rels = revisions.map {
        case (instanceId, action) => RevisionResourcesRelation(revisionId, instanceId, action)
      }
      relations ++= rels
    }

  def deregisterRevisions(revisionId: RevisionID, instanceIds: Seq[InstanceID])(implicit s: Session): Try[Int] =
    forDraftRevision(revisionId) { revision =>
      val target = instanceIds.toSet
      val relation = relations.filter(r => r.revisionId === revisionId && r.instanceId.inSetBind(target))
      relation.delete
    }
}
