package jp.riken.mnn.database

import jp.riken.mnn.ErrorResponses
import jp.riken.mnn.types._
import jp.riken.mnn.util.SecurityTools
import scala.slick.driver.H2Driver.simple._
import scala.util.Try

/** Encapsulates internal logic of resource storage, versioning, diff, etc. */
object UserManager extends ErrorResponses {
  val elems = TableQuery[User.SlickModel]

  def find(userId: UserID)(implicit s: Session): Query[User.SlickModel, User, Seq] =
    elems.filter(u => u.userId === userId)

  def find(username: String)(implicit s: Session): Query[User.SlickModel, User, Seq] =
    elems.filter(u => u.username === username)

  def create(userId: UserID, password: Array[Byte], name: String, email: String)(implicit s: Session): Try[User] =
    Try {
      val salt = SecurityTools.generateSalt()
      val hashed = SecurityTools.hashPassword(password, salt)
      elems += User(userId, userId.username, hashed, salt, name, email)
      find(userId).first
    }

  def edit(userId: UserID, password: Array[Byte], name: String, email: String)(implicit s: Session): Try[User] =
    Try {
      val existing = find(userId.username)
      if (existing.exists.run) {
        val salt = SecurityTools.generateSalt()
        val hashed = SecurityTools.hashPassword(password, salt)
        val user = existing.first.copy(name = name, email = email, password = hashed)
        existing.update(user)
        user
      } else throw notFound(s"User does not exist")
    }

  def delete(userId: UserID)(implicit s: Session): Try[Int] =
    Try {
      val existing = find(userId.username)
      if (existing.exists.run) {
        existing.delete
      } else throw notFound(s"User does not exist")
    }
}
