package jp.riken.mnn.database

import com.github.tototoshi.slick.H2JodaSupport._
import java.util.UUID
import jp.riken.mnn.{ ErrorResponses, ObjectRegistry }
import jp.riken.mnn.mediatypes.{ MassSpectraSingleReader, MassSpectraListReader }
import jp.riken.mnn.types._
import org.joda.time.DateTime
import scala.annotation.tailrec
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }
import spray.json._

/** Encapsulates internal logic of resource storage, versioning, diff, etc. */
object UnregisteredResourceManager extends ErrorResponses {
  val elems = TableQuery[UnregisteredResource.SlickModel]

  /** Get all resources in project */
  def find(temporaryId: TemporaryID): Query[UnregisteredResource.SlickModel, UnregisteredResource, Seq] =
    elems.filter(r => r.temporaryId === temporaryId)

  def resourcesOf(revisionId: RevisionID): Query[UnregisteredResource.SlickModel, UnregisteredResource, Seq] = {
    val relations = TableQuery[RevisionResourcesRelation.SlickModel]
    for {
      rel <- relations if rel.revisionId === revisionId
      res <- elems if res.temporaryId.asColumnOf[InstanceID] === rel.instanceId
    } yield res
  }

  def get(temporaryId: TemporaryID)(implicit s: Session): Option[UnregisteredResource] = find(temporaryId).firstOption

  /** Creates a new temporary resource with a temporary ID. */
  def create(data: RawData)(implicit s: Session): Try[Vector[UnregisteredResource]] = {
    val readers = ObjectRegistry.readers.of(data.mediaType)
    if (readers.isEmpty) Failure(unsupportedMediaType(s"Cannot read '${data.mediaType}'"))
    else {
      val reader = readers.head
      reader match {
        case r: MassSpectraSingleReader => createSingle(data).map(Vector(_))
        case r: MassSpectraListReader =>
          s.withTransaction {
            try {
              r.read(data).flatMap { elems =>
                val result = elems.map(createSingle(_))
                Try(result.map(_.get))
              }
            } catch {
              case err: Throwable =>
                s.rollback
                Failure(err)
            }
          }
      }
    }
  }

  private def createSingle(data: RawData)(implicit s: Session): Try[UnregisteredResource] = {
    val id = TemporaryID.random
    val metadata = MetadataManager.extractBasic(data)
    val _title = metadata.get("_title").getOrElse(id.toString)
    val originId = metadata.get("_origin_id")

    // Search if origin ID exists in other documents
    val versionedId: Option[VersionedID] =
      originId.flatMap(oid => RegisteredResourceManager.resourceOfOriginId(oid).firstOption.map(_.versionedId))

    val newResource = UnregisteredResource(id, _title, versionedId, originId, Option(data))
    for {
      savedData <- newResource.saveToDisk()
      savedResource <- Success(newResource.copy(data = savedData))
      _ <- Try {
        elems += savedResource
        MetadataManager.save(savedResource.id, metadata)
      }
    } yield savedResource
  }

  /**
    *  Updates a temporary resource.
    *  Stores resource metadata in the database and raw data in local disk.
    */
  def update(resource: UnregisteredResource)(implicit s: Session): Try[UnregisteredResource] = {
    val existing = elems.filter(r => r.temporaryId === resource.temporaryId)
    if (!existing.exists.run)
      throw notFound(s"Resource ${resource.temporaryId} does not exist")
    else for {
      _ <- resource.saveToDisk()
      _ <- Try(existing.update(resource))

      // Save metadata
      _ <- Try {
        val metadata = resource.data match {
          case Some(d) => MetadataManager.extractBasic(d)
          case None    => Map.empty[String, String]
        }
        MetadataManager.save(resource.id, metadata)
      }
    } yield existing.first
  }

  /**
    * Deletes a temporary resource, including its metadata.
    */
  def delete(temporaryId: TemporaryID)(implicit s: Session): Try[Int] = {
    val existing = elems.filter(r => r.temporaryId === temporaryId)
    if (!existing.exists.run)
      throw notFound(s"Resource ${temporaryId} does not exist")
    else for {
      result <- Try(existing.delete)
      _ <- MetadataManager.remove(temporaryId)
    } yield result
  }
}
