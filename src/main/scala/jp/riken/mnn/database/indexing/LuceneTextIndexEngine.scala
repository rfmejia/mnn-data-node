package jp.riken.mnn.database.indexing

import java.io.File
import jp.riken.mnn.Settings
import jp.riken.mnn.database.DBTools
import jp.riken.mnn.types._
import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.document._
import org.apache.lucene.index._
import org.apache.lucene.queryparser.classic.QueryParser
import org.apache.lucene.search._
import org.apache.lucene.store._
import org.apache.lucene.util.Version
import scala.util.{ Try, Success, Failure }

case class TextSearchResult(query: String, totalHits: Int, hits: Vector[(String, Float)])

object LuceneTextIndexEngine extends DBTools {
  type RegisteredResourceIndexer = RegisteredResource => Vector[Field]

  val lucene_version = Version.LUCENE_4_10_1
  private val defaultSearchField = "data"
  private val analyzer = new StandardAnalyzer()
  private val parser = new QueryParser(defaultSearchField, analyzer)

  val indexers = Vector(
    StandardLuceneTextIndexers.indexDefaultFields,
    StandardLuceneTextIndexers.indexDataField
  )

  def withIndexWriter[T](f: IndexWriter => T): Try[T] = for {
    directory <- Try(FSDirectory.open(new File(Settings.indexing.path)))
    config <- Try(new IndexWriterConfig(lucene_version, analyzer))
    writer <- Try(new IndexWriter(directory, config))
    result <- try {
      Success(f(writer))
    } catch {
      case t: Throwable => Failure(t)
    } finally {
      writer.close()
      directory.close()
    }
  } yield result

  def index(r: RegisteredResource)(implicit writer: IndexWriter): Try[String] = for {
    doc <- Success(new Document())
    result <- Try {
      for (indexer <- indexers) {
        val fields = indexer.apply(r)
        fields foreach (doc.add(_))
      }
      val idTerm = new Term("id", r.id.toString)
      writer.updateDocument(idTerm, doc)
      s"Successfully indexed resource '${r.id}'"
    }
  } yield result

  def search(term: String, offset: Int, limit: Int): Try[TextSearchResult] =
    for {
      directory <- Try(FSDirectory.open(new File(Settings.indexing.path)))
      reader <- Try(DirectoryReader.open(directory))
      searcher <- Try(new IndexSearcher(reader))
      finalTerm <- Try(if (term.startsWith("*")) term.substring(1) else term)
      query <- Try(parser.parse(finalTerm))
      collector <- try {
        println(s"Searching for '${finalTerm}'")
        val coll = TopScoreDocCollector.create(offset + limit, true)
        searcher.search(query, coll)
        val result = coll.topDocs(offset)
        val hits = result.scoreDocs.map { hit =>
          (searcher.doc(hit.doc).get("id"), hit.score)
        }.toVector
        println(s"Hits: ${result.totalHits}")
        hits foreach (hit => println(s"\t${hit._1} (${hit._2})"))
        Success(TextSearchResult(finalTerm, result.totalHits, hits))
      } catch {
        case t: Throwable => Failure(t)
      } finally {
        reader.close()
        directory.close()
      }
    } yield collector
}

object StandardLuceneTextIndexers {
  def indexDefaultFields: LuceneTextIndexEngine.RegisteredResourceIndexer = {
    r: RegisteredResource =>
      Vector(
        new Field("id", r.id.toString, StringField.TYPE_STORED),
        new Field("title", r.title, StringField.TYPE_STORED)
      )
    // new Field("origin_id", r.title, StringField.TYPE_STORED))
  }

  // TODO: def indexMetadata: LuceneTextIndexEngine.ResourceIndexer

  def indexDataField: LuceneTextIndexEngine.RegisteredResourceIndexer = {
    r: RegisteredResource =>
      r.data match {
        case Some(raw) =>
          val data = for (d <- raw.getData) yield {
            val str = new String(d)
            new Field("data", str, TextField.TYPE_STORED)
          }
          data match {
            case Success(d) => Vector(d)
            case Failure(_) => Vector.empty
          }
        case None => Vector.empty
      }
  }

  // TODO: Add metadata indexer
}
