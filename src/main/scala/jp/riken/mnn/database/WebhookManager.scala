package jp.riken.mnn.database

import java.util.UUID
import jp.riken.mnn.ErrorResponses
import jp.riken.mnn.types._
import org.joda.time.DateTime
import scala.slick.driver.H2Driver.simple._
import scala.util.{ Try, Success, Failure }
import spray.http.StatusCodes._
import spray.http._
import spray.json._

object WebhookManager extends ErrorResponses {
  val elems = TableQuery[Webhook.SlickModel]

  def find(uuid: UUID)(implicit s: Session): Query[Webhook.SlickModel, Webhook, Seq] =
    for (w <- elems if w.uuid === uuid) yield w

  def webhooksOf(publisher: ProjectID): Query[Webhook.SlickModel, Webhook, Seq] =
    elems.filter(_.publisher === publisher)

  def validateEventType(eventType: String): String = {
    // TODO: Handle only registered event types using a trait
    if (eventType.toLowerCase != "revision_committed")
      throw ErrorResponses.badRequest(s"'${eventType}' is not a valid webhook event type.")
    eventType
  }

  def create(subscriber: UserID, publisher: ProjectID, uri: Uri, eventType: String)(implicit s: Session): Try[Webhook] =
    Try {
      val webhook = Webhook(subscriber, publisher, uri, validateEventType(eventType))
      elems += webhook
      find(webhook.uuid).first
    }

  def edit(uuid: UUID, uri: Uri, eventType: String)(implicit s: Session): Try[Webhook] = Try {
    val existing = find(uuid)
    if (existing.exists.run) {
      val hook = existing.first.copy(uri = uri, eventType = validateEventType(eventType))
      existing.update(hook)
      hook
    } else throw notFound(s"Webhook does not exist")
  }

  def delete(id: UUID)(implicit s: Session): Try[Int] = Try {
    val existing = find(id)
    if (existing.exists.run) {
      existing.delete
    } else throw notFound(s"Webhook does not exist")
  }
}
