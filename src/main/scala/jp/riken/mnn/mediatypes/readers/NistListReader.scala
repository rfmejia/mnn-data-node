package jp.riken.mnn.mediatypes.readers

import jp.riken.mnn.ObjectRegistry
import jp.riken.mnn.ObjectRegistry.registeredMediaTypes.`application/vnd.nist`
import jp.riken.mnn.types.RawData
import jp.riken.mnn.mediatypes.MassSpectraListReader
import scala.util.Try
import spray.json.JsObject

/**
  * Parses a NIST list string stream into a JSON AST (flat)
  */
object NistListReader extends MassSpectraListReader {
  val version = 1
  val mediaType = "application/vnd.nist.list"

  def split(input: String): Try[Vector[String]] = Try {
    input.split("(?m)^\\s*$").toVector.map(_.trim).filterNot(_.isEmpty)
  }

  def read(input: String): Try[Vector[RawData]] =
    split(input).flatMap { lines =>
      val result = lines.map(data => RawData(`application/vnd.nist`, data))
      Try(result.map(_.get))
    }

  def readToJsObject(input: String): Try[Vector[JsObject]] =
    split(input).flatMap { lines =>
      val result = lines.map(data => NistReader.readOne(data))
      Try(result.map(_.get))
    }
}
