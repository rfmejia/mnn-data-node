package jp.riken.mnn.mediatypes.readers

import jp.riken.mnn.json.SprayJsonUtils._
import jp.riken.mnn.mediatypes.MassSpectraSingleReader
import scala.util.parsing.combinator.JavaTokenParsers
import spray.json.JsField
import spray.json._

/**
  * Parses a NIST string stream into a JSON AST (flat)
  */
object NistReader extends MassSpectraSingleReader {
  val version = 1

  val mediaType = "application/vnd.nist"

  def title: Parser[String] = """[a-zA-Z_ ]\w*""".r

  def field: Parser[JsField] = title ~ ":" ~ string ^^ {
    case name ~ _ ~ value => (name, value)
  }

  // Required field titles
  def nameTitle: Parser[String] = "(?i)(name)".r
  def peakTitle: Parser[String] = "(?i)num peaks".r

  // Required fields
  def nameField: Parser[JsField] = nameTitle ~ ":" ~ nonEmptyString ^^ {
    case name ~ _ ~ value => (name, value)
  }

  def peakField: Parser[JsArray] = peakTitle ~ ":" ~ int ~ peakPair.* ^^ {
    case _ ~ _ ~ numPeaks ~ peaks =>
      val expected = numPeaks.value.toInt
      val actual = peaks.size
      if (expected == actual) JsArray(peaks.toVector)
      else throw badRequest(s"Expected ${expected} peaks, found ${actual}")
  }

  def peakPair: Parser[JsArray] = peakDelim.* ~> float ~ peakDelim.* ~ int <~ peakDelim.* ^^ {
    case mass ~ _ ~ intensity =>
      JsArray(Vector(mass, intensity))
  }

  def peakDelim: Parser[String] = """\s\t|\,|\;|\:|\(|\)|\[|\]|\{|\}""".r

  def structure: Parser[JsObject] = nameField ~ field.* ~ peakField ^^ {
    case name ~ fs ~ peaks =>
      val fields: Vector[JsField] = Vector(name) ++ fs :+ (("_peaks", peaks))

      // Set other additional fields here post-parsing
      // TODO: Where is the ID for NIST Individual formats?
      // val f1 = fields.find(_._1 == "NAME").map(f => valueToJsString(f._2)) match {
      //   case Some(t) => fields :+ (("_origin_id", t))
      //   case None => fields
      // }

      val f2 = fields.find(_._1 == "NAME").map(f => valueToJsString(f._2)) match {
        case Some(t) => fields :+ (("_title", t))
        case None    => fields
      }

      val f3 = f2.find(_._1 == "PRECURSORMZ").map(f => valueToJsString(f._2)) match {
        case Some(t) => fields :+ (("_precursor_mz", t))
        case None    => f2
      }

      val f4 = f3 :+ (("_reader", JsString(this.toString)))

      JsObject(f4.toMap)
  }
}
