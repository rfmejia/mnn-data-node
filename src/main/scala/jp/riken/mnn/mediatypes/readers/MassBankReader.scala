package jp.riken.mnn.mediatypes.readers

import jp.riken.mnn.json.SprayJsonUtils._
import jp.riken.mnn.mediatypes.MassSpectraSingleReader
import scala.util.parsing.combinator.JavaTokenParsers
import spray.json.DefaultJsonProtocol._
import spray.json.JsField
import spray.json._

object MassBankReader extends MassSpectraSingleReader {
  val version = 1

  val mediaType = "application/vnd.massbank"

  def field(implicit tag: Parser[String]): Parser[JsStringField] = tag ~ ":" ~ string ^^ {
    case name ~ ":" ~ value => (name, value)
  }

  sealed trait GroupParser {
    val required: Vector[String]

    val validator: Option[Vector[JsField] => Vector[JsString]]

    implicit def tag: Parser[String]

    def structure: Parser[Vector[JsField]] = field.+ ^^ {
      case fs =>
        val fields = fs.toVector
        // Check for required fields, etc., put warnings in an object
        val warnings: Option[JsField] = {
          val missing: Vector[JsString] = (for {
            name <- required
          } yield {
            if (fields.exists(f => f._1 == name)) None
            else Some(JsString(s"Missing required field '${name}'"))
          }).flatten

          val validation: Vector[JsString] =
            validator.map(v => v(fields)).getOrElse(Vector.empty)

          if (missing.isEmpty && validation.isEmpty) None
          else Some(("_warnings", JsArray(missing ++ validation)))
        }

        warnings match {
          case Some(w) => (fields.groupBy(_._1).map(combine) + w).toVector
          case None    => fields.groupBy(_._1).map(combine).toVector
        }
    }

    // Consolidate multiline and iterative fields into one field
    // If there is only one line of input for a tag, keep as a single string
    // else, combine as an json array of strings
    def combine(e: (String, Vector[JsStringField])): JsField = {
      val name = e._1
      val value: Vector[String] = e._2.map(_._2.value) // Unwrap JsString value from tuple

      if (value.size == 1) (name, JsString(value.head))
      else (name, JsArray(value.map(JsString(_)).toVector))
    }
  }

  object BaseParser extends GroupParser {
    val required = Vector("ACCESSION", "RECORD_TITLE", "DATE", "AUTHORS", "LICENSE")

    val idFormat = "(^[A-Z]{2}\\d{6}$|^[A-Z]{3}\\d{5}$)".r

    val validator = Some({ fields: Vector[JsField] =>
      // Check ID for correct format
      val checkId = fields.filter(_._1 == "ACCESSION") flatMap { accession =>
        // To be defensive, pattern match JsValue to String
        val id = accession._2 match {
          case JsString(value) => value
          case v: JsValue      => v.toString
          case _               => ""
        }

        idFormat.findFirstIn(id) match {
          case Some(_) => None
          case None    => Some(JsString(s"ID format of '${id}' is incorrect"))
        }
      }

      // Other validations here...
      checkId
    })

    implicit def tag: Parser[String] = """[A-Z_]+""".r
  }

  object CHParser extends GroupParser {
    val required = Vector("NAME", "COMPOUND_CLASS", "FORMULA", "EXACT_MASS",
      "SMILES", "IUPAC")

    val validator = None

    implicit def tag: Parser[String] = """CH\$[A-Z_]+""".r
  }

  object SPParser extends GroupParser {
    val required = Vector.empty

    val validator = None

    implicit def tag: Parser[String] = """SP\$[A-Z_]+""".r
  }

  object ACParser extends GroupParser {
    val required = Vector("INSTRUMENT", "INSTRUMENT_TYPE", "MASS_SPECTROMETRY: MS_TYPE",
      "MASS_SPECTROMETRY: ION_MODE")

    val validator = None

    implicit def tag: Parser[String] = """AC\$[A-Z_]+""".r
  }

  object MSParser extends GroupParser {
    val required = Vector.empty

    val validator = None

    implicit def tag: Parser[String] = """MS\$[A-Z_]+""".r
  }

  object PKParser extends GroupParser {
    val required = Vector("ANNOTATION", "NUM_PEAK", "PEAK")

    val validator = None

    implicit def tag: Parser[String] = """PK\$[A-Z_]+""".r

    override def structure: Parser[Vector[JsField]] =
      ("PK$ANNOTATION:" ~> string).? ~ peakField ^^ {
        case Some(annotation) ~ peaks =>
          Vector(("PK$ANNOTATION", annotation), ("_peaks", peaks))
        case None ~ peaks =>
          Vector(("_peaks", peaks))
      }

    val peakTag = "PK$PEAK: m/z int. rel.int."

    // TODO: Exit gracefully, do not throw an exception
    def peakField: Parser[JsArray] = "PK$NUM_PEAK:" ~ int ~ peakTag ~ peakTriple.* ^^ {
      case _ ~ numPeaks ~ _ ~ peaks =>
        val expected = numPeaks.value.toInt
        val actual = peaks.size
        if (expected == actual) JsArray(peaks.toVector)
        else throw badRequest(s"Expected ${expected} peaks, found ${actual}")
    }

    // As discussed with Dr. Hiroshi Tsugawa, eventhough the specifications say that
    // triples should be (float ~ float ~ int), he believes that it should be
    // (float ~ int ~ float). For generality, this parser accommodates both.
    def peakTriple: Parser[JsArray] = float ~ float ~ float ^^ {
      case mz ~ abs ~ rel => JsArray(mz, abs, rel)
    }
  }

  def structure: Parser[JsObject] =
    BaseParser.structure.? ~
      CHParser.structure.? ~
      SPParser.structure.? ~
      ACParser.structure.? ~
      MSParser.structure.? ~
      PKParser.structure.? <~
      "//" ^^ {
        case base ~ chFields ~ spFields ~ acFields ~ msFields ~ pkFields =>

          val groups = Vector(base, chFields, spFields, acFields, msFields, pkFields)

          val fields = groups flatMap (_.getOrElse(Vector.empty))

          val warnings = fields.filter(_._1 == "_warnings")
            .map(_._2.convertTo[Vector[String]])
            .foldLeft(Vector.empty[String])(_ ++ _)
            .map(JsString.apply) // Convert back to JsString

          // Set other additional fields here post-parsing
          // TODO: [Low] Improve readibility
          val f1 = fields.find(_._1 == "ACCESSION")
            .map(f => valueToJsString(f._2)) match {
              case Some(id) => fields :+ (("_origin_id", id))
              case None     => fields
            }

          val f2 = f1.find(_._1 == "RECORD_TITLE")
            .map(f => valueToJsString(f._2)) match {
              case Some(t) => f1 :+ (("_title", t))
              case None    => f1
            }

          val f3 = f2.find(_._1 == "MS$FOCUSED_ION").map(f => valueToJsString(f._2)) match {
            case Some(str) =>
              val pattern = """PRECURSOR_M/Z [-+]?[0-9]*\.?[0-9]+""".r
              pattern.findFirstIn(str.toString) match {
                case Some(kv) =>
                  // Because we use a regex pattern we can skip exception checking
                  val mz = kv.split(" ").apply(1).toDouble
                  f2 :+ (("_precursor_mz", JsNumber(mz)))
                case None => f2
              }
            case None => f2
          }

          val f4 = f3 :+ (("_reader", JsString(this.toString)))

          if (warnings.isEmpty) JsObject(f4.toMap)
          else {
            val f5 = f4 :+ (("_warnings", JsArray(warnings)))
            JsObject(f5.toMap)
          }
      }
}
