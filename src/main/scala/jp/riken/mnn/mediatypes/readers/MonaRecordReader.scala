package jp.riken.mnn.mediatypes.readers

import jp.riken.mnn.json.SprayJsonUtils._
import jp.riken.mnn.mediatypes.MassSpectraSingleReader
import scala.util.parsing.combinator.JavaTokenParsers
import spray.json.JsField
import spray.json._

/**
  * Parses a NIST string stream into a JSON AST (flat)
  */
object MonaRecordReader extends MassSpectraSingleReader with DefaultJsonProtocol {
  val version = 1

  val mediaType = "application/vnd.mona"

  def peakDelim: Parser[String] = """\s\t|\,|\;|\:|\(|\)|\[|\]|\{|\}""".r

  def structure: Parser[JsObject] = jsonObject ^^ { obj =>
    // _title => $id for $biologicalCompound.names.name || $chemicalCompound.names.name
    // _origin_id => hash
    // _precursor_mz => metaData[{name = precursormz}]
    // _inchi_key => $biologicalCompound.inchiKey || $biologicalCompound.inchiKey
    // _precursor_type => metaData[{name = precursortype}]
    // _retention_time => metaData[{name = retentiontime}]
    // _formula => (bio or chem) . metaData[{name = molecule formula}]
    // _ms_type => metaData[{name = ms type}]
    // _ion_mode => metaData[{name = ion mode}]
    // _peaks => spectrum (parse)

    def metadataStripper(meta: JsArray): Vector[JsField] = {
      val elems: Vector[Option[JsField]] =
        meta.elements map (_.asJsObject) map { obj =>
          for {
            name <- obj.fields.get("name")
            value <- obj.fields.get("value")
          } yield (valueToString(name) -> value)
        }
      elems.flatten.toVector
    }

    def findMetadata(metadata: Option[Vector[JsField]], key: String, name: String): Option[JsField] =
      for {
        meta <- metadata
        value <- meta.find(_._1 == key) map (_._2)
      } yield (name, value)

    val _title = for {
      id <- obj.fields.get("id")
      name <- {
        find(obj, "biologicalCompound", "names", "name")
          .orElse(find(obj, "chemicalCompound", "names", "name"))
          .orElse(Some("(undefined)"))
      }
    } yield ("_title", JsString(s"${id} for ${name}"))

    val _origin_id = obj.fields.get("hash") map (hash => ("_origin_id", hash))

    // From metaData field
    val metadata: Option[Vector[JsField]] = obj.fields.get("metaData") map {
      case arr: JsArray => metadataStripper(arr)
      case _            => Vector.empty
    }

    val _precursor_mz: Option[JsField] = findMetadata(metadata, "precursormz", "_precursor_mz")
    val _precursor_type: Option[JsField] = findMetadata(metadata, "precursortype", "_precursor_type")
    val _retention_time: Option[JsField] = findMetadata(metadata, "retentiontime", "_retention_time")
    val _ms_type: Option[JsField] = findMetadata(metadata, "ms type", "_ms_type")
    val _ion_mode: Option[JsField] = findMetadata(metadata, "ion mode", "_ion_mode")

    // From biologicalCompound or chemicalCompound
    val _inchi_key: Option[JsField] = for {
      key <- {
        find(obj, "biologicalCompound", "inchiKey")
          .orElse(find(obj, "chemicalCompound", "inchiKey"))
      }
    } yield ("_inchi_key", key)

    val _formula: Option[JsField] = {
      val bioMeta = find(obj, "biologicalCompound", "metaData") map {
        case arr: JsArray => metadataStripper(arr)
        case _            => Vector.empty
      }
      val chemMeta = find(obj, "chemicalCompound", "metaData") map {
        case arr: JsArray => metadataStripper(arr)
        case _            => Vector.empty
      }

      findMetadata(bioMeta, "molecule formula", "_formula")
        .orElse(findMetadata(chemMeta, "molecule formula", "_formula"))
    }

    val _peaks: Option[JsField] = obj.fields.get("spectrum") map {
      case JsString(value) =>
        val x = value.split(' ').toVector.map {
          peaks =>
            val peak = peaks.split(':').map(_.toDouble)
            JsArray(anyToJsValue(peak(0)), anyToJsValue(peak(1)))
        }
        ("_peaks", JsArray(x))
      case _ => ("_peaks", JsNull)
    }

    val fields = Vector(_title, _origin_id, _precursor_mz, _precursor_type, _retention_time, _ms_type, _ion_mode, _inchi_key, _formula, _peaks).flatten ++ (metadata getOrElse Nil)
    JsObject(fields: _*)
  }
}

