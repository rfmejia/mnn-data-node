package jp.riken.mnn.mediatypes

import jp.riken.mnn.ObjectRegistry
import jp.riken.mnn.mediatypes._
import jp.riken.mnn.types._
import jp.riken.mnn.ErrorResponses
import scala.util.{ Try, Success, Failure }
import spray.http._
import spray.json._

trait RepresentationTools extends ErrorResponses {

  def convertListToRepresentation(data: Seq[RawData], writer: MassSpectraListWriter): Try[RawData] = {
    val mapped: Try[Seq[JsObject]] = {
      val result: Seq[Try[JsObject]] = data.map { d =>
        if (d.mediaType == writer.registeredMediaType) {
          ObjectRegistry.readers.of(d.mediaType) match {
            case Some(reader) => reader.readOne(d)
            case None =>
              throw unsupportedMediaType(s"No suitable mapper found for ${d.mediaType} to ${writer.mediaType}")
          }
        } else {

          val mappers = ObjectRegistry.mappers.of(d.mediaType, writer.registeredMediaType)
          if (mappers.isEmpty)
            throw unsupportedMediaType(s"No suitable mapper found for ${d.mediaType} to ${writer.mediaType}")
          else {
            // Note: Use the first mapper, consider addressing this in future versions.
            val mapper = mappers.head
            for {
              r <- mapper.reader.readOne(d)
              m <- mapper.transform(r)
            } yield m
          }
        }
      }
      Try(result.map(_.get))
    }
    for {
      objs <- mapped
      data <- writer.writeRaw(objs)
    } yield data
  }

  // Note: Use the first mapper, consider addressing this in future versions.
  def convertSingleToRepresentation(data: RawData, mapper: MassSpectraMapper): Try[RawData] =
    if (data.mediaType == mapper.target) Success(data)
    else mapper.map(data)
}
