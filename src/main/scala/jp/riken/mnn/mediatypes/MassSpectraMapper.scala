package jp.riken.mnn.mediatypes

import jp.riken.mnn.ObjectRegistry
import jp.riken.mnn.types.RawData
import scala.util.{ Try, Success, Failure }
import spray.http.MediaType
import spray.json.JsObject

/**
  * Defines field mappings from a resource of media type A into (one or more) media type B.
  */
abstract class MassSpectraMapper(val reader: MassSpectraReader, val writer: MassSpectraWriter) {
  val version: Int

  val source = reader.registeredMediaType
  val target = writer.registeredMediaType

  override def toString = s"mapper=v${version};reader='${reader}';writer='${writer}'"

  /**
    * Transforms a model produced from a source media type into a model for the target media type.
    */
  def transform(input: JsObject): Try[JsObject]

  override def equals(o: Any) = o match {
    case that: MassSpectraMapper => this.toString.equals(that.toString)
    case _                       => false
  }

  override def hashCode = this.toString.hashCode

  /** Convenience method to convert data from a source to target media type. */
  def map(input: RawData): Try[RawData] =
    for {
      r <- reader.readOne(input)
      m <- transform(r)
      w <- writer.writeOne(m)
    } yield w
}
