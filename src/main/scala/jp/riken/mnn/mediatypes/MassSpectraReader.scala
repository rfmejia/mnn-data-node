package jp.riken.mnn.mediatypes

import jp.riken.mnn.ErrorResponses
import jp.riken.mnn.types.RawData
import scala.io.Source
import scala.util.parsing.combinator.JavaTokenParsers
import spray.json._
import spray.json.JsField

trait MassSpectraReader extends MediaTypeHandler with JavaTokenParsers with ErrorResponses {
  type JsStringField = (String, JsString)

  // Literals
  def float: Parser[JsNumber] = floatingPointNumber ^^ { JsNumber(_) }
  def int: Parser[JsNumber] = wholeNumber ^^ { JsNumber(_) }
  def string: Parser[JsString] = ".*".r ^^ { s => JsString(s.trim) }
  def nonEmptyString: Parser[JsString] = ".+".r ^^ { s => JsString(s.trim) }
  def space: Parser[JsString] = " " ^^^ (JsString(" "))
  def jsonObject: Parser[JsObject] = ".+".r ^^ { s => s.parseJson.asJsObject }

  override def toString = s"reader='${mediaType}';version=v${version}"

  /** @return a single JSON object as a result of reading some string */
  def readOne(input: String): util.Try[JsObject]

  /** @return a single JSON object as a result of reading some byte array */
  def readOne(input: Array[Byte]): util.Try[JsObject]

  /** @return a single JSON object as a result of reading some RawData */
  def readOne(input: RawData): util.Try[JsObject]
}

abstract class MassSpectraSingleReader extends MassSpectraReader {
  /** A parser combinator for the target media type. */
  def structure: Parser[JsObject]

  def read(input: String): util.Try[JsObject] = for {
    in <- {
      if (input != null && !input.isEmpty) util.Success(input.trim)
      else util.Failure(badRequest("Empty input received"))
    }
    result <- parseAll(structure, in) match {
      case Success(res, _)   => util.Success(res)
      case NoSuccess(msg, _) => util.Failure(badRequest(msg))
    }
  } yield result

  def read(input: Array[Byte]): util.Try[JsObject] = read(new String(input, "UTF-8"))

  def read(input: RawData): util.Try[JsObject] = for {
    data <- input.getData
    result <- this.read(data)
  } yield result

  def readOne(input: String): util.Try[JsObject] = read(input)
  def readOne(input: Array[Byte]): util.Try[JsObject] = read(input)
  def readOne(input: RawData): util.Try[JsObject] = read(input)
}

/** Splits a single raw data source with a list of resources into multiple raw data sources. */
abstract class MassSpectraListReader extends MassSpectraReader {
  def read(input: String): util.Try[Vector[RawData]]
  def read(input: Array[Byte]): util.Try[Vector[RawData]] = read(new String(input, "UTF-8"))
  def read(input: RawData): util.Try[Vector[RawData]] =
    for {
      data <- input.getData
      result <- this.read(data)
    } yield result

  def readToJsObject(input: String): util.Try[Vector[JsObject]]
  def readToJsObject(input: Array[Byte]): util.Try[Vector[JsObject]] = readToJsObject(new String(input, "UTF-8"))
  def readToJsObject(input: RawData): util.Try[Vector[JsObject]] =
    for {
      data <- input.getData
      result <- this.readToJsObject(data)
    } yield result

  private def getHead(x: util.Try[Vector[JsObject]]): util.Try[JsObject] =
    x map (y => y.headOption match {
      case Some(d) => d
      case None    => throw new IllegalArgumentException("Could not extract head of JSON object list")
    })

  def readOne(input: String): util.Try[JsObject] = getHead(readToJsObject(input))
  def readOne(input: Array[Byte]): util.Try[JsObject] = getHead(readToJsObject(input))
  def readOne(input: RawData): util.Try[JsObject] = getHead(readToJsObject(input))
}
