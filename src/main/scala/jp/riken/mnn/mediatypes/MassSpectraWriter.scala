package jp.riken.mnn.mediatypes

import jp.riken.mnn.types.RawData
import scala.util.{ Try, Success, Failure }
import spray.http.MediaType
import spray.json.JsObject

trait MassSpectraWriter extends MediaTypeHandler {
  override def toString = s"writer='${mediaType}';version=v${version}"

  /** @return RawData as a result of reading some JsObject */
  def writeOne(input: JsObject): Try[RawData]
}

abstract class MassSpectraSingleWriter extends MassSpectraWriter {
  def write(input: JsObject): Try[String]

  def writeRaw(obj: JsObject): Try[RawData] = for {
    data <- write(obj)
    raw <- RawData(registeredMediaType, data)
  } yield raw

  def writeOne(input: JsObject): Try[RawData] = writeRaw(input)
}

abstract class MassSpectraListWriter extends MassSpectraWriter {
  def write(input: Seq[JsObject]): Try[String]

  def writeRaw(objs: Seq[JsObject]): Try[RawData] = for {
    data <- write(objs)
    raw <- RawData(registeredMediaType, data)
  } yield raw

  def writeOne(input: JsObject): Try[RawData] = writeRaw(Seq(input))
}
