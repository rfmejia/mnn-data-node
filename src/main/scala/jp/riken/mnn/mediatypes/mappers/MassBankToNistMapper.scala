package jp.riken.mnn.mediatypes.mappers

import collection.mutable
import jp.riken.mnn.json.SprayJsonUtils._
import jp.riken.mnn.mediatypes.MassSpectraMapper
import jp.riken.mnn.mediatypes.readers.MassBankReader
import jp.riken.mnn.mediatypes.writers.NistWriter
import scala.util.Try
import spray.json._

object MassBankToNistMapper extends MassSpectraMapper(MassBankReader, NistWriter) {
  val version = 1

  val fieldMapping: Map[String, String] = Map(
    "RECORD_TITLE" -> "NAME",
    "COMMENT" -> "COMMENT",
    "CH$FORMULA" -> "FORMULA"
  )

  def hasMapping(field: JsField): Boolean = fieldMapping.keys.exists(_ == field._1)

  def transform(model: JsObject) = Try {
    val standard = model.fields.filter(!_._1.startsWith("_"))
    val special = model.fields.filter(_._1.startsWith("_"))
    val withMapping = standard.filter(hasMapping(_))
    val withoutMapping = standard.filter(!hasMapping(_))
    val peaks = special.filter(_._1 == "_peaks")

    val fields = mutable.Map.empty[String, JsValue]

    fields ++= withMapping.map { field =>
      val oldName = field._1
      val newName = fieldMapping.get(oldName).get
      (newName, field._2)
    }

    // Copy those without mapping, or do more processing
    fields ++= withoutMapping
    val info = JsString(this.toString)
    fields.get("COMMENT") match {
      case Some(comm) => fields += (("COMMENT", JsArray(info +: valueToList(comm))))
      case None       => fields += (("COMMENT", info))
    }
    fields ++= peaks

    JsObject(fields.toMap)
  }
}
