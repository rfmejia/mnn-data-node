package jp.riken.mnn.mediatypes.mappers

import collection.mutable
import jp.riken.mnn.json.SprayJsonUtils._
import jp.riken.mnn.mediatypes._
import jp.riken.mnn.mediatypes.readers.NistReader
import jp.riken.mnn.mediatypes.writers.MassBankWriter
import scala.util.{ Try, Success, Failure }
import spray.json._

object NistToMassBankMapper extends MassSpectraMapper(NistReader, MassBankWriter) {
  val version = 1

  val fieldMapping: mutable.Map[String, String] = mutable.Map(
    "NAME" -> "RECORD_TITLE",
    "COMMENT" -> "COMMENT",
    "FORMULA" -> "CH$FORMULA"
  )

  def hasMapping(field: JsField): Boolean = fieldMapping.keys.exists(_ == field._1)

  def transform(model: JsObject): Try[JsObject] = Try {

    val standard = model.fields.filter(!_._1.startsWith("_"))
    val special = model.fields.filter(_._1.startsWith("_"))
    val withMapping = standard.filter(hasMapping(_))
    val withoutMapping = standard.filter(!hasMapping(_))
    val peaks = special.filter(_._1 == "_peaks")

    val fields = mutable.Map.empty[String, JsValue]

    fields ++= withMapping.map { field =>
      val oldName = field._1
      val newName = fieldMapping.get(oldName).get
      (newName, field._2)
    }

    // Copy those without mapping, or do more processing
    // Save other fields as comments array
    val unmapped = JsArray(withoutMapping.map {
      f => JsString(f._1 + ": " + valueToString(f._2))
    }.toVector)

    val comments = JsString(this.toString) +: unmapped.elements
    fields.get("COMMENT") match {
      case Some(comm) => fields += (("COMMENT", JsArray(comments ++ valueToList(comm))))
      case None       => fields += (("COMMENT", JsArray(comments)))
    }
    fields ++= peaks

    if (!fields.exists(_._1 == "ACCESSION")) {
      fields += (("ACCESSION", JsString("........")))
    }

    JsObject(fields.toMap)
  }
}
