package jp.riken.mnn.mediatypes.mappers

import jp.riken.mnn.mediatypes.MassSpectraMapper
import jp.riken.mnn.mediatypes.readers.NistReader
import jp.riken.mnn.mediatypes.writers.NistListWriter
import scala.util.{ Try, Success }
import spray.json.JsObject

object NistToNistListMapper extends MassSpectraMapper(NistReader, NistListWriter) {
  val version = 1

  def transform(model: JsObject): Try[JsObject] = Success(model)
}
