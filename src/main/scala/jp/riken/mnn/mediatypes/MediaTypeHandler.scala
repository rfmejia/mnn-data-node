package jp.riken.mnn.mediatypes

import jp.riken.mnn.ObjectRegistry
import jp.riken.mnn.types.RegisteredMediaType
import jp.riken.mnn.ErrorResponses

trait MediaTypeHandler {
  val version: Int
  val mediaType: String

  lazy val registeredMediaType: RegisteredMediaType = ObjectRegistry.registeredMediaTypes.find(mediaType) match {
    case Some(mt) => mt
    case None     => throw ErrorResponses.serverError(s"The media type handler '${this}' is not a registered media type.")
  }

  override def toString = s"handler='${mediaType}';version=v${version}"
}
