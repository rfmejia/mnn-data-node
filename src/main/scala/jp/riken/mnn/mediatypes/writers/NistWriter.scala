package jp.riken.mnn.mediatypes.writers

import jp.riken.mnn.json.SprayJsonUtils._
import jp.riken.mnn.mediatypes.MassSpectraSingleWriter
import jp.riken.mnn.types.RawData
import scala.util.{ Try, Success, Failure }
import spray.json.{ JsArray, JsObject }

object NistWriter extends MassSpectraSingleWriter {
  val mediaType = "application/vnd.nist"

  val version = 1

  def write(obj: JsObject): Try[String] = Try {
    val (special, fields) = obj.fields.partition(_._1.startsWith("_"))

    val name = fields.find(_._1.equalsIgnoreCase("name")) match {
      case Some(n) => valueToString(n._2)
      case None    => "[mnn: Could not find name]"
    }

    val fs = {
      val list = fields.filterKeys(!_.equalsIgnoreCase("name")) map { f =>
        val key = f._1
        val value = valueToString(f._2)
        s"${key}: ${value}"
      }
      list.reduce(_ + "\n" + _)
    }

    val ps = special.get("_peaks") map { arr =>
      val pairs = valueToList(arr).map(valueToList(_))
      val list = pairs map { pair =>
        val mz = pair.head
        val intensity = pair.tail.head
        s"${mz} ${intensity};"
      }
      s"Num Peaks: ${list.size}\n" +
        list.reduce(_ + " " + _)
    }

    val result: String = s"NAME: ${name}\n" + fs + "\n" + ps.getOrElse("")
    result
  }
}
