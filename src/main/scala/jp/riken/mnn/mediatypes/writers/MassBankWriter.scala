package jp.riken.mnn.mediatypes.writers

import jp.riken.mnn.json.SprayJsonUtils._
import jp.riken.mnn.mediatypes.MassSpectraSingleWriter
import jp.riken.mnn.types.RawData
import scala.util.{ Try, Success, Failure }
import spray.json._

object MassBankWriter extends MassSpectraSingleWriter {
  val mediaType = "application/vnd.massbank"

  val version = 1

  def write(obj: JsObject): Try[String] = Try {
    val (special, fields) = obj.fields.partition(_._1.startsWith("_"))

    val fs = {
      val list = fields map { f =>
        val name = f._1
        f._2 match {
          case JsArray(elements) =>
            val lines = for (e <- elements) yield {
              val value = valueToString(e)
              s"${name}: ${value}"
            }
            lines.reduce(_ + "\n" + _)
          case v: JsValue =>
            val value = valueToString(v)
            s"${name}: ${value}"
        }
      }
      list.reduce(_ + "\n" + _)
    }

    val ps = special.get("_peaks") map { arr =>
      // Two cases: If given triples, print as m/z int. rel.int.
      // If given pairs, compute for relative intensity then print

      def formatPeaks(list: Vector[String]) =
        "PK$NUM_PEAK: " + list.size + "\n" +
          "PK$PEAK: m/z int. rel.int.\n" +
          list.reduce(_ + "\n" + _)

      val numbers = valueToList(arr).map(valueToList(_))

      if (numbers.size == 0) "PK$NUM_PEAK: 0"

      else if (numbers.head.size == 2) {
        val intensities = numbers.map(_(1).toString.toFloat)
        val max = intensities.max

        val list = numbers map { pair =>
          val mz = pair(0)
          val intensity = pair(1).toString.toFloat
          val relative = (intensity / max) * 999
          s"  ${mz} ${intensity} ${relative.toInt}"
        }

        formatPeaks(list)
      } else {
        val list = numbers map { triple =>
          val mz = triple(0)
          val intensity = triple(1)
          val relative = triple(2)
          s"  ${mz} ${intensity} ${relative}"
        }
        formatPeaks(list)
      }
    }

    val result: String = fs + "\n" +
      ps.getOrElse("") + "\n" +
      "//"
    result
  }
}
