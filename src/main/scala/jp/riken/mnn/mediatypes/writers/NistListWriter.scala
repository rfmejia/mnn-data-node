package jp.riken.mnn.mediatypes.writers

import jp.riken.mnn.mediatypes.MassSpectraListWriter
import scala.util.{ Try, Success, Failure }
import spray.json._

object NistListWriter extends MassSpectraListWriter {
  val version = 1

  val mediaType = "application/vnd.nist.list"

  def write(input: Seq[JsObject]): Try[String] = Try {
    val result = input.map(NistWriter.write(_))
    result.map(_.get).mkString("\n\n")
  }
}
