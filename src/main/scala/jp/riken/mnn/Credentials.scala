package jp.riken.mnn

import com.typesafe.config.Config
import java.util.Properties
import jp.riken.mnn.util.Validators._
import jp.riken.mnn.util.StringOps._
import scala.util.Try

case class Credentials(val url: String, val driver: String, val username: Option[String] = None,
    val password: Option[String] = None, propMap: Option[Map[String, String]] = None) {

  val prop = {
    val p = new Properties()
    if (propMap.isDefined) propMap.get foreach { case (k, v) => p.put(k, v) }
    p
  }
}

object Credentials {
  def fromConfig(c: Config): Try[Credentials] = Try {
    val url = c.getString("project.url")
    val driver = c.getString("project.driver")

    // Allow empty string for username and password
    val username = if (c.hasPath("project.username")) Some(c.getString("project.username"))
    else None
    val password = if (c.hasPath("project.password")) Some(c.getString("project.password"))
    else None
    Credentials(url, driver, username, password)
  }
}
