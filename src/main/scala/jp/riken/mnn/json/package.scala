package jp.riken.mnn

import spray.json._
import scala.language.implicitConversions
import spray.http.Uri
import jp.riken.mnn.json.ExtendedJsonProtocol._

package object json {

  /** Value class for `spray.json.JsObject` that implements JsonApi version 1.0 stable */
  implicit class RichJsObject(val underlying: JsObject) extends AnyVal {
    def ++(that: JsObject): JsObject = JsObject(underlying.fields ++ that.fields)

    def addAttributes(members: JsField*): JsObject = JsonApi.addAttributes(underlying)(members: _*)

    def addCollectionData(members: Seq[JsObject]): JsObject = JsonApi.addCollectionData(underlying)(members)

    def addData(members: JsObject*): JsObject = JsonApi.addData(underlying)(members)

    def addLinks(members: JsField*): JsObject = JsonApi.addLinks(underlying)(members: _*)

    def addMeta(members: JsField*): JsObject = JsonApi.addMeta(underlying)(members: _*)

    def addRelationships(members: JsField*): JsObject = JsonApi.addRelationships(underlying)(members: _*)

    def addSelf(s: String): JsObject = JsonApi.addLinks(underlying)("self" -> s.toJson)
    def addSelf(u: Uri): JsObject = addSelf(u.toString)

    def addCollectionNavLinks(base: Uri, total: Int, offset: Int, limit: Int): JsObject =
      JsonApi.addCollectionNavLinks(underlying)(base, total, offset, limit)
  }
}
