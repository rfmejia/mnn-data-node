package jp.riken.mnn.json

import jp.riken.mnn.ObjectRegistry
import jp.riken.mnn.ObjectRegistry._
import jp.riken.mnn.database.indexing.TextSearchResult
import jp.riken.mnn.database.{ DB, MetadataManager }
import jp.riken.mnn.json.SprayJsonUtils._
import jp.riken.mnn.mediatypes._
import jp.riken.mnn.types._
import jp.riken.mnn.util.LinkingTools
import scala.slick.driver.H2Driver.simple._
import scala.slick.lifted.Query
import scala.util.{ Try, Success, Failure }
import spray.http.Uri
import spray.http.Uri.Path
import spray.json._

trait HalJsObjectTools extends LinkingTools with ExtendedJsonProtocol {
  // Note: internals of this class use the following mutable variable; you have been warned...
  private class HalJsBuilder(var obj: HalJsObject, v: View) {
    def applyIf(name: String, f: HalJsObject => HalJsObject): HalJsBuilder = {
      if (v(name)) this.obj = f(obj)
      this
    }

    def apply(f: HalJsObject => HalJsObject): HalJsBuilder = {
      this.obj = f(obj)
      this
    }
  }

  val curie = ("mnn", "http://massbank.jp/docs/profiles/{rel}")

  def convertSingle[T <: Resource](resource: T): JsObject =
    resource match {
      case r: UnregisteredResource => r.toJson.asJsObject
      case r: RegisteredResource   => r.toJson.asJsObject
    }

  def addResourceMetadata(instanceId: InstanceID, obj: JsObject)(implicit s: Session): JsObject = ???
  // add extensive links, metadata, representations, etc.

  def convertSingle[T <: Resource](resource: T, view: View)(implicit self: Uri, session: Session): HalJsObject = {
    val t: HalJsBuilder = new HalJsBuilder(HalJsObject.empty, view)

    t.applyIf("_links.self", _.self(linkto(s"/resources/${resource.id}")))
    t.applyIf("_links.curie", _.withCurie(curie._1, curie._2))
    t.applyIf("id", _.withField("id", resource.id.urn))
    t.applyIf("title", _.withField("title", resource.title))
    t.applyIf("namespace", _.withField("namespace", resource.id.namespace))

    t.applyIf("_links.edit", _.withLink(
      "edit",
      linkto("/resources/edit", Uri.Query("id" -> s"${resource.id}")),
      Some("Update or delete this resource")
    ))

    // TODO: Metadata from parsed_json (prefixed with underscore)
    if (view.fields.keys.exists(_.startsWith("metadata."))) {
      val meta = MetadataManager.get(resource.id)

      val metaObj = new HalJsBuilder(HalJsObject.empty, view)

      metaObj.applyIf("metadata.*", { obj =>
        val ms: Iterable[JsField] = meta.filterNot(_._1 == "_peaks")
          .map {
            case (k, v) =>
              val key = if (k.startsWith("_")) k.substring(1) else k
              (key, anyToJsValue(v))
          }
        val withPeaks: Iterable[JsField] = meta.get("_peaks") map {
          peaks => ms ++ Try { ("peaks", meta("_peaks").parseJson) }.toOption
        } getOrElse ms
        obj.withFields(withPeaks)
      })

      metaObj.applyIf(
        "metadata.origin_id",
        _.withField("origin_id", meta.get("_origin_id"))
      )
      metaObj.applyIf(
        "metadata.title",
        _.withField("title", meta.get("_title"))
      )
      metaObj.applyIf(
        "metadata.reader",
        _.withField("reader", meta.get("_reader"))
      )
      metaObj.applyIf("metadata.warnings", { obj =>
        val msgs = meta.get("_warnings").map(_.parseJson)
        obj.withField("warnings", msgs)
      })

      metaObj.applyIf("metadata.tags", { obj =>
        val tags = MetadataManager.getTags(resource.id)
        obj.withField("tags", tags)
      })

      if (view("metadata.peaks") && meta.get("_peaks").isDefined) {
        val peaks = meta("_peaks").parseJson
        metaObj.applyIf("metadata.peaks", _.withField("peaks", peaks))
      }

      t.apply(_.withField("metadata", metaObj.obj.asJsValue))
    }

    // Show the entire parsed file in JSON format, if any
    t.applyIf("reader_result", { obj =>
      val wrangled: Option[JsObject] = resource.data flatMap { data =>
        val readers = ObjectRegistry.readers.of(data.mediaType)

        if (readers.isEmpty) None
        else {
          // Note: If there is more than one matched parser, use the first match only.
          // This should be addressed in a future version.
          val reader = readers.head

          reader.readOne(data) match {
            case Success(obj) => Some(obj)
            case Failure(_)   => None
          }
        }
      }

      obj.withField("reader_result", wrangled getOrElse JsObject())
    })

    resource match {
      case ur: UnregisteredResource => convertUnregisteredResource(t, ur)
      case rr: RegisteredResource   => convertRegisteredResource(t, rr)
    }

    if (resource.data.isDefined) convertRawData(t, resource.id, resource.data.get)

    t.obj
  }

  private def convertRegisteredResource(t: HalJsBuilder, resource: RegisteredResource)(implicit self: Uri) = {
    t.applyIf("resource_status", { obj =>
      obj.withLink("resource_status", "registered")
    })

    t.applyIf("committed_on", _.withField("committed_on", resource.date))

    t.applyIf("version", _.withField("version", resource.versionedId.version))

    t.applyIf("_links.version-history", { obj =>
      val versionsLink = linkto(s"/resource/${resource.versionedId.asPlainID}/")
      obj.withLink("version-history", versionsLink)
    })
  }

  private def convertUnregisteredResource(t: HalJsBuilder, resource: UnregisteredResource)(implicit self: Uri) = {
    t.applyIf("resource_status", { obj =>
      obj.withLink("resource_status", "unregistered")
    })

    t.applyIf("registered_id", { obj =>
      resource.versionedId.map(vid => obj.withLink("registered_id", vid.urn))
        .getOrElse(obj)
    })
  }

  private def convertRawData(t: HalJsBuilder, id: InstanceID, data: RawData)(implicit self: Uri) = {
    t.applyIf("_links.enclosure", { obj =>
      var url = linkto(s"/resources/${id}/mt/${data.mediaType.hash}")
      obj.withLink("enclosure", url, Some(data.mediaType.toString))
    })

    // Add byte size of resource raw data (add to RawData class)
    t.applyIf("bytes", _.withField("size", data.size))

    t.applyIf("mediatype", _.withField("mediatype", data.mediaType.toString))

    t.applyIf("_links.mnn:representations", { obj =>
      val mediaTypes = ObjectRegistry.mappers.ofSource(data.mediaType).map(_.target)
      val links: Vector[Link] = mediaTypes.map { mt =>
        var url = linkto(s"/resources/${id}/mt/${mt.hash}")
        Link.create(url, Some(mt.toString))
      }
      links.foldLeft(obj) { (o, l) =>
        o.withLink("mnn:representations", l.href, l.title)
      }
    })
  }

  def convertList[T <: Resource](
    rs: Seq[T],
    view: View = View.minimalSingle
  )(implicit self: Uri, session: Session): JsArray =
    JsArray(rs.toVector.map(convertSingle(_, view).asJsValue))

  def convertSlickQuery[T <: Resource](query: Query[_, T, Seq], title: Option[String] = None, offset: Int, limit: Int,
    collectionView: View = View.minimalCollection, singleView: View = View.minimalSingle)(implicit self: Uri, session: Session): HalJsObject = {

    val t: HalJsBuilder = new HalJsBuilder(HalJsObject.empty, collectionView)

    t.applyIf("_links.self", _.self(linkto(self)))

    t.applyIf("_links.profile", _.withLink("profile", "collection"))

    // List navigation
    val total = query.length.run
    generateListNavigation(t, query.length.run, offset, limit)

    if (title.isDefined) t.applyIf("title", _.withField("title", title.get))

    t.applyIf("_embedded", { obj =>
      val items = query.drop(offset).take(limit).list.toVector
      val halItems = HalJsObject.empty.withField(
        "items",
        convertList(items, singleView)
      )
      obj.withEmbedded(halItems).withField("count", items.size)
    })

    t.obj
  }

  def convertTextSearchResult(result: TextSearchResult, view: View, offset: Int,
    limit: Int)(implicit self: Uri, session: Session): HalJsObject = {
    val t: HalJsBuilder = new HalJsBuilder(HalJsObject.empty, view)

    t.applyIf("_links.self", _.self(linkto(self)))

    t.applyIf("_links.profile", _.withLink("profile", "collection"))

    generateListNavigation(t, result.totalHits, offset, limit)

    t.applyIf("scores", { obj =>
      obj.withField("scores", JsArray {
        result.hits map {
          case (id: String, score: Float) =>
            JsObject(("id", JsString(id)), ("score", JsNumber(score)))
        }
      })
    })

    t.applyIf("_embedded", { obj =>
      // Convert search hits into resources
      val items = result.hits flatMap {
        case (id: String, _) =>
          /* TODO: Fix
          ResourceID.parse(id) match {
            case Success(_id) =>
              DB.ids.asVersioned(_id) flatMap (DB.resources.get(_))
            case Failure(_) => None
          }
          */
          None
      }

      if (items.size > 0) {
        val halItems = HalJsObject.empty.withField(
          "items",
          convertList(items, View.minimalSingle)
        )
        obj.withEmbedded(halItems).withField("count", items.size)
      } else {
        obj.withField("count", items.size)
      }
    })

    t.obj
  }

  private def generateListNavigation(t: HalJsBuilder, total: Int,
    offset: Int, limit: Int)(implicit _self: Uri) = {
    val self = linkto(_self)
    t.applyIf("total", _.withField("total", total))
    t.applyIf("_links.first", { obj =>
      if (offset > 0) obj.withLink("first", {
        self.withQuery {
          self.query.toMap ++ Map("offset" -> 0.toString, "limit" -> limit.toString)
        }
      })
      else obj
    });
    t.applyIf("_links.prev", { obj =>
      if (offset > 0) obj.withLink("prev", {
        val start = Math.max((offset - limit), 0)
        self.withQuery {
          self.query.toMap ++ Map("offset" -> start.toString, "limit" -> limit.toString)
        }
      })
      else obj
    });
    t.applyIf("_links.next", { obj =>
      if ((offset + limit) < total) obj.withLink("next", {
        val start = Math.min((offset + limit), total)
        self.withQuery {
          self.query.toMap ++ Map("offset" -> start.toString, "limit" -> limit.toString)
        }
      })
      else obj
    });
    t.applyIf("_links.last", { obj =>
      if ((offset + limit) < total) obj.withLink("last", {
        val start = if (limit != 0) limit * (total / limit) else 0
        self.withQuery {
          self.query.toMap ++ Map("offset" -> start.toString, "limit" -> limit.toString)
        }
      })
      else obj
    });
  }
}
