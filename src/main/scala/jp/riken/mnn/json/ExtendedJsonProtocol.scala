package jp.riken.mnn.json

import java.net.URL
import java.util.UUID
import jp.riken.mnn.types._
import org.joda.time.{ DateTime, DateTimeZone }
import org.joda.time.format.ISODateTimeFormat
import scala.util.{ Try, Success, Failure }
import spray.http.Uri
import spray.json._

/**
  * Additional JSON formats for commonly used types.
  */
trait ExtendedJsonProtocol extends DefaultJsonProtocol {
  private def deserializeJsString[T](value: JsValue)(f: String => T): T = value match {
    case JsString(s) => Try(f(s)) match {
      case Success(x) => x
      case Failure(e) => deserializationError(e.getMessage)
    }
    case x => deserializationError(s"Invalid URI '${x.toString}'")
  }

  implicit object uuidFormat extends JsonFormat[UUID] {
    def read(value: JsValue) = deserializeJsString(value)(UUID.fromString(_))
    def write(id: UUID) = JsString(id.toString)
  }

  implicit object uriFormat extends JsonFormat[Uri] {
    def read(value: JsValue) = deserializeJsString(value)(Uri.apply(_))
    def write(uri: Uri) = JsString(uri.toString)
  }

  private val dateFormat = ISODateTimeFormat.dateTime.withZone(DateTimeZone.UTC)
  private val dateTimeParser = ISODateTimeFormat.dateTimeParser().withZone(DateTimeZone.UTC)
  implicit object dateTimeFormat extends JsonFormat[DateTime] {
    def read(value: JsValue) = deserializeJsString(value)(dateTimeParser.parseDateTime(_))
    def write(date: DateTime) = JsString(date.toString(dateFormat))
  }
}

object ExtendedJsonProtocol extends ExtendedJsonProtocol
