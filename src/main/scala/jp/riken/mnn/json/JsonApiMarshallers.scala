package jp.riken.mnn.json

import jp.riken.mnn.database.{ DB, MetadataManager }
import jp.riken.mnn.types._
import jp.riken.mnn.util.LinkingTools
import scala.slick.driver.H2Driver.simple._
import spray.http.Uri
import spray.json._

trait JsonApiMarshallers extends LinkingTools with ExtendedJsonProtocol {
  def asJsonApi[T <: Resource](resource: T): JsObject =
    resource match {
      case r: UnregisteredResource => r.toJson.asJsObject
      case r: RegisteredResource   => r.toJson.asJsObject
    }

  def addResourceMetadata(instanceId: InstanceID, obj: JsObject)(implicit s: Session): JsObject = ???
  // add extensive links, metadata, representations, etc.

  //def asJsonApi[T]()
}
