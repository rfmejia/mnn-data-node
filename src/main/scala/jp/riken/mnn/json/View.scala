package jp.riken.mnn.json

import scala.language.implicitConversions

/**
  * Creates and manages a Map[String, Boolean], where a field name (key) is to
  * be rendered iff it exists in the map and its value is true. It uses a whitelist approach.
  */

class View(val fields: Map[String, Boolean], showAll: Boolean = false) {
  /** Shows specific fields */
  def show(names: String*): View = new View(fields ++ names.map(_ -> true), showAll)

  /** Hides specific fields */
  def hide(names: String*): View = new View(fields ++ names.map(_ -> false), showAll)

  /** Perform group editing of fields based on predetermined keywords */
  def withView(view: String): View = view match {
    case "meta_summary" => show("metadata.tags", "metadata.origin_id",
      "metadata.title", "metadata.reader", "metadata.warnings")
    case "meta_full"       => show("metadata.*")
    case "modified"        => show("_links.edit", "otherMediaTypes", "modified")
    case "hide_navigation" => hide("_links.first", "_links.prev", "_links.next", "_links.last")
    case _                 => this
  }

  /** True iff name is listed and value is true, else false */
  def apply(name: String): Boolean = showAll || fields.get(name.toLowerCase).getOrElse(false)

  def merge(v: View): View = fields ++ v.fields
  def ++(v: View): View = merge(v)
}

object View {
  implicit def mapToView(m: Map[String, Boolean]): View = new View(m)
  implicit def viewToMap(v: View): Map[String, Boolean] = v.fields

  val empty = new View(Map.empty[String, Boolean])

  val minimalSingle: View = new View(Map(
    "_links.profile" -> true,
    "_links.self" -> true,
    "id" -> true,
    "title" -> true,
    "mediatype" -> true
  ))

  val defaultSingle: View = minimalSingle ++ new View(Map(
    "_links.curie" -> true,
    "_links.enclosure" -> true,
    "_links.mnn:representations" -> true,
    "version" -> true,
    "modified" -> true,
    "resource_status" -> true
  ))

  val minimalCollection: View = new View(Map(
    "_links.profile" -> true,
    "_links.self" -> true,
    "title" -> true,
    "total" -> true
  ))

  val defaultCollection: View = minimalCollection ++ new View(Map(
    "_links.first" -> true,
    "_links.prev" -> true,
    "_links.next" -> true,
    "_links.last" -> true,
    "_links.curie" -> true,
    "_embedded" -> true
  ))

  def fromParams(params: Map[String, String]) = {
    val view = params.get("view").map(_.split(","))
    val fields = params.get("fields").map(_.split(","))
    val hideFields = params.get("hide_fields").map(_.split(","))

    val view1 = view match {
      case Some(vs) => vs.foldLeft(View.empty)(_.withView(_))
      case None     => View.empty
    }

    val view2 = fields match {
      case Some(fs) => fs.foldLeft(view1)(_.show(_))
      case None     => view1
    }

    val view3 = hideFields match {
      case Some(fs) => fs.foldLeft(view2)(_.hide(_))
      case None     => view2
    }

    view3
  }
}
