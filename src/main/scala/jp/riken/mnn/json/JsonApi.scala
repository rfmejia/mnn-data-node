package jp.riken.mnn.json

import scala.collection.mutable
import spray.http.Uri
import spray.json._

/**
  * This is kept as static because its primary use is to be a value class. See the `jp.riken.mnn.json` package object
  *  for more information
  */
object JsonApi extends ExtendedJsonProtocol {
  val mediaType = "application/vnd.api+json"

  val version = "1.0"

  /** Creates a resource object */
  def resource(resType: String, id: String): JsObject =
    JsObject("type" -> resType.toJson, "id" -> id.toJson)

  def collection(members: Seq[JsObject]) =
    addCollectionData(JsObject.empty)(members)
      .addMeta("count" -> members.length.toJson)

  private def overwriteJsObject(fieldName: String)(obj: JsObject, members: JsField*): JsObject = {
    val attrs: JsObject = obj.fields.get(fieldName) match {
      case Some(JsObject(fields)) => JsObject(fields ++ Map(members: _*))
      case Some(_)                => deserializationError(s"`${fieldName}` is not a JSON object")
      case None                   => JsObject(members: _*)
    }
    JsObject(obj.fields + ((fieldName -> attrs)))
  }

  def addAttributes(obj: JsObject)(members: JsField*): JsObject = overwriteJsObject("attributes")(obj, members: _*)

  def addLinks(obj: JsObject)(members: JsField*): JsObject = overwriteJsObject("links")(obj, members: _*)

  def addMeta(obj: JsObject)(members: JsField*): JsObject = overwriteJsObject("meta")(obj, members: _*)

  def addRelationships(obj: JsObject)(members: JsField*): JsObject =
    overwriteJsObject("relationships")(obj, members: _*)

  def addData(obj: JsObject)(members: Seq[JsObject]): JsObject =
    if (members.isEmpty) obj
    else {
      obj.fields.get("data") match {
        case Some(o: JsObject) =>
          val d = JsArray(Vector(o, members.head))
          addData(JsObject(obj.fields + ("data" -> d)))(members.tail)
        case Some(JsArray(elems)) =>
          val d = JsArray(elems :+ members.head)
          addData(JsObject(obj.fields + ("data" -> d)))(members.tail)
        case None    => addData(JsObject(obj.fields + ("data" -> members.head)))(members.tail)
        case Some(_) => deserializationError(s"Expected data object, found '${obj.toString}'")
      }
    }

  def addCollectionData(obj: JsObject)(members: Seq[JsObject]): JsObject =
    obj.fields.get("data") match {
      case Some(o: JsObject) => addData(obj)(o +: members)
      case Some(JsArray(e))  => addData(obj)((e ++ members).map(_.asJsObject))
      case None              => addData(JsObject(obj.fields + ("data" -> JsArray())))(members)
      case Some(_)           => deserializationError(s"Expected data object, found '${obj.toString}'")
    }

  def addCollectionNavLinks(obj: JsObject)(base: Uri, total: Int, offset: Int, limit: Int): JsObject = {
    def withOffset(off: Int): Uri = base.withQuery {
      base.query.toMap ++ Map("offset" -> off.toString, "limit" -> limit.toString)
    }

    val links = mutable.ListBuffer.empty[JsField]
    val pages = total / limit
    if (offset > 0) {
      links ++= Seq(
        "first" -> withOffset(0).toJson,
        "prev" -> withOffset(Math.max(offset - limit, 0)).toJson
      )
    }
    if (offset + limit < total) {
      links ++= Seq(
        "next" -> withOffset(offset + limit).toJson,
        "last" -> withOffset(limit * pages).toJson
      )
    }

    obj.addLinks(links.toVector: _*)
      .addMeta(
        "total" -> total.toJson,
        "pages" -> (pages + 1).toJson
      )
  }
}
