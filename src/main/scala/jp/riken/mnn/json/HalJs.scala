package jp.riken.mnn.json

import jp.riken.mnn.json.SprayJsonUtils._
import jp.riken.mnn.ObjectRegistry.registeredMediaTypes.`application/hal+json`
import scala.collection.{ immutable, mutable }
import spray.json._

sealed trait HalJsValue {
  val asJsValue: JsValue
}

case class HalJsObject(fields: immutable.Map[String, JsValue], links: HalJsLinks,
    embedded: Option[HalJsObject]) extends HalJsValue {

  val self = links.self
  def self(href: String) = this.withLink("self", href)

  def withFields(itr: Iterable[(String, JsValue)]): HalJsObject =
    this.copy(fields = fields ++ itr)

  def withFields(itr: (String, JsValue)*): HalJsObject =
    withFields(itr.toList)

  def withField(name: String, value: JsValue): HalJsObject =
    this.copy(fields = fields + ((name, value)))

  def withField(name: String, opt: Option[Any]): HalJsObject = opt match {
    case Some(value) => withField(name, anyToJsValue(value))
    case None        => this
  }

  def withField(name: String, value: Any): HalJsObject =
    withField(name, Option(value))

  def withLink(rel: String, href: String, title: Option[String] = None): HalJsObject =
    this.copy(links = links.add(rel, href, title))

  def withCurie(name: String, href: String): HalJsObject =
    this.copy(links = links.curie(name, href))

  def withEmbedded(obj: HalJsObject): HalJsObject =
    this.copy(embedded = Some(obj))

  def merge(that: HalJsObject): HalJsObject = {
    val e: Option[HalJsObject] = (this.embedded, that.embedded) match {
      case (Some(e1), Some(e2)) => Some(e1 ++ e2)
      case (Some(_), None)      => this.embedded
      case (None, Some(_))      => that.embedded
      case _                    => None
    }
    HalJsObject(this.fields ++ that.fields, this.links ++ that.links, e)
  }

  def merge(that: JsObject): HalJsObject = this.merge(HalJsObject.empty.copy(fields = that.fields))

  def ++(that: HalJsObject): HalJsObject = merge(that)

  def ++(that: JsObject): HalJsObject = merge(that)

  lazy val asJsValue: JsObject = {
    val temp = mutable.MutableList.empty[(String, JsValue)]

    if (!links.isEmpty) temp += (("_links", links.asJsValue))

    temp ++= fields

    embedded map (obj => temp += (("_embedded", obj.asJsValue)))

    JsObject(temp.toMap)
  }
}

object HalJsObject {
  val mediaType = `application/hal+json`

  val empty = HalJsObject(immutable.Map.empty, HalJsLinks.empty, None)

  def create(href: String) = empty.self(href)

  def fromJsObject(obj: JsObject): HalJsObject = {
    val links = obj.fields.get("_links")
    val embedded = obj.fields.get("_embedded")
    val fields = obj.fields.filter(e => e._1 != "_links" && e._1 != "_embedded")

    // TODO: Finish unmarshalling
    HalJsObject(fields, HalJsLinks.empty, None)
  }

  implicit object HalJsValueWriter extends RootJsonFormat[HalJsObject] {
    def write(obj: HalJsObject) = obj.asJsValue
    def read(value: JsValue) = HalJsObject.fromJsObject(value.asJsObject)
  }
}

/**
  * Holds the '_links' reserved field. Automatically renders as an array if there
  * are two or more links assigned to a relation.
  */
case class HalJsLinks(
    links: immutable.Map[String, Vector[Link]],
    curies: Vector[Curie]
) extends HalJsValue {

  val isEmpty = links.isEmpty && curies.isEmpty

  def self(href: String) = this.add("self", href)

  // Only returns the last result, if any
  val self: Option[Link] = links.get("self").map(_.head)

  def curie(name: String, href: String) =
    this.copy(curies = curies :+ Curie.create(name, href))

  def add(rel: String, href: String, title: Option[String] = None): HalJsLinks = {
    val link = Link.create(href, title)
    val newList = links.get(rel) match {
      case Some(list) => list :+ link
      case None       => Vector(link)
    }
    this.copy(links = links + ((rel, newList)))
  }

  lazy val asJsValue = {
    // Selects rendering type: Single object, JSON array of objects, or none
    def render(rel: String, ls: Vector[Link]): Option[JsField] = {
      if (ls.isEmpty) None
      else if (ls.size == 1) Some((rel, ls.head.asJsValue))
      else Some((rel, JsArray(ls map (_.asJsValue))))
    }

    val ls = links.map(e => render(e._1, e._2)).flatten.toVector.sortWith(alphabeticalJsField)

    if (curies.isEmpty) JsObject(ls.toMap)
    else {
      val cs = ("curies", JsArray(curies.map(_.asJsValue)))
      JsObject((cs +: ls).toMap)
    }
  }

  def merge(that: HalJsLinks): HalJsLinks = HalJsLinks(
    this.links ++ that.links,
    this.curies ++ that.curies
  )

  def ++(that: HalJsLinks): HalJsLinks = merge(that)
}

object HalJsLinks {
  val empty = HalJsLinks(immutable.Map.empty, Vector.empty)

  def create(href: String) = empty.self(href)
}

case class Link(href: String, title: Option[String] = None, templated: Boolean = false) extends HalJsValue {
  lazy val asJsValue = {
    val temp = mutable.MutableList.empty[(String, JsValue)]
    temp += (("href", JsString(href)))
    title map (t => temp += (("title", JsString(t))))
    if (templated) temp += (("templated" -> JsTrue))
    JsObject(temp.toMap)
  }
}

object Link {
  val templatePattern = "\\{[a-zA-Z0-9_]+\\}".r

  def create(href: String, title: Option[String] = None): Link = {
    val t = templatePattern.findFirstIn(href).isDefined
    Link(href, title, t)
  }
}

case class Curie(name: String, link: Link) extends HalJsValue {
  lazy val asJsValue = {
    val linkObj = link.asJsValue
    JsObject(linkObj.fields + ("name" -> JsString(name)))
  }
}

object Curie {
  def create(name: String, href: String): Curie = Curie(name, Link.create(href))
}
