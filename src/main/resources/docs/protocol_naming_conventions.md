## Naming Conventions for Network Identifiers

Named resources are identified using the following conventions. Identifiers are case-insensitive, and may contain only alphanumeric characters, a dot, a dash, and an underscore.

The main components used in an identifier are:

* **Node ID** - A network-wide unique identifier for a node. Node names must be registered to with master node in order to resolve names into URLs.
* **User ID** - An identifier for a user. This is unique to the node where the user is registered (i.e., a data node). The same user ID may exist in multiple nodes.
* **Repository ID** - A user-specific identifier for a repository. This is managed by the node where the user is registered. The same repository name may be used by multiple users.
* **Serial number** - A variable-length integer (min. length 5) used to identify resources. This is unique to the node where the resource is uploaded.
* **Version** - An integer used to identify the version of a resource.

Conventions that are recognized are:

1. **_[node_id]/[user_id]_** to identify users
2. **_[node_id]/[user_id]/[repository_id]_** to identify repositories.
3. **_[node_id].[serial].[version]_** to identify resources.

Short-hand URL may be denoted under the mnn protocol via **_mnn://[identifier]_**. Name resolution is managed by the master node, where a *node_id* is expanded into a URL. For example, *_mnn://riken/jdoe/repo1_* can be resolved into *_http://www.riken.jp/services/mnn/jdoe/repo1_*.

*Note: Major nodes will have name expansion in the future.
