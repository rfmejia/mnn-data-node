## Repositories

Users organize spectra using a **repository**.

*Note: Resources can only be uploaded via existing repositories.

### Ownership and Groups

The creator of the repository is designated as its owner. Currently, transferring a repository to another owner is not supported.

### Management Policies

The properties of a repository and the inclusion of resources can modified by different users depending on the configured policy.

- *Private* - Modifications may only be made by the creator of the repository
- *Limited* - Group members assigned by the owner may modify the repository
- *Public* - Any registered user may modify the repository

*Note: The scope of modification only affects repositories, and not the resources themselves.

### Special Repositories

Some repositories contain *compiled spectra* that are used as an index to all resources in the network. These are saved under a special repository managed in the master node.
