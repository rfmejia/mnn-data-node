<!-- from CoreService.scala and SecuredAction paths -->
<!-- TODO: Force SSL -->

## Authentication

The authentication mechanism of mnn uses [JSON web tokens][jwt]. First, the user agent acquires a token by supplying the user's credentials via HTTP Basic Authentication. Given a valid credential, a JSON response containing a string `token` field and an ISO8601 expiry date `exp` is supplied. Secured actions can now be accessed by supplying the `token` value via the HTTP header `X-Auth-Token`.

To logout, simply issue a DELETE request with the value of the token. By default, tokens are invalidated upon the indicated expiry date, but do not rely on this mechanism to properly logout.

[jwt]: http://jwt.io/

### POST /api/auth/token
Creates a new JWT under the supplied user credentials.

#### Header parameters

*Required:*

- `Authorization` = `Basic [token]`, where token is the Base64 encoded concatination of "username&password"

**Example**

	Authorization: Basic dGVzdHVzZXIxOnRlc3RwYXNzMQ==

#### Success response

Code: 200 OK

Content:

	{
		"token": "[JSON web token]",
		"exp": "[ISO8601 date]"
	}

#### Failure response(s)

Code: 401 Unauthorized

---

### _All secured actions_

Validates a supplied JWT for a secured action. Note that some actions may require further authorization checks.

#### Header parameters

*Required:*

- `Authorization` = `Bearer [jwt]`, where jwt is the JSON web token obtained from the **POST /api/auth/token** action

**Example**

	Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZXN0dXNlcjEiLCJleHAiOjE0MjM1OTMxMDJ9.fSRud6o8Xsfb2La5EuLv6FYSwCTTwtNvIcuMK8tfntA

#### Failure response(s)

Code: 401 Unauthorized

Content: Message indicating that the supplied token is invalid, or the token was not supplied.

Code: 403 Forbidden

Content: Message indicating that the user does not have the appropriate permissions to perform the action

### DELETE /api/auth/token
Invalidates a JWT.

#### Data parameters

*Required:*

- `token`: `Bearer [jwt]` where jwt is the JSON web token obtained from the **POST /api/auth/token** action

**Example**

	Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZXN0dXNlcjEiLCJleHAiOjE0MjM1OTMxMDJ9.fSRud6o8Xsfb2La5EuLv6FYSwCTTwtNvIcuMK8tfntA

#### Success response

Code: 204 No Content

<!-- TODO: DELETE /api/auth/token (for users) -->
<!-- Invalidates **all** JWTs for a given user. -->

