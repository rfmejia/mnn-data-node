<!-- from UserService.scala -->
## Users

| Method | URL                  | Parameters  | Profile    | Description |
|--------|----------------------|-------------|------------|-------------|
| GET    | /api/users           |             | collection | List all users in this node
| GET    | /api/users/:username |             | mnn:user   | Show user information
| POST   | /api/users           |             |            | Create a new user
| PUT    | /api/users/:username |             |            | Edit user information

### GET /api/users
List all users in this node.

#### Success response

Code: 200 OK

Content: `collection` profile type containing `mnn:user` items

### GET /api/users/{user.name}
Show a specific user's repositories

#### Success response

Code: 200 OK

Content: `mnn:users` profile type

#### Failure response(s)

Code: 404 Not Found

<!-- TODO: Force SSL -->
### POST /api/users/{user.name}
Create a new user

#### Data parameters

*Required:*

- `username` = *String* username of the user
- `password` = *String* plaintext password of the user
- `name` = *String* name of this user
- `email` = *String* email of this user

#### Success response

Code: 201 Created

#### Failure response(s)

Code: 400 Bad Request

Content: Missing required field, or field has incorrect data type or validation
