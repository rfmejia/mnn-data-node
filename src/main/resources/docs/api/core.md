<!-- from CoreService.scala -->
<!-- TODO: Show node statistics, such as mnn host name, http host name, version -->
## Entry point

The API's start URL provides information about this host. Moreover, the reply defines the current state of services available at this endpoint; A node may not have the latest version, therefore user agents must handle individual nodes in a RESTful way as well.

### GET /api
Top-level listing of existing collections in this repository.

#### Success response

Code: 200 OK

Content: `mnn:node` profile type

- List of repositories
- List of users
- List of spectra
- Link to authorization actions


