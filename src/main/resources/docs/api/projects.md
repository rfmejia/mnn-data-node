<!-- from RepositoryService.scala -->
## Repositories

<!-- // TODO: Maybe we should separate the node listings to "local", "external", and "subscribed" -->

### GET /api/repositories
List all repositories in this node.

#### Success response

Code: 200 OK

Content: `collection` profile type containing `mnn:repository` items

<!-- TODO: Remove 'owner' requirement, get currently logged on user instead -->
<!-- TODO: Value-restricted policy name (private, limited, public) -->
### POST /api/repositories
Create a new repository

#### Data parameters

*Required:*

- `owner` = *String* username of the owner of this repository
- `name` = *String* name of this repository
- `description` = *String* short description of this repository
- `readme` = *String* description of this repository in markdown format
- `policy` = *String* management policy name for this repository

#### Success response

Code: 201 Created

#### Failure response(s)

Code: 400 Bad Request

Content: Missing required field, or field has incorrect data type or validation

### GET /api/users/{user.name}/{repository.name}
Show a specific repository, and list all resources contained within.

#### Query parameters

- `offset` = *Integer* indicating how many resources to skip before starting the list
- `limit` = *Integer* indicating the maximum number of resources that can be returned
- `view` = *String* name of a predefined view of the list
- `fields` = *Comma-separated list* of fields to render in each resource body

#### Success response

Code: 200 OK

Content: `mnn:repository` profile type

#### Failure response(s)

Code: 404 Not Found

### PUT /api/users/{user.name}/{repository.name}
Update the details of a specific repository

#### Data parameters

*Required:*

- `name` = *String* name of this repository
- `description` = *String* short description of this repository
- `markdown` = *String* description of this repository in markdown format
- `policy` = *String* management policy name for this repository

#### Success response

Code: 204 No Content

#### Failure response(s)

Code: 400 Bad Request

Content: Missing required field, or field has incorrect data type or validation

<!-- TODO: Code: 404 Not Found -->

### DELETE /api/users/{user.name}/{repository.name}
Delete a specific repository. Note that this does not delete versioned resources that were already created.

#### Success response

Code: 204 No Content

#### Failure response(s)

Code: 404 Not Found

### POST /api/users/{user.name}/{repository.name}/resources
Upload a resource into a repository.

#### Header parameters

*Required*

- `Content-Type` = *String* media type of the content body (It is recommended that this is one of the existing media types the node can recognize)

*Optional*

- `Content-Title` = *String* title of this resource
- `Content-Tags` = *Comma-separated strings* of tags associated with this resource

#### Content body

- Text or byte stream representation of the resource in the format supplied in the Content-Type header

#### Success response

Code: 200 OK

Content: `mnn:compiled_record` or `mnn:reference_record` profile type with complete metadata

#### Failure response

Code: 400 Bad Request - if the repository does not exist, or the content body is missing

<!-- TODO: Code: 404 Not Found - if the repository does not exist -->
