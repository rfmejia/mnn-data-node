<!-- from ResourceService.scala -->
## Resources
### GET /api/resources
List all spectra in this node.

#### Query parameters

- `offset` = *Integer* indicating how many resources to skip before starting the list
- `limit` = *Integer* indicating the maximum number of resources that can be returned
- `view` = *String* name of a predefined view
- `fields` = *Comma-separated list* of fields to render in the response body, if it exists

#### Success response

Code: 200 OK

Content: `collection` profile type containing `mnn:compiled_record` and/or `mnn:reference_record` items

### GET /api/resources/details/{resource.id}
Returns a spectra in its JSON object representation.

#### Success response

Code: 200 OK

Content: `mnn:compiled_record` or `mnn:reference_record` profile type

#### Failure response(s)

Code: 404 Not Found

### GET /api/resources/details/{resource.id}/mt/{media.type}
Returns a spectra in the requested media type format from the path. Note: media type must be URL-encoded.

#### Success response

Code: 200 OK

Content: Raw data in the requested media type

#### Failure response(s)

Code: 404 Not Found

### PUT /api/resources/details/{resource._id}

#### Success response
#### Failure response

### DELETE /api/resources/details/{resource._id}

#### Success response
#### Failure response


### GET /api/resources/search

#### Success response
#### Failure response
