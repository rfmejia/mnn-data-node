# mnn Data Node

## What is this service?

Mass spectra repository

Through mnn, you can create new software 

### Data management

Repositories can be accessed using our REST API.

### Connectivity with 

### Distributed data nodes

Groups that want to host a data node only bear the cost of their own data plus compiled spectra. 



This data node is a part of the [Metabolomics Node Network](linkto:paper).

## Users

*User documentation will be made available soon.*

## Developers

Interested to integrate using APIs, or contribute to the project? Developers can hop on over to this [page](api/docs/developers.md) for more info.
