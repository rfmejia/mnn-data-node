# Webhooks

Data repositories employ *webhooks* to notify subscribers when an event occurs. These can be used to synchronize latest versions, start processing pipelines, or event logging.

To use webhooks, subscribing services must:

1. Implement an HTTP endpoint that receives POST requests. This serves as your webhook.
2. Register your webhook URI with data repositories you want to subscribe to.
3. Listen to HTTP POST requests at webhook URI.

Your webhooks should process a POST request body with the profile `mnn:event`. Behavior is usually based on the `event-type` property.

## Event publishing

Events are published to registered webhooks in the following format:

```
POST /my-webhook-uri
Host: www.myapp.com
Content-type: application/hal+json
Content-length: *

{
  /* General properties */
  "version": 1, /* Webhook protocol version */
  "type": "mnn:event",
  "event_type": "[predefined event type (see below)]",
  "registered_on": "[ISO8601 timestamp]",
  /* Event type-specific properties */
}
```

In case of failed delivery, a repository will try to publish events to subscribers up to a certain number of retries. These events are then discarded.

### Event types

#### `resource_update` 

Indicates that one or more resources have been updated in the repository. 

- repository - Repository ID
- updated - ISO-8601 timestamp indicating the time of commit
- resources - Array of objects with the hyperlink and description where changes can be fetched

## Webhook model

- `id` - UUID
- `repository_name` - Repository ID
- `event_type` - String
- `registered_on` - org.joda.time.DateTime

## Commit model

- `id` - UUID of revision
- `message` - String
- `resources` - List[VersionID] of affected IDs
