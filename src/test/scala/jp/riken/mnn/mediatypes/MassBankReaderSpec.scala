package jp.riken.mnn.mediatypes

import org.specs2.mutable.Specification
import org.specs2.matcher.ParserMatchers
import scalaz._
import spray.json._

class MassBankReaderSpec extends Specification with ParserMatchers {
  val parsers = MassBankReader

  "The MassBankReader" should {
    "recognize single pair assignments" in {
      MassBankReader.ACParser.structure must succeedOn("  AC$INSTRUMENT_TYPE  : LC-ESI-QQ  ")
    }

    "recognize individual peak definitions" in {
      MassBankReader.PKParser.peakTriple must succeedOn("  54.000 39604.0 44")
        .withResult(JsArray(JsNumber(54.0), JsNumber(39604.0), JsNumber(44)))
      MassBankReader.PKParser.peakTriple must succeedOn("  81.900 19802.0 22")
        .withResult(JsArray(JsNumber(81.9), JsNumber(19802.0), JsNumber(22)))
    }

    "recognize multiple peak definitions" in {
      val a = """
PK$ANNOTATION: Some annotation
PK$NUM_PEAK: 3
PK$PEAK: m/z int. rel.int.
  54.000 39604.0 44
  81.900 19802.0 22
  99.900 891090.0 999
"""
      val aJson = MassBankReader.PKParser.structure(a)

      val b = """
PK$ANNOTATION: Some annotation
PK$NUM_PEAK: 3
PK$PEAK: m/z int. rel.int.
  54.000 39604.0 44
  81.900 19802.0 22
  99.900 891090.0 999
   """

      val bJson = MassBankReader.PKParser.structure(b)

      aJson.get must be equalTo (bJson.get)
    }

    "parse sample files correctly" in {
      MassBankReader(TestData.MassBank.samples.head) must beLike {
        case Success(obj) =>
          // println(obj.prettyPrint)
          ok
      }
    }
  }
}

