package jp.riken.mnn.mediatypes

import org.specs2.mutable.Specification
import org.specs2.matcher.ParserMatchers
import scalaz._
import spray.json._

class NistReaderSpec extends Specification with ParserMatchers {
  val parsers = NistReader

  "The NistReader" should {
    "recognize single pair assignments" in {
      NistReader.nameField must succeedOn("Name: Some Molecule Name")
        .withResult(("Name", JsString("Some Molecule Name")))

      NistReader.field must succeedOn("CAS:  One two three  ")
        .withResult(("CAS", JsString("One two three")))
    }

    "recognize individual peak definitions" in {
      val expected = JsArray(JsNumber(26), JsNumber(430))
      NistReader.peakPair must succeedOn("26 430;")
        .withResult(expected)
      NistReader.peakPair must succeedOn("(26,430)")
        .withResult(expected)
      NistReader.peakPair must succeedOn("[26:430]")
        .withResult(expected)
      NistReader.peakPair must succeedOn(":[26\t430];")
        .withResult(expected)
    }

    "recognize multiple peak definitions" in {
      val a = """
Num Peaks: 18
26 430; 27 340; 28 40; 37 480; 38 611; 39 1411; 49 300; 50 1792;
51 2052; 52 1962; 63 340; 73 160; 74 480; 75 180;
76 721; 77 1401; 78 9806; 79 651;
"""
      val aJson = NistReader.peakField(a)

      val b = """
num peaks: 18
(26,430),(27,340),(28,40),(37,480),(38,611),(39,1411),
(49,300),(50,1792),(51,2052),(52,1962),(63,340),
(73,160),(74,480),(75,180),(76,721),(77,1401),(78,9806),(79,651)
   """

      val bJson = NistReader.peakField(b)

      aJson.get must be equalTo (bJson.get)
    }

    "parse sample files correctly" in {
      NistReader(TestData.Nist.samples.head) must beLike {
        case Success(obj) =>
          // println(obj.prettyPrint)
          ok
      }
    }
  }
}

