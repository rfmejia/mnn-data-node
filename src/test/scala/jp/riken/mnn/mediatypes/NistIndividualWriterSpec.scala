package jp.riken.mnn.mediatypes

import org.specs2.mutable.Specification
import scalaz._
import spray.json._

class NistWriterSpec extends Specification {
  "The NistWriter" should {
    "read then write the exact same input" in {
      val sample = TestData.Nist.samples.head
      NistReader(sample) must beLike {
        case Success(obj) =>
          val written = NistWriter(obj)
          written must beLike {
            case Success(result) =>
              result.getData must beLike {
                case Success(data) =>
                  val s = (new String(data))
                  sample.getBytes("UTF-8") must be equalTo (s.getBytes("UTF-8"))
              }
          }
      }
    }
  }
}

