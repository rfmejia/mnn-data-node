package jp.riken.mnn.mediatypes

import org.specs2.mutable.Specification
import scalaz._
import spray.json._

class MassBankWriterSpec extends Specification {
  "The MassBankWriter" should {
    "read then write the exact same input" in {
      val sample = TestData.MassBank.samples.head
      MassBankReader(sample) must beLike {
        case Success(obj) =>
          val written = MassBankWriter(obj)
          written must beLike {
            case Success(result) =>
              result.getData must beLike {
                case Success(data) =>
                  val s = (new String(data))
                  // println(s)
                  sample.getBytes("UTF-8") must be equalTo (s.getBytes("UTF-8"))
              }
          }
      }
    }
  }
}

