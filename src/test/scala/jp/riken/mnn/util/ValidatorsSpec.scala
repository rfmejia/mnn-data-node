package jp.riken.mnn.util

import org.specs2.mutable.Specification
import scalaz._
import scala.util.matching.Regex

class ValidatorsSpec extends Specification {
  "Validators.nonNull" should {
    "check for non-null objects" in {
      Validators.nonNull(1) must beLike { case Success(s) => ok }
      Validators.nonNull(null) must beLike { case Failure(f) => ok }
    }
  }

  "Validators.nonEmptyString" should {
    "check for both non-null and non-empty strings" in {
      Validators.nonEmptyString("nonempty") must beLike { case Success(s) => ok }
      Validators.nonEmptyString("") must beLike { case Failure(f) => ok }
      Validators.nonEmptyString(null) must beLike { case Failure(f) => ok }
    }
  }

  "Validators.hasPattern" should {
    "check for pattern matches" in {
      val regex = "^[a-zA-Z0-9_-]+$".r
      Validators.hasSingleMatch("abc123", regex) must beLike { case Success(s) => ok }
      Validators.hasSingleMatch("123-abc", regex) must beLike { case Success(s) => ok }
      Validators.hasSingleMatch("123 abc", regex) must beLike { case Failure(s) => ok }
      Validators.hasSingleMatch("!#$%&'()", regex) must beLike { case Failure(s) => ok }
      Validators.hasSingleMatch("", regex) must beLike { case Failure(s) => ok }
    }
  }
}
