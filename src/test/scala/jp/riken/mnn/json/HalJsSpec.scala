package jp.riken.mnn.json

import org.specs2.mutable._
import spray.json._
import spray.http.Uri
import spray.json.DefaultJsonProtocol

class HalJsSpec extends Specification {

  "HalJsLinks" should {
    "render the links object correctly" in {
      HalJsLinks.empty.asJsValue === "{}".parseJson

      val url1 = "http://something.com"
      val a = HalJsLinks.create(url1)
      a === HalJsLinks.empty.self(url1)
      a.asJsValue === s"""{"self": { "href": "${url1}" }}""".parseJson

      val url2 = "http://something.com?page=1"
      val b = a.add("next", url2)
      b.asJsValue === s"""{"self": { "href": "${url1}" },
        "next": { "href": "${url2}"}}""".parseJson

      val url3 = "http://mnn.massbank.jp/rels/{rel}"
      val c = b.curie("mnn", url3)
      c.asJsValue === s"""{"self": { "href": "${url1}" },
        "curies": [{ "name": "mnn", "href": "${url3}", "templated": true }],
        "next": { "href": "${url2}"}}""".parseJson

      val url4 = "http://something.com?page=2"
      val d = c.add("next", url4)
      d.asJsValue === s"""{"self": { "href": "${url1}" },
        "curies": [{ "name": "mnn", "href": "${url3}", "templated": true }],
        "next": [{ "href": "${url2}"}, { "href": "${url4}"}]}""".parseJson
    }

    "render the fields object correctly" in {
      HalJsObject.empty.asJsValue === "{}".parseJson

      val url1 = "http://something.com/items/1"
      val a = HalJsObject.create(url1).withField("hello", JsString("world"))
        .withField("_reserved", JsArray(JsTrue, JsFalse, JsNull))
      a.asJsValue === s"""{
        "_links": {
          "self": { "href": "${url1}" }
        },
        "hello": "world",
        "_reserved": [true, false, null]
      }""".parseJson

      val url2 = "http://something.com/items"
      val b = HalJsObject.create(url2).withEmbedded(a)
      b.asJsValue === s"""{
        "_links": {
          "self": { "href": "${url2}" }
        },
        "_embedded": ${a.asJsValue.toString}
      }""".parseJson
      ok
    }
  }
}

