package jp.riken.mnn.slick

import java.net.URL
import jp.riken.mnn._
import jp.riken.mnn.database.Database
import org.joda.time.DateTime
import org.specs2.mutable.Specification
import scala.slick.driver.H2Driver.simple._
import scala.slick.lifted.Query

class ModelSpec extends Specification {
  implicit val cred = new Credentials("jdbc:h2:mem:modelspec", "org.h2.Driver")

  "NodeInfo models" should {
    "be inserted and removed from the database" in {
      DB.connect withSession { implicit session =>
        val id = "some-authority"
        val inMem = NodeInfo(id, DateTime.now, new URL("http://localhost:8080"), "MASTER")

        DB.nodeInfos().ddl.create
        DB.nodeInfos() += inMem

        val inDB = DB.nodeInfos(id).getOrElse(failure)
        inDB must be equalTo (inMem)
      }
    }
  }

  // TODO: Complete modeling necessary datatypes
}
