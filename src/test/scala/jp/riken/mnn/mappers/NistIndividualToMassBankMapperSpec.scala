package jp.riken.mnn.mappers

import jp.riken.mnn.mediatypes.{ NistReader, MassBankWriter }
import jp.riken.mnn.mediatypes.TestData
import jp.riken.mnn.RawData
import org.specs2.mutable.Specification
import scalaz._
import scalaz.Scalaz._
import spray.json._

class NistToMassBankMapperSpec extends Specification {
  "The mapper" should {
    "successfully transform a sample object correctly" in {
      val result = for {
        sample <- TestData.Nist.samples.head.successNel
        obj <- NistReader(sample)
        transformed <- NistToMassBankMapper.transformModel(obj)
      } yield transformed
      result must beLike {
        case Success(obj) =>
          // println(obj.prettyPrint)
          ok
      }
    }

    "successfully perform a full transformation (read-transform-write)" in {
      println(NistToMassBankMapper)
      val result = for {
        sample <- TestData.Nist.samples.head.successNel
        raw <- RawData("application/vnd.nist.individual", sample.getBytes("UTF-8"))
        transformed <- NistToMassBankMapper(raw)
      } yield transformed

      result must beLike {
        case Success(raw) =>
          for {
            data <- raw.getData
          } {
            println(new String(data))
          }
          ok
      }
    }
  }
}

