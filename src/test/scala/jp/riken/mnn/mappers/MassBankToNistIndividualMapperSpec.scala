package jp.riken.mnn.mappers

import jp.riken.mnn.mediatypes.{ MassBankReader, NistWriter }
import jp.riken.mnn.mediatypes.TestData
import jp.riken.mnn.RawData
import org.specs2.mutable.Specification
import scalaz._
import scalaz.Scalaz._
import spray.json._

class MassBankToNistMapperSpec extends Specification {
  "The mapper" should {
    "successfully transform a sample object correctly" in {
      val result = for {
        sample <- TestData.MassBank.samples.head.successNel
        obj <- MassBankReader(sample)
        transformed <- MassBankToNistMapper.transformModel(obj)
      } yield transformed
      result must beLike {
        case Success(obj) =>
          // println(obj.prettyPrint)
          ok
      }
    }

    "successfully perform a full transformation (read-transform-write)" in {
      println(MassBankToNistMapper)
      val result = for {
        sample <- TestData.MassBank.samples.head.successNel
        raw <- RawData("application/vnd.massbank", sample.getBytes("UTF-8"))
        transformed <- MassBankToNistMapper(raw)
      } yield transformed

      result must beLike {
        case Success(raw) =>
          // for {
          //   data <- raw.getData
          // } {
          //   println(new String(data))
          // }
          ok
      }
    }
  }
}

