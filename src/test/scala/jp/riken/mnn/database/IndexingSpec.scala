package jp.riken.mnn.database

import com.typesafe.config.{ Config, ConfigFactory }
import jp.riken.mnn._
import jp.riken.mnn.database._
import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.document._
import org.apache.lucene.index._
import org.apache.lucene.queryparser.classic.QueryParser
import org.apache.lucene.search._
import org.apache.lucene.store._
import org.apache.lucene.util.Version
import org.specs2.mutable.Specification
import scala.slick.driver.H2Driver.simple._
import scalaz._
import scalaz.Scalaz._

class IndexingSpec extends Specification {
  "Database indexer" should {
    System.setProperty("project.driver", "org.h2.Driver")
    System.setProperty("project.url", "jdbc:h2:/home/rfmejia/workspace/mnn-data-node/test/testdb2")
    System.setProperty("project.resourcePath", "/home/rfmejia/workspace/mnn-data-node/test/resources2")
    System.setProperty("project.indexDataPath", "/home/rfmejia/workspace/mnn-data-node/test/index")
    val conf = ConfigFactory.load()
    val testdb = Credentials.fromConfig(conf) match {
      case Success(cred) => cred
      case Failure(t) => throw new IllegalStateException("Test credentials error")
    }
    DB.create(conf)

    "index resource objects" in {
      DB.connect(testdb) withSession { implicit s =>
        val checks = for {
          json <- "application/json".successNel
          bytes1 <- doc1.getBytes("UTF-8").successNel
          bytes2 <- doc2.getBytes("UTF-8").successNel
          bytes3 <- doc3.getBytes("UTF-8").successNel
          raw1 <- RawData(json, bytes1)
          raw2 <- RawData(json, bytes2)
          raw3 <- RawData(json, bytes3)
          res1 <- DB.resources.create(Some("Pink Floyd"), raw1)
          res2 <- DB.resources.create(Some("Eagles"), raw2)
          res3 <- DB.resources.create(Some("Pink"), raw3)
        } yield {
          def search(term: String, offset: Int = 0, limit: Int = 10) = {
            for (result <- DB.resources.search(term, offset, limit)) {
              println(s"Result for '${result.query}': ${result.totalHits}")
              result.hits foreach { hit =>
                println(s"> ${hit._1} (${hit._2})")
              }
              println()
            }
          }

          search("title:Pi*")
          search("Easy")
          search("id:unregistered*")

          ok
        }
        checks must beLike { case Success(_) => ok }
      }
    }
  }

  val doc1 = """
Comfortably Numb
Dogs
Shine On You Crazy Diamond
"""

  val doc2 = """
Take It Easy
Hotel California
Love Will Keep Us Alive
"""

  val doc3 = """
Beam Me Up
Just Give Me A Reason
Just Like A Pill
Try
"""
}
