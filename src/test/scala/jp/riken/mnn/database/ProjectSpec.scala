package jp.riken.mnn.database

import com.typesafe.config.{ Config, ConfigFactory }
import jp.riken.mnn._
import jp.riken.mnn.database._
import org.specs2.mutable.Specification
import scala.slick.driver.H2Driver.simple._
import scalaz._
import scalaz.Scalaz._

class DatabaseSpec extends Specification {
  "Database object" should {
    // TODO: Redo tests

    "test for invalid credentials" in {
      //      implicit val c1 = new Credentials("jdbc:h2:mem:credTest", "org.h2.Driver", Some("root"), Some("test"))
      //      DB.connect withSession { implicit s =>
      //        DB.buildTables
      //        //      DB.authenticate(c1) must beLike
      //        implicit val c2 = new Credentials("jdbc:h2:mem:credTest", "org.h2.Driver")
      //        //      DB.authenticate(c2)
      //      }
      failure
    }

    System.setProperty("project.driver", "org.h2.Driver")
    System.setProperty("project.url", "jdbc:h2:/home/rfmejia/workspace/mnn-data-node/test/testdb")
    System.setProperty("project.resourcePath", "/home/rfmejia/workspace/mnn-data-node/test/resources")
    val conf = ConfigFactory.load()
    val testdb = Credentials.fromConfig(conf) match {
      case Success(cred) => cred
      case Failure(t) => throw new IllegalStateException("Test credentials error")
    }
    DB.create(conf)

    "have the default settings upon creation" in {
      DB.connect(testdb) withSession { implicit s =>
        val settings = DB.settings()
        settings.get("node.id") must beSome("unregistered")
        settings.get("node.serial") must beSome("1")
        settings.get("node.type") must beSome("minor")
      }
    }

    "create a new resource" in {
      DB.connect(testdb) withSession { implicit s =>
        val checks = for {
          mt <- "application/json".successNel
          bytes <- "hello".getBytes("UTF-8").successNel
          orig <- RawData(mt, bytes)
          saved <- DB.resources.create(None, orig)
        } yield {
          saved.data must beLike {
            case Some(data) =>
              data.mediaType must be equalTo orig.mediaType
              (data compareBytes orig) must beTrue
          }
        }

        checks must beLike { case Success(_) => ok }
      }
    }

    "derive a new resource correctly" in {
      DB.connect(testdb) withSession { implicit s =>

        val checks = for {
          xml <- "text/xml".successNel
          bytes1 <- "<xml ns='http://...'><something><something/>".getBytes("UTF-8").successNel
          data1 <- RawData(xml, bytes1)
          html <- "text/html".successNel
          bytes2 <- "<html><h1>This is a test for updating resources</h1></html>".getBytes("UTF-8").successNel
          data2 <- RawData(html, bytes2)
          orig <- DB.resources.create(None, data1)
          updated <- DB.resources.update(orig.id, data2)
        } yield {
          ResourceID.next(orig.id) must be equalTo updated.id
          // TODO: Add more tests
        }

        checks must beLike { case Success(_) => ok }
      }
    }

    "return the latest and old versions of resources properly" in {
      var generatedID: VersionedID = null

      DB.connect(testdb) withSession { implicit s =>

        val checks = for {
            json <- "application/json".successNel
            bytes1 <- "{\"variable\": 1}".getBytes("UTF-8").successNel
            bytes2 <- "{\"variable\": 2}".getBytes("UTF-8").successNel
            bytes3 <- "{\"variable\": 3}".getBytes("UTF-8").successNel
            raw1 <- RawData(json, bytes1)
            raw2 <- RawData(json, bytes2)
            raw3 <- RawData(json, bytes3)
            res1 <- DB.resources.create(None, raw1)
            res2 <- DB.resources.update(res1.id, raw2)
            res3 <- DB.resources.update(res1.id, raw3)
          } yield {
            generatedID = res1.id
            DB.resources.versionsOf(generatedID.asPlain).list.size must be equalTo 3
          }

          /*
          To fix bug: Change DB.save(..., id: Option[VersionID], ...) to DB.save(..., id: Option[ID], ...) and change the code to get the latest version
           */

        checks must beLike { case Success(_) => ok }
      }
    }

    //    "create the cache successfully"
  }
}
