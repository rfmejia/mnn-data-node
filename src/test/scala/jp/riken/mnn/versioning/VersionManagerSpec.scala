package jp.riken.mnn.versioning

import org.specs2.mutable.Specification
import jp.riken.mnn._
import scala.util.Random
import scalaz._
import scalaz.Scalaz._

class VersionManagerSpec extends Specification {
  val text = "Sir Adrian Paul Ghislain Carton de Wiart VC, KBE, CB, CMG, DSO (5 May 1880 – 5 June 1963) was a British Army officer of Belgian and Irish descent. He served in the Boer War, First World War, and Second World War; was shot in the face, head, stomach, ankle, leg, hip, and ear; survived a plane crash; tunnelled out of a POW camp; and bit off his own fingers when a doctor refused to amputate them. Describing his experiences in World War I, he wrote, 'Frankly I had enjoyed the war.'"
  val shortText = "One Two Three Four Five"

  "A resource generator" should {
    def derive(raw: RawData): ValidationNel[String, RawData] = for {
      b <- raw.getData
      words <- new String(b).split(' ').toList.successNel
      i <- if (words.length <= 1) "Empty".failureNel
      else (Random.nextInt(words.length - 1) + 1).successNel
      derived <- {
        if (Random.nextBoolean) words.take(i - 1) ++ words.drop(i)
        else
          (words.take(i - 1) :+ words.apply(i).toList.map(c => "-").reduce(_ + _)) ++ words.drop(i)
      }.reduce(_ + " " + _).getBytes("UTF-8").successNel
      newData <- RawData(raw.mediaType, derived)
    } yield newData

    "correctly generate diffs for data and patch back" in {
      def loop(curr: RawData): ValidationNel[String, RawData] = derive(curr) match {
        case Success(next) =>
          val d = VersionManager.diff(next, curr) valueOr (t => throw new Exception(s"Could not get diff: ${t}"))
          val p = VersionManager.patch(next, d) valueOr (t => throw new Exception(s"Could not patch: ${t}"))
          val result = for {
            c <- curr.getData
            p <- p.getData
            test <- if ((c zip p) exists (b => b._1 == b._2)) {
              s"Byte mismatch => \n  expected: ${new String(c)}\\n  actual: ${new String(p)}".failureNel
            } else p.successNel
          } yield test

          result match {
            case Success(s) => loop(next)
            case Failure(t) =>
              t.head.failureNel
          }
        case Failure(t) => curr.successNel
      }

      val results = for {
        mt <- "text/*".successNel
        raw <- RawData(mt, text.getBytes("UTF-8"))
      } yield {
        loop(raw)
      }
      results must beLike { case Success(d) => ok }

    }

//    "correctly create a new resource metadata" in {
//      for {
//        mt <- MediaType("text/html")
//        raw <- RawData(mt, "abc123".getBytes("UTF-8"))
//      } {
//        implicit val cred = new Credentials("jdbc:h2:~/test1", "org.h2.Driver")
//        DB.connect withSession { s =>
//          VersionManager.generateNew(raw)
//        }
//      }
//    }

    "correctly create a new resource when supplied with an existing resource"
  }
}
