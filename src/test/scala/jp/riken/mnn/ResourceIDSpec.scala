package jp.riken.mnn

import jp.riken.mnn.util.Validators._
import org.specs2.mutable.Specification
import scalaz._

class ResourceIDSpec extends Specification {
  "ResourceIDs" should {

    "not be created using empty or null strings" in {
      ResourceID.parse("") must beLike { case Failure(f) => ok }
      ResourceID.parse(null) must beLike { case Failure(f) => ok }
    }

    "not be accept invalid formats" in {
      ResourceID.parse("ABC12#") must beLike { case Failure(f) => ok }
      ResourceID.parse("ABC12#") must beLike { case Failure(f) => ok }
      ResourceID.extractNamespace("ABC-123.") must beLike { case Success(f) => ok }
      ResourceID.extractNamespace("ABC_123.") must beLike { case Success(f) => ok }
    }

    "check for parsed plain ID equality" in {
      ResourceID.parse("abc.123") must beLike {
        case Success(id) => id must beLike {
          case plain: PlainID => plain === PlainID("abc", 123)
        }
      }
    }

    "check for parsed versioned ID equality" in {
      ResourceID.parse("abc.123.1") must beLike {
        case Success(id) => id === VersionedID("abc", 123, 1)
      }
    }
  }
}
