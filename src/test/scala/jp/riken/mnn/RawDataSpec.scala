package jp.riken.mnn

import com.typesafe.config.ConfigFactory
import jp.riken.mnn.database.Database
import org.specs2.mutable.Specification
import scala.io.Source
import scalaz._
import scalaz.Scalaz._

class RawDataSpec extends Specification {
  val mediaType = "text/html"
  val emptyData: Array[Byte] = Array.empty

  System.setProperty("project.driver", "org.h2.Driver")
  System.setProperty("project.url", "jdbc:h2:mem:RawDataSpec")
  System.setProperty("project.resourcePath", "./test/RawDataSpec")
  val conf = ConfigFactory.load()
  val testdb = Credentials.fromConfig(conf) match {
    case Success(cred) => cred
    case Failure(t) => throw new IllegalStateException("Test credentials error")
  }
  DB.create(conf)

  "RawData objects" should {
    "be stored in memory upon user instantiation" in {
      val raw = RawData(mediaType, emptyData) getOrElse failure
      raw must beLike {
        case InMemoryData(mt, delta, d) =>
          mt must be equalTo mediaType
          delta must be equalTo None
          d must be equalTo emptyData
      }
    }

    "not accept missing media types or data" in {
      RawData(null, emptyData) must beLike { case scalaz.Failure(msg) => ok }
      RawData(mediaType, null) must beLike { case scalaz.Failure(msg) => ok }
    }

    "save into a local file successfully" in {
      val filename = "saveTest.txt"
      RawData(mediaType, "hello".getBytes("UTF-8")) must beLike {
        case scalaz.Success(raw) =>
          raw.saveToDisk(filename) must beLike {
            case Success(local) =>
              val source = Source.fromFile(DB.settings.resourcePath + "/" + filename)
              source.map(_.toByte).toArray must be equalTo "hello".getBytes("UTF-8")
              source.close
              ok
          }
      }
    }

    "open a local file successfully" in {
      val filename = "openTest.txt"
      val check = (for {
        raw <- RawData(mediaType, "world".getBytes("UTF-8"))
        local <- raw.saveToDisk(filename)
        loaded <- LocalFile(raw.mediaType, raw.deltaMethod, filename).successNel
        d1 <- local.getData
        d2 <- loaded.getData
      } yield {
        d1 must be equalTo d2
      })
      check must beLike {
        case Success(_) => ok
      }
      //      RawData(mediaType, "world".getBytes("UTF-8")) must beLike {
      //        case Success(raw) =>
      //          raw.saveToDisk("openTest.txt") must beLike { case Success(_) => ok }
      //          val loaded = LocalFile(raw.mediaType, raw.deltaMethod, "openTest.txt")
      //          val d1 = raw.getData getOrElse null
      //          val d2 = loaded.getData getOrElse null
      //          d1 must be equalTo d2
      //      }
    }
  }
}
