package jp.riken.mnn.api

import org.specs2.mutable.Specification
import spray.json._
import spray.testkit.Specs2RouteTest
import spray.http.StatusCodes._

class BasicSpec extends Specification with Specs2RouteTest with CoreService {
  def actorRefFactory = system

  "A minimal API service" should {
    "render a valid minimal HAL JSON representation at the entry point" in {
      // val target = CollectionJson

      Get() ~> coreRoute ~> check {
        val r = responseAs[String]
        r must contain("_links")
      }
    }
  }
}

