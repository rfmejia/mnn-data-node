package jp.riken.mnn

import jp.riken.mnn.mappers._
import jp.riken.mnn.ObjectRegistry.registeredMediaTypes._
import org.specs2.mutable.Specification
import scalaz._
import scalaz.Scalaz._
import spray.json._

class ObjectRegistrySpec extends Specification {
  "The mapper oracle" should {
    "successfully report mappers for each media type" in {
      ObjectRegistry.mappers.ofSource(`application/vnd.massbank`).map(_.toString) ===
        List(MassBankToNistMapper).map(_.toString)

      ObjectRegistry.mappers.ofSource(`application/vnd.nist.individual`).map(_.toString) ===
        List(NistToMassBankMapper).map(_.toString)
    }
  }
}

